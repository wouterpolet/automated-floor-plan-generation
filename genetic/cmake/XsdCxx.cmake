function (xsdcxx)
    set(OPTION_PARAMS GENERATE_SERIALIZATION)
    set(SINGLE_VALUE_PARAMS XSD OUTPUT_DIR)
    set(MULTI_VALUE_PARAMS NAMESPACE_MAPPINGS LOCATION_MAPPINGS ORDERED_TYPES)
    cmake_parse_arguments(ARG "${OPTION_PARAMS}" "${SINGLE_VALUE_PARAMS}" "${MULTI_VALUE_PARAMS}" ${ARGN})

    if (ARG_OUTPUT_DIR_MISSING)
        set(ARG_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}")
    endif ()

    set(NAMESPACE_MAPPINGS "")
    foreach (NAMESPACE_MAPPING ${ARG_NAMESPACE_MAPPINGS})
        list(APPEND NAMESPACE_MAPPINGS "--namespace-map" "${NAMESPACE_MAPPING}")
    endforeach ()

    set(ORDERED_TYPES "")
    foreach (ORDERED_TYPE ${ARG_ORDERED_TYPES})
        list(APPEND ORDERED_TYPES "--ordered-type" "${ORDERED_TYPE}")
    endforeach ()

    set(LOCATION_MAPPINGS "")
    foreach (LOCATION_MAPPING ${ARG_LOCATION_MAPPINGS})
        list(APPEND LOCATION_MAPPINGS "--location-map" "${LOCATION_MAPPING}")
    endforeach ()

    set(GENERATE_SERIALIZATION "")
    if (ARG_GENERATE_SERIALIZATION)
        set(GENERATE_SERIALIZATION "--generate-serialization")
    endif ()

    file(MAKE_DIRECTORY "${ARG_OUTPUT_DIR}")

    execute_process(COMMAND xsdcxx cxx-tree
                --std c++11
                --type-naming ucc
                --generate-doxygen
                --hxx-suffix .hpp
                --cxx-suffix .cpp
                ${NAMESPACE_MAPPINGS}
                ${ORDERED_TYPES}
                ${LOCATION_MAPPINGS}
                ${GENERATE_SERIALIZATION}
                ${ARG_XSD}
            WORKING_DIRECTORY "${ARG_OUTPUT_DIR}"
            COMMAND_ECHO STDERR
            RESULT_VARIABLE XSDCXX_STATUS
    )

    if (NOT XSDCXX_STATUS EQUAL 0)
        message(FATAL_ERROR "Unable to generate C++ for schema" ${ARG_XSD})
    endif ()
endfunction ()
