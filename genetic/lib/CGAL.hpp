#pragma once

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/partition_2.h>

namespace Quinoa {

using Kernel = CGAL::Simple_cartesian<double>;
using Point_2 = CGAL::Point_2<Kernel>;
using Segment_2 = CGAL::Segment_2<Kernel>;
using Polygon_2 = CGAL::Polygon_2<Kernel>;
using Polygon_with_holes_2 = CGAL::Polygon_with_holes_2<Kernel>;

static inline bool is_in_poly(const Polygon_2& poly, const Point_2& p) {
    const auto side = poly.oriented_side(p);
    return side == CGAL::POSITIVE || side == CGAL::ON_ORIENTED_BOUNDARY;
}

}
