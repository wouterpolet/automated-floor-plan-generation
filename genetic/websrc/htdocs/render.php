<?php

$render_exc = '/usr/local/bin/render';
$extra_args = ['-x', '/var/lib/afpg-render/input.xsd', '-y', '/var/lib/afpg-render/output.xsd'];

if (version_compare(PHP_VERSION, '8.0') < 0) {
    function str_ends_with($haystack, $needle) {
        return substr($haystack, -strlen($needle)) == $needle;
    }
}

$files = $_FILES['files'];
$invalid_files = array_filter($files['name'], function ($name) { return !str_ends_with($name, '.xml'); });

if ($invalid_files != []) {
    header('Content-Type: text/plain');
    echo 'Only .xml files are accepted';
    return;
}


$orig_names = $files['name'];
$file_names = [];
$out_names = [];
$file_name_to_orig_name = [];
foreach ($files['tmp_name'] as $file) {
    move_uploaded_file($file, $file . '.xml');
    $file_names[] = $file . '.xml';
    $out_names[] = $file . '.svg';
}

$cmd = array_merge([$render_exc], $extra_args, $file_names);
$dspec = [
    ['pipe', 'r'],
    ['pipe', 'w'],
    ['pipe', 'w']
];
$proc = proc_open($cmd, $dspec, $pipes);
fclose($pipes[0]);

$fcmd = '$ ' . implode(' ', $cmd);
$stdout = stream_get_contents($pipes[1]);
$stderr = stream_get_contents($pipes[2]);
proc_close($proc);

$ar_path = tempnam(sys_get_temp_dir(), '.zip');
$ar = new ZipArchive();
$res = $ar->open($ar_path, ZipArchive::CREATE);
if ($res !== true) {
    header('Content-Type: text/plain');
    echo 'Cannot create zip file: ' . $res;
}

if ($stdout) $ar->addFromString('stdout.txt', $stdout);
if ($stderr) $ar->addFromString('stderr.txt', $stderr);

for ($i = 0; $i < sizeof($file_names); ++$i) {
    $ar->addFile($out_names[$i], preg_replace('/\.xml$/', '.svg', $orig_names[$i]));
    $file_name_to_orig_name[$files['tmp_name'][$i] . '.xml'] = $orig_names[$i];
}

$ar->addFromString('mapping.txt', print_r($file_name_to_orig_name, true));

if (!$ar->close()) {
    header('Content-Type: text/plain');
    echo 'Cannot create zip file';
}

header('Cache-Control: private');
header('Content-Type: application/zip');
header('Content-Length: ' . filesize($ar_path));
header('Content-Disposition: filename=floorplans.zip');

readfile($ar_path);
flush();

unlink($ar_path);
foreach ($out_names as $out_path) {
    unlink($out_path);
}
