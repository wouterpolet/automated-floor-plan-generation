#pragma once

#include "CardinalDirection.hpp"
#include "HyperparamSupport.hpp"
#include "Rect.hpp"

#include "input.hpp"
#include "NSGA2.hpp"

#include <ostream>
#include <utility>

namespace Quinoa {

class FloorPlanGenomeNode;
class FloorPlanGenomeSplit;
class FloorPlanGenome;

template <std::size_t D, std::uniform_random_bit_generator R>
class FloorPlanMutation;

template <std::size_t D, std::uniform_random_bit_generator R>
class FloorPlanCrossover;

template <typename T>
class FloorPlanIterator {
private:
    std::vector<T> visit_stack;

public:
    FloorPlanIterator() = default;
    explicit FloorPlanIterator(T first) : visit_stack{first} {}
    FloorPlanIterator(const FloorPlanIterator& other) = default;

    auto operator=(const FloorPlanIterator& other) -> FloorPlanIterator& = default;

    auto operator*() const -> T {
        if (this->visit_stack.empty()) {
            throw std::runtime_error("Cannot dereference end iterator");
        }

        return this->visit_stack.back();
    }

    auto operator++() -> FloorPlanIterator&;

    auto operator++(int) -> FloorPlanIterator {
        FloorPlanIterator copy(*this);
        ++(*this);
        return copy;
    }

    bool operator==(const FloorPlanIterator& other) const = default;

    bool done() {
        return this->visit_stack.empty();
    }
};

template <typename T, typename F>
class FloorPlanIteratorFilter {
private:
    FloorPlanIterator<T> it;

public:
    FloorPlanIteratorFilter() = default;

    explicit FloorPlanIteratorFilter(FloorPlanIterator<T> it) : it(std::move(it)) {
        this->advance();
    }

    FloorPlanIteratorFilter(const FloorPlanIteratorFilter& other) = default;

    auto operator=(const FloorPlanIteratorFilter& other) -> FloorPlanIteratorFilter& = default;

    auto operator*() const -> F {
        return dynamic_cast<F>(*this->it);
    }

    auto operator->() const -> F {
        return **this;
    }

    auto operator++() -> FloorPlanIteratorFilter& {
        ++this->it;
        this->advance();
        return *this;
    }

    auto operator++(int) -> FloorPlanIteratorFilter {
        FloorPlanIteratorFilter copy(*this);
        ++(*this);
        return copy;
    }

    bool operator==(const FloorPlanIteratorFilter& other) const = default;

private:
    void advance() {
        while (!this->it.done() && !dynamic_cast<F>(*this->it)) ++this->it;
    }
};

class FloorPlanGenomeNode {
protected:
    bool left = false;
    FloorPlanGenomeSplit* parent = nullptr;
    FloorPlanGenome* container = nullptr;
    friend FloorPlanGenomeSplit;
    friend FloorPlanGenome;
    friend FloorPlanMutation<6, std::mt19937_64>;
    friend FloorPlanCrossover<6, std::mt19937_64>;

public:
    virtual ~FloorPlanGenomeNode() = default;

    [[nodiscard]] virtual auto clone() const -> std::unique_ptr<FloorPlanGenomeNode> = 0;

    virtual explicit operator std::string() const = 0;

    virtual auto operator==(const FloorPlanGenomeNode& other) const -> bool = 0;

    inline auto replace(std::unique_ptr<FloorPlanGenomeNode> replacement) -> void;

    auto get_parent() -> FloorPlanGenomeSplit* {
        return this->parent;
    }

    [[nodiscard]] auto has_parent() const -> bool {
        return this->parent;
    }

    [[nodiscard]] auto has_container() const -> bool {
        return this->container;
    }

    [[nodiscard]] auto is_left() const {
        return this->left;
    }

    inline auto get_self_p() -> std::unique_ptr<FloorPlanGenomeNode>&;

    auto set_left(bool left) {
        this->left = left;
    }

    [[nodiscard]] virtual unsigned int get_height() const = 0;
    [[nodiscard]] virtual unsigned int get_node_count() const = 0;
    [[nodiscard]] virtual unsigned int get_leaf_count() const = 0;
    [[nodiscard]] virtual unsigned int get_split_count() const = 0;

    [[nodiscard]] virtual auto begin() const -> FloorPlanIterator<const FloorPlanGenomeNode*> {
        return FloorPlanIterator<const FloorPlanGenomeNode*>(this);
    }

    [[nodiscard]] virtual auto begin() -> FloorPlanIterator<FloorPlanGenomeNode*> {
        return FloorPlanIterator<FloorPlanGenomeNode*>(this);
    }

    [[nodiscard]] virtual auto end() const -> FloorPlanIterator<const FloorPlanGenomeNode*> {
        return FloorPlanIterator<const FloorPlanGenomeNode*>();
    }

    [[nodiscard]] virtual auto end() -> FloorPlanIterator<FloorPlanGenomeNode*> {
        return FloorPlanIterator<FloorPlanGenomeNode*>();
    }

    auto swap(FloorPlanGenomeNode& other) {
        using std::swap;
        swap(this->get_self_p(), other.get_self_p());
        swap(this->parent, other.parent);
        swap(this->container, other.container);
        swap(this->FloorPlanGenomeNode::left, other.FloorPlanGenomeNode::left);
    }
};

class FloorPlanGenomeSplit : public FloorPlanGenomeNode {
private:
    Split split;
    double split_dist;

protected:
    std::unique_ptr<FloorPlanGenomeNode> left;
    std::unique_ptr<FloorPlanGenomeNode> right;

    friend FloorPlanGenomeNode;
    friend FloorPlanIterator<FloorPlanGenomeNode*>;
    friend FloorPlanIterator<const FloorPlanGenomeNode*>;

public:
    FloorPlanGenomeSplit(
            Split split,
            double split_dist,
            std::unique_ptr<FloorPlanGenomeNode> left,
            std::unique_ptr<FloorPlanGenomeNode> right
    ) :
            split(split),
            split_dist(split_dist),
            left(std::move(left)),
            right(std::move(right))
    {
        this->left->left = true;
        this->left->container = this->right->container = nullptr;
        this->left->parent = this;
        this->right->parent = this;
    }

    [[nodiscard]] auto clone() const -> std::unique_ptr<FloorPlanGenomeNode> override {
        return std::make_unique<FloorPlanGenomeSplit>(
                this->split,
                this->split_dist,
                this->left->clone(),
                this->right->clone()
        );
    }

    explicit operator std::string() const override {
        using namespace std::string_literals;
        return "{["s + (this->split == Split::HORIZONTAL ? "x" : "y") + "/" + std::to_string(this->split_dist) + "] "
                + std::string(*this->left) + ", " + std::string(*this->right) + '}';
    }

    auto operator==(const FloorPlanGenomeNode &obj) const -> bool override {
        try {
            auto& other = dynamic_cast<const FloorPlanGenomeSplit&>(obj);
            return this->split == other.split
                    && this->split_dist == other.split_dist
                    && *(this->left) == *(other.left)
                    && *(this->right) == *(other.right);
        } catch (const std::bad_cast &ex) {
            return false;
        }
    }

    [[nodiscard]] auto get_left() const -> const FloorPlanGenomeNode& {
        return *this->left;
    }

    [[nodiscard]] auto get_right() const -> const FloorPlanGenomeNode& {
        return *this->right;
    }

    [[nodiscard]] auto extract_left() -> std::unique_ptr<FloorPlanGenomeNode> {
        return std::move(this->left);
    }

    [[nodiscard]] auto extract_right() -> std::unique_ptr<FloorPlanGenomeNode> {
        return std::move(this->right);
    }

    [[nodiscard]] auto get_left_p() -> std::unique_ptr<FloorPlanGenomeNode>& {
        return this->left;
    }

    [[nodiscard]] auto get_right_p() -> std::unique_ptr<FloorPlanGenomeNode>& {
        return this->right;
    }

    [[nodiscard]] auto get_split() const {
        return this->split;
    }

    void set_split(Split split) {
        this->split = split;
    }

    [[nodiscard]] auto get_split_dist() const {
        return this->split_dist;
    }

    auto set_split_dist(double split_dist) {
        this->split_dist = split_dist;
    }

    [[nodiscard]] auto get_height() const -> unsigned int override {
        return std::max(this->left->get_height(), this->right->get_height()) + 1;
    }

    [[nodiscard]] auto get_node_count() const -> unsigned int override {
        return this->left->get_node_count() + this->right->get_node_count() + 1;
    }

    [[nodiscard]] auto get_leaf_count() const -> unsigned int override {
        return this->left->get_leaf_count() + this->right->get_leaf_count();
    }

    [[nodiscard]] auto get_split_count() const -> unsigned int override {
        return this->left->get_split_count() + this->right->get_split_count() + 1;
    }
};

class FloorPlanGenomeRoom : public FloorPlanGenomeNode {
private:
    const FPInput::RoomType* room_type;
    unsigned int instance;

public:
    explicit FloorPlanGenomeRoom(const FPInput::RoomType& room_type, unsigned int instance) :
            room_type(&room_type), instance(instance)
    {
        assert(!room_type.name().empty());
    }

    [[nodiscard]] auto clone() const -> std::unique_ptr<FloorPlanGenomeNode> override {
        return std::make_unique<FloorPlanGenomeRoom>(*this->room_type, this->instance);
    }

    [[nodiscard]] auto get_room_type() const -> const FPInput::RoomType& {
        return *this->room_type;
    }

    auto set_room_type(const FPInput::RoomType* rt, unsigned int instance = 1) {
        this->room_type = rt;
        this->instance = instance;
    }

    [[nodiscard]] auto get_instance() const {
        return this->instance;
    }

    void set_instance(unsigned int instance) {
        this->instance = instance;
    }

    explicit operator std::string() const override {
        if (this->instance == 1) return this->room_type->name();
        return this->room_type->name() + " " + std::to_string(this->instance);
    }

    auto operator==(const FloorPlanGenomeNode &obj) const -> bool override {
        try {
            auto& other = dynamic_cast<const FloorPlanGenomeRoom&>(obj);
            return this->room_type == other.room_type;
        } catch (const std::bad_cast &ex) {
            return false;
        }
    }

    [[nodiscard]] auto get_height() const -> unsigned int override {
        return 1;
    }

    [[nodiscard]] auto get_node_count() const -> unsigned int override {
        return 1;
    }

    [[nodiscard]] auto get_leaf_count() const -> unsigned int override {
        return 1;
    }

    [[nodiscard]] auto get_split_count() const -> unsigned int override {
        return 0;
    }
};

class FloorPlanGenome {
private:
    std::unique_ptr<FloorPlanGenomeNode> root;

public:
    FloorPlanGenome() = default;

    explicit FloorPlanGenome(std::unique_ptr<FloorPlanGenomeNode> root) : root(std::move(root)) {
        this->own_root();
    }

    FloorPlanGenome(const FloorPlanGenome& other) : FloorPlanGenome(other.root ? other.root->clone() : nullptr) {}
    FloorPlanGenome(FloorPlanGenome&& other) noexcept {
        using std::swap;
        swap(this->root, other.root);
        this->own_root();
    }

    auto operator=(const FloorPlanGenome& other) -> FloorPlanGenome& {
        this->root = other.root ? other.root->clone() : nullptr;
        this->own_root();
        return *this;
    }

    auto operator=(FloorPlanGenome&& other) noexcept -> FloorPlanGenome& {
        using std::swap;
        swap(this->root, other.root);
        this->own_root();
        return *this;
    }

    auto replace(std::unique_ptr<FloorPlanGenomeNode> root) {
        this->root = std::move(root);
        this->own_root();
    }

    [[nodiscard]] auto get_root() -> FloorPlanGenomeNode& {
        return *this->root;
    }

    [[nodiscard]] auto get_root() const -> const FloorPlanGenomeNode& {
        return *this->root;
    }

    auto get_root_p() -> std::unique_ptr<FloorPlanGenomeNode>& {
        return this->root;
    }

    auto operator==(const FloorPlanGenome& other) const {
        return this == &other || *this->root == *other.root;
    }

    explicit operator std::string() const {
//        return "...";
        if (!this->root) return "no solution";
        return std::string(*this->root);
    }

    template <std::uniform_random_bit_generator R>
    auto get_random_leaf(R& rng) -> FloorPlanGenomeRoom* {
        auto& r = *this->root;
        const auto leaf_count = r.get_leaf_count();
        std::uniform_int_distribution<unsigned int> l_idx_dist(0, leaf_count - 1);
        const auto l_idx = l_idx_dist(rng);
        auto it = FloorPlanIteratorFilter<FloorPlanGenomeNode*, FloorPlanGenomeRoom*>(r.begin());
        for (unsigned int i = 0; i < l_idx; ++i) ++it;
        return *it;
    }

    template <std::uniform_random_bit_generator R>
    auto get_random_split(R& rng) -> FloorPlanGenomeSplit* {
        auto& r = *this->root;
        const auto split_count = r.get_split_count();
        if (split_count == 0) return nullptr;
        std::uniform_int_distribution<unsigned int> l_idx_dist(0, split_count - 1);
        const auto l_idx = l_idx_dist(rng);
        auto it = FloorPlanIteratorFilter<FloorPlanGenomeNode*, FloorPlanGenomeSplit*>(r.begin());
        for (unsigned int i = 0; i < l_idx; ++i) ++it;
        return *it;
    }

    template <std::uniform_random_bit_generator R>
    auto get_random_node(R& rng) -> FloorPlanGenomeNode* {
        auto& r = *this->root;
        const auto node_count = r.get_node_count();
        std::uniform_int_distribution<unsigned int> n_idx_dist(0, node_count - 1);
        const auto n_idx = n_idx_dist(rng);
        auto it = r.begin();
        for (unsigned int i = 0; i < n_idx; ++i) ++it;
        return *it;
    }

private:
    void own_root() {
        if (this->root) {
            this->root->parent = nullptr;
            this->root->container = this;
        }
    }
};

template <typename T>
auto FloorPlanIterator<T>::operator++() -> FloorPlanIterator<T> & {
    if (this->visit_stack.empty()) {
        throw std::runtime_error("Cannot dereference end iterator");
    }

    const auto last = this->visit_stack.back();
    this->visit_stack.pop_back();
    const auto split = dynamic_cast<const FloorPlanGenomeSplit*>(last);
    if (split) {
        this->visit_stack.push_back(split->right.get());
        this->visit_stack.push_back(split->left.get());
    }
    return *this;
}

auto FloorPlanGenomeNode::replace(std::unique_ptr<FloorPlanGenomeNode> replacement) -> void {
    if (!this->parent) {
        if (!this->container) {
            throw std::runtime_error("Cannot replace genome node with no parent or container");
        }

        this->container->replace(std::move(replacement));
        return;
    }

    replacement->container = nullptr;
    replacement->parent = this->parent;
    replacement->left = this->left;
    if (this->left) {
        this->parent->left = std::move(replacement);
    } else {
        this->parent->right = std::move(replacement);
    }
}

auto FloorPlanGenomeNode::get_self_p() -> std::unique_ptr<FloorPlanGenomeNode>& {
    if (this->parent) {
        if (this->is_left()) {
            return this->parent->get_left_p();
        }

        return this->parent->get_right_p();
    }

    return this->container->get_root_p();
}

static_assert(NSGA2::Genome<FloorPlanGenome>, "The floor plan genome is not a valid genome");

}

namespace std {

template <>
struct hash<Quinoa::FloorPlanGenome> {
    auto operator()(const Quinoa::FloorPlanGenome& g) const -> std::size_t {
        return 1099511628211ULL * reinterpret_cast<std::uintptr_t>(&g.get_root());
    }
};

inline std::ostream& operator<<(std::ostream& os, const std::optional<Quinoa::Edge>& v) {
    if (!v) {
        os << "<empty>";
    } else {
        os << '{' << v->length << ", " << v->face << '}';
    }
    return os;
}

}
