#include "FloorPlanGeometry.hpp"
#include "FloorPlanFitness.hpp"
#include "floor-plan-io.hpp"
#include "input-patching.hpp"

#include "output.hpp"

#include "slopt/opt.h"
#include <cairo-svg.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Boolean_set_operations_2.h>

#include <filesystem>
#include <string>
#include <vector>
#include <iostream>
#include <utility>

using namespace Quinoa;
using namespace std::string_literals;

using Kernel = CGAL::Simple_cartesian<double>;
using Polygon_2 = CGAL::Polygon_2<Kernel>;
using Polygon_with_holes_2 = CGAL::Polygon_with_holes_2<Kernel>;

constexpr auto plan_dpi = 72;
constexpr auto m_per_in = .0254;
constexpr auto plan_scale = 1./100;
constexpr auto dots_per_plan_m = (plan_dpi / m_per_in) * plan_scale;
constexpr auto dots_margin = 30;
constexpr auto text_line_height = 1.3;

std::vector<std::filesystem::path> plan_paths;

extern std::optional<std::filesystem::path> input_schema_path;
extern std::optional<std::filesystem::path> output_schema_path;

std::optional<std::filesystem::path> ext_reqs_path;

static slopt_Option options[] = {
        { 'x', "input-schema-path", SLOPT_REQUIRE_ARGUMENT },
        { 'y', "output-schema-path", SLOPT_REQUIRE_ARGUMENT },
        { 'r', "input", SLOPT_REQUIRE_ARGUMENT },
};

static void on_argument(int sw, char snam, const char *lnam, const char *val, [[maybe_unused]] void *pl) {
    switch (sw) {
        case SLOPT_OK1:
        case SLOPT_OK2:
            switch (snam) {
                case 'x':
                    input_schema_path = val;
                    break;
                case 'y':
                    output_schema_path = val;
                    break;
                case 'r':
                    ext_reqs_path = val;
                    break;
                default:
                    throw std::invalid_argument(
                            "Unrecognized option "s + static_cast<char>(sw) + snam
                            + " or " + static_cast<char>(sw) +  static_cast<char>(sw) + lnam
                    );
            }
            break;
        case SLOPT_DIRECT:
            plan_paths.push_back(val);
            break;
        case SLOPT_MISSING_ARGUMENT:
            throw std::invalid_argument("Missing argument for "s + snam + "/" + lnam);
        case SLOPT_UNEXPECTED_ARGUMENT:
            throw std::invalid_argument("Unexpected argument for "s + snam + "/" + lnam);
        case SLOPT_UNKNOWN_LONG_OPTION:
            throw std::invalid_argument("Unknown option "s + lnam);
        case SLOPT_UNKNOWN_SHORT_OPTION:
            throw std::invalid_argument("Unknown option "s + snam);
        default:
            throw std::invalid_argument("Unknown option parser state " + std::to_string(sw));
    }
}

static inline double to_im_a_x(double i) {
    return i * dots_per_plan_m + dots_margin;
}

static inline double to_im_a_y(double i, double im_height) {
    return im_height - (i * dots_per_plan_m + dots_margin);
}

static inline double to_im_r(double i) {
    return i * dots_per_plan_m;
}

// template <typename T>
// static inline auto calc_sq_error(T v, T min, T max) -> T {
//     if (v < min) return (min - v) * (min - v);
//     if (v > max) return (v - max) * (v - max);
//     return 0;
// }

static auto find_bounds(const FPOutput::FloorPlan& plan) -> Rect {
    Rect r;
    for (const auto& floor : plan.Floor()) {
        for (const auto& room : floor.Room()) {
            for (const auto& geom : room.Geometry()) {
                for (const auto& point : geom.Point()) {
                    r.x = std::min(r.x, point.x());
                    r.y = std::min(r.y, point.y());
                    r.w = std::max(r.w, point.x());
                    r.h = std::max(r.h, point.y());
                }
            }
        }
    }

    r.w -= r.x;
    r.h -= r.y;

    return r;
}

int run(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Missing path to plan to render\n");
        return 1;
    }

    slopt_parse(argc - 1, argv + 1, options, on_argument, nullptr);

    xml_schema::Properties props;
    props.schema_location("urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed",
            std::filesystem::absolute(input_schema_path.value_or("input.xsd"))
    );
    props.schema_location("urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4",
            std::filesystem::absolute(output_schema_path.value_or("output.xsd"))
    );

    printf("file;absence;dimension;aspectRatio;relativeArea;externalWall;adjacency;disjointedness;reachability;bounds;total\n");

    for (const auto& plan_path : plan_paths) {
        const auto plan = std::shared_ptr(FPOutput::FloorPlan_(plan_path.c_str(), 0, props));

        Rect bounds;
        std::optional<double> fitness;
        if (plan->Requirements() || ext_reqs_path) {
            std::shared_ptr<FPInput::FloorPlanRequirements> reqs;
            std::shared_ptr<FloorPlanGeometry> fp_geo;
            std::unique_ptr<FloorPlanFitness> ff;
            const FPInput::RoomType* void_type;
            const FPInput::RoomType* out_type;

            if (ext_reqs_path) {
                reqs = std::shared_ptr(FPInput::FloorPlanRequirements_(ext_reqs_path.value(), 0, props));
            } else {
                reqs = std::make_shared<FPInput::FloorPlanRequirements>(*plan->Requirements());
            }

            std::for_each(reqs->RoomTypes().RoomType().begin(), reqs->RoomTypes().RoomType().end(), patch_room_type);

            if (!reqs->RoomAreaRatios()) {
                reqs->RoomAreaRatios(FPInput::RoomAreaRatios());
            }

            if (!reqs->RoomAdjacencies()) {
                reqs->RoomAdjacencies(FPInput::RoomAdjacencies());
            }

            if (!reqs->Objectives()) {
                reqs->Objectives(FPInput::Objectives());
            }

            auto& rars = reqs->RoomAreaRatios()->RoomAreaRatio();
            std::for_each(rars.begin(), rars.end(), patch_room_area_ratio_range);

            {
                FPInput::RoomType void_type_def("<void>");
                void_type_def.Count(FPInput::CountRange());
                void_type_def.Count()->min(0);
                void_type_def.Count()->max("0");
                patch_room_type(void_type_def);

                FPInput::RoomType out_type_def("<out>");
                out_type_def.Count(FPInput::CountRange());
                out_type_def.Count()->min(0);
                patch_room_type(out_type_def);

                reqs->RoomTypes().RoomType().push_back(void_type_def);
                reqs->RoomTypes().RoomType().push_back(out_type_def);
            }

            void_type = &reqs->RoomTypes().RoomType().at(reqs->RoomTypes().RoomType().size() - 2);
            out_type = &reqs->RoomTypes().RoomType().back();

            fp_geo = std::make_shared<FloorPlanGeometry>(reqs, *out_type);
            ff = std::make_unique<FloorPlanFitness>(reqs, *fp_geo, *void_type, *out_type);

            bounds = fp_geo->get_full_plan_rect();

            std::unordered_map<int, double> objective_weights;
            for (const auto& v : FPInput::ObjectiveName::_xsd_ObjectiveName_indexes_) {
                objective_weights.insert(std::make_pair(v, v >= 5 ? 1000.0 : 1.0));
            }

            if (reqs->Objectives()) {
                for (const auto& ob : reqs->Objectives()->Objective()) {
                    objective_weights.insert(std::make_pair(FPInput::ObjectiveName::Value(ob.name()), ob.weight()));
                }
            }

            std::unordered_map<std::string, const FPInput::RoomType*> rts_by_name;
            for (const auto& rt : reqs->RoomTypes().RoomType()) {
                rts_by_name[rt.name()] = &rt;
            }

            unsigned int disjointedness = 0;
            double ar_error = 0;
            const auto& floor = plan->Floor()[0];
            for (const auto& room : floor.Room()) {
                if (room.Geometry().size() > 1) disjointedness += room.Geometry().size() - 1;

                const auto rt = rts_by_name.at(room.type());
                const double rt_min_ar = rt->AspectRatio()->min();
                const double rt_max_ar = std::stod(rt->AspectRatio()->max());

                for (const auto& geom : room.Geometry()) {
                    Polygon_2 poly;
                    for (const auto& point : geom.Point()) {
                        const auto x = round_to_mm(point.x());
                        const auto y = round_to_mm(point.y());

                        poly.push_back({x, y});
                    }

                    const auto bbox = poly.bbox();
                    const auto w = std::max(bbox.xmax() - bbox.xmin(), QUINOA_EPSILON);
                    const auto h = std::max(bbox.ymax() - bbox.ymin(), QUINOA_EPSILON);
                    ar_error += calc_sq_error(std::max(w / h, h / w), rt_min_ar, rt_max_ar);
                }
            }

            auto g = represent_floor_plan(*reqs, out_type, bounds, *plan);
            auto fc = ff->calc_fitness_components(g);
            fitness = fc.absence_error * objective_weights.at(0 /*FPInput::ObjectiveName::absence*/) +
                      fc.dimension_error * objective_weights.at(1 /* FPInput::ObjectiveName::dimension */) +
                      ar_error * objective_weights.at(2 /* FPInput::ObjectiveName::aspectRatio */)  +
                      fc.relative_area_error * objective_weights.at(3 /* FPInput::ObjectiveName::relativeArea */) +
                      fc.external_wall_error * objective_weights.at(4 /* FPInput::ObjectiveName::externalWall */) +
                      fc.adjacency_error * objective_weights.at(5 /* FPInput::ObjectiveName::adjacency */) +
                      disjointedness * objective_weights.at(6) +
                      fc.reachability_error * objective_weights.at(8 /* FPInput::ObjectiveName::reachability */) +
                      fc.out_of_bounds_error * objective_weights.at(9 /* FPInput::ObjectiveName::bounds */);
            printf("%s;%f;%f;%f;%f;%f;%u;%u;%u;%u;%f\n",
                   plan_path.c_str(),
                   fc.absence_error,
                   fc.dimension_error,
                   ar_error,
                   fc.relative_area_error,
                   fc.external_wall_error,
                   fc.adjacency_error,
                   disjointedness,
                   fc.reachability_error,
                   fc.out_of_bounds_error,
                   *fitness
            );
        } else {
            bounds = find_bounds(*plan);
            printf("%s\n", plan_path.c_str());
        }

        std::filesystem::path out_path = plan_path;

        out_path.replace_extension(".svg");
        auto im_width = dots_per_plan_m * bounds.w + 2 * dots_margin;
        auto im_height = dots_per_plan_m * bounds.h + 2 * dots_margin;
        auto surface = cairo_svg_surface_create(out_path.c_str(), im_width, im_height);
        auto im = cairo_create(surface);
        cairo_select_font_face(im, "URW Gothic", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
        cairo_set_font_size(im, 5);

        cairo_new_path(im);
        cairo_rectangle(im, 0, 0, im_width, im_height);
        cairo_set_source_rgb(im, 1, 1, 1);
        cairo_fill(im);

        const auto arr_margin = 3.0;
        const auto arr_dim = dots_margin - 2 * arr_margin;
        const auto arr_dim_half = arr_dim / 2;
        const auto arr_bx = arr_margin;
        const auto arr_by = dots_margin + arr_dim + arr_margin;
        const auto to_rad = 0.0174532925199;
        const auto arr_angle = 39.0 * to_rad;
        const auto arr_lw = 0.55;
        const auto arr_lw_half = arr_lw / 2;

        cairo_set_line_width(im, arr_lw);

        cairo_new_path(im);
        cairo_line_to(im, arr_bx, arr_by);
        cairo_line_to(im, arr_bx + arr_dim_half - arr_lw_half, arr_by - arr_dim_half * std::tan(arr_angle));
        cairo_line_to(im, arr_bx + arr_dim_half - arr_lw_half, arr_by - arr_dim);
        cairo_close_path(im);
        cairo_fill_preserve(im);
        cairo_set_source_rgb(im, 0.4, 0.4, 0.4);;
        cairo_stroke(im);

        cairo_new_path(im);
        cairo_line_to(im, arr_bx + arr_dim, arr_by);
        cairo_line_to(im, arr_bx + arr_dim_half + arr_lw_half, arr_by - arr_dim);
        cairo_line_to(im, arr_bx + arr_dim_half + arr_lw_half, arr_by - arr_dim_half * std::tan(arr_angle));
        cairo_close_path(im);
        cairo_fill_preserve(im);
        cairo_stroke(im);

        cairo_text_extents_t n_extents;
        cairo_text_extents(im, "N", &n_extents);
        cairo_move_to(im, arr_bx + arr_dim_half - n_extents.width / 2, arr_by);
        cairo_show_text(im, "N");

        if (fitness) {
            char fitness_buf[64];
            snprintf(fitness_buf, sizeof(fitness_buf), "Fitness (0 = optimal): %.3f", -*fitness);
//
//            cairo_text_extents_t f_extents;
//            cairo_text_extents(im, fitness_buf, &f_extents);
            cairo_move_to(im, dots_margin, im_height - dots_margin / 2.0);
            cairo_show_text(im, fitness_buf);
        }

        cairo_set_line_width(im, 1.0);

        cairo_new_path(im);
        const auto m1_line_start_x = im_width - dots_margin - to_im_r(1);
        const auto m1_line_end_x = im_width - dots_margin;
        const auto m1_line_y = im_height - dots_margin / 2.0;
        const auto m1_line_stick_out = 3;
        cairo_line_to(im, m1_line_start_x, m1_line_y - m1_line_stick_out);
        cairo_line_to(im, m1_line_start_x, m1_line_y + m1_line_stick_out);
        cairo_line_to(im, m1_line_start_x, m1_line_y);
        cairo_line_to(im, m1_line_end_x, m1_line_y);
        cairo_line_to(im, m1_line_end_x, m1_line_y + m1_line_stick_out);
        cairo_line_to(im, m1_line_end_x, m1_line_y - m1_line_stick_out);
        cairo_stroke(im);

        cairo_set_line_width(im, 1.5);

        const auto m1_text_center_x = im_width - dots_margin - to_im_r(0.5);

        cairo_text_extents_t m1_extents;
        cairo_text_extents(im, "1 m", &m1_extents);
        cairo_move_to(im,
                m1_text_center_x - m1_extents.width / 2,
                m1_line_y + m1_extents.height + m1_line_stick_out / 2.0
        );
        cairo_show_text(im, "1 m");

        cairo_set_line_width(im, 2.0);
        cairo_set_source_rgb(im, 0, 0, 0);

        const auto& floor = plan->Floor()[0];
        for (const auto& room : floor.Room()) {
            for (const auto& geom : room.Geometry()) {
                Polygon_2 poly;

                cairo_new_path(im);

                double cx = 0;
                double cy = 0;

                for (const auto& p : geom.Point()) {
                    const auto x = to_im_a_x(p.x());
                    const auto y = to_im_a_y(p.y(), im_height);
                    cairo_line_to(im, x, y);

                    cx += p.x();
                    cy += p.y();

                    poly.push_back({p.x(), p.y()});
                }

                cairo_close_path(im);
                cairo_stroke(im);

                cx /= geom.Point().size();
                cy /= geom.Point().size();

                const auto area = poly.area();
                auto name = room.type();
                auto inst = room.instance();
                if (inst != 1) {
                    name += " " + std::to_string(inst);
                }

                cairo_text_extents_t extents;
                cairo_text_extents(im, name.c_str(), &extents);

                cairo_text_extents_t area_extents;
                char area_buf[64];
                snprintf(area_buf, sizeof(area_buf), "%.1f m²", area);
                cairo_text_extents(im, area_buf, &area_extents);

                cairo_move_to(im, to_im_a_x(cx) - extents.width / 2, to_im_a_y(cy, im_height) + extents.height / 2);
                cairo_show_text(im, name.c_str());

                cairo_move_to(im,
                        to_im_a_x(cx) - area_extents.width / 2,
                        to_im_a_y(cy, im_height) + extents.height * text_line_height + area_extents.height / 2
                );
                cairo_show_text(im, area_buf);
            }
        }

        cairo_destroy(im);
        cairo_surface_destroy(surface);
    }

    return 0;
}

int main(int argc, char **argv) {
    try {
        run(argc, argv);
    } catch (const xml_schema::Exception& ex) {
        std::cerr << "Caught XML exception:" << std::endl << ex << std::endl;
    }
}
