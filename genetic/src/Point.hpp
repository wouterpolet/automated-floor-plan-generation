#pragma once

#include "CGAL.hpp"

#include <ostream>
#include <utility>

namespace Quinoa {

struct Point {
    double x = 0;
    double y = 0;

    Point() = default;
    Point(double x, double y) : x(x), y(y) {}
    Point(const Point& other) = default;
    explicit Point(const Point_2& other): x(other.x()), y(other.y()) {}

    friend auto operator<<(std::ostream& os, const Point& p) -> std::ostream& {
        os << '(' << p.x << "; " << p.y << ')';
        return os;
    }

    [[nodiscard("pure")]] auto is_in_poly(const Polygon_2& poly) const {
        return Quinoa::is_in_poly(poly, {this->x, this->y});
    }

    explicit operator Point_2() const {
        return Point_2{this->x, this->y};
    }
};

}

namespace std {

template <>
struct hash<Quinoa::Point> {
    auto operator()(const Quinoa::Point& p) const -> std::size_t {
        const std::hash<double> hd;
        return 1099511628211ULL * (hd(p.x) ^ hd(p.y));
    }
};

}
