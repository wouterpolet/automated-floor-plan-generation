#include "floor-plan-io.hpp"
#include "RoomMetrics.hpp"
#include "util.hpp"
#include "partition-rectilinear-polygon.hpp"
#include "FloorPlanGeometry.hpp"

#include "output.hpp"

#include "CGAL.hpp"

#include <iostream>
#include <utility>
#include <string>
#include <unordered_map>

using namespace Quinoa;
using namespace std::string_literals;
using namespace std::ranges;

std::optional<std::filesystem::path> input_schema_path = "input.xsd";
std::optional<std::filesystem::path> output_schema_path = "output.xsd";

void Quinoa::save_floor_plan(
        const FloorPlanGenome& plan,
        const FloorPlanGeometry& fp_geo,
        std::optional<std::shared_ptr<FPInput::FloorPlanRequirements>> reqs,
        const FPInput::RoomType* out_type,
        const std::filesystem::path& path
) {
    FPOutput::FloorPlan fp;
    if (reqs) {
        fp.Requirements(**reqs);
    }

    const auto& bb = fp_geo.get_full_plan_rect();

    MetricsMap metrics_map;
    collect_metrics(metrics_map, plan.get_root(), bb);

    FPOutput::Floor floor;

    for (const auto& p : metrics_map) {
        const auto& rt = p.first.first;
        const auto inst = p.first.second;
        const auto& metrics = p.second;

        if (rt == out_type) continue;

        FPOutput::Room room(rt->name());
        room.instance(inst);

        std::vector<Polygon_2> rect_polys;
        std::transform(metrics.rects.begin(), metrics.rects.end(), std::back_inserter(rect_polys), [](const auto& r) {
            const auto [p1, p2, p3, p4] = r.grown_slightly().to_points();
            Polygon_2 poly;
            poly.push_back(Point_2(p1));
            poly.push_back(Point_2(p2));
            poly.push_back(Point_2(p3));
            poly.push_back(Point_2(p4));
            return poly;
        });

        std::vector<Polygon_with_holes_2> union_polys;
        CGAL::join(rect_polys.begin(), rect_polys.end(), std::back_inserter(union_polys));

        for (const auto& poly : union_polys) {
            const auto& poly_bound = poly.outer_boundary();

            std::vector<Point_2> bound;
            transform(poly_bound, std::back_inserter(bound), [](const auto& p) {
                return Point_2(round_to_mm(p.x()), round_to_mm(p.y()));
            });
            auto unique_it = unique(bound);
            bound.erase(unique_it.begin(), unique_it.end());

            if (bound.front() == bound.back()) bound.pop_back();

            FPInput::PointList points;
            std::transform(bound.begin(), bound.end(), std::back_inserter(points.Point()), [](const auto& p) {
                FPInput::Point cip(round_to_mm(p.x()), round_to_mm(p.y()));
                return cip;
            });

            room.Geometry().push_back(points);
        }

        floor.Room().push_back(room);
    }

    if constexpr (false) {
        FPOutput::Room room("<true out>");
        room.instance(1);

        std::vector<Polygon_2> rect_polys;
        std::transform(fp_geo.get_out_rects().begin(), fp_geo.get_out_rects().end(), std::back_inserter(rect_polys), [](const auto& r) {
            const auto [p1, p2, p3, p4] = r.grown_slightly().to_points();
            Polygon_2 poly;
            poly.push_back(Point_2(p1));
            poly.push_back(Point_2(p2));
            poly.push_back(Point_2(p3));
            poly.push_back(Point_2(p4));
            return poly;
        });


        for (const auto& poly_bound : rect_polys) {
            std::vector<Point_2> bound;
            transform(poly_bound, std::back_inserter(bound), [](const auto& p) {
                return Point_2(round_to_mm(p.x()), round_to_mm(p.y()));
            });
            auto unique_it = unique(bound);
            bound.erase(unique_it.begin(), unique_it.end());

            FPInput::PointList points;
            std::transform(bound.begin(), bound.end(), std::back_inserter(points.Point()), [](const auto& p) {
                FPInput::Point cip(round_to_mm(p.x()), round_to_mm(p.y()));
                return cip;
            });

            room.Geometry().push_back(points);
        }

        floor.Room().push_back(room);
    }

    fp.Floor().push_back(floor);

    xml_schema::NamespaceInfomap ns_info;
    ns_info["fpi"] = {"urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed", "input.xsd"};
    ns_info["fpo"] = {"urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4", "output.xsd"};

    std::ofstream ofs(path);
    assert(ofs.is_open());
    FPOutput::FloorPlan_(ofs, fp, ns_info);
    ofs.close();
}

using RoomInst = std::pair<const FPInput::RoomType*, unsigned int>;
using SimpleRoomPolygon = std::pair<Polygon_2, RoomInst>;

static std::unique_ptr<FloorPlanGenomeNode> gen_node(
        const FPInput::RoomType* out_type,
        const Rect& bbox,
        const std::vector<Segment_2>& horiz_edges,
        const std::vector<Segment_2>& vert_edges,
        const std::vector<SimpleRoomPolygon>& room_polys
) {
    for (const auto& room_poly : room_polys) {
        const auto& poly = room_poly.first;
        const auto& room_inst = room_poly.second;

        if (bbox.is_in_poly(poly)) {
            return std::make_unique<FloorPlanGenomeRoom>(*room_inst.first, room_inst.second);
        }
    }

    if (!horiz_edges.empty() || !vert_edges.empty()) {
        // Generate split
        if (horiz_edges.size() >= vert_edges.size()) {
            const auto& edge = horiz_edges.at(horiz_edges.size() / 2);
            const auto y = edge.start().y();

            const auto ry = (y - bbox.y) / bbox.h;

            std::vector<Segment_2> upper_horiz_edges;
            std::vector<Segment_2> lower_horiz_edges;
            std::vector<Segment_2> upper_vert_edges;
            std::vector<Segment_2> lower_vert_edges;
//            std::vector<SimpleRoomPolygon> upper_room_polys;
//            std::vector<SimpleRoomPolygon> lower_room_polys;

            for (const auto& e : horiz_edges) {
                if (&e == &edge) continue;

                if (e.start().y() >= y) upper_horiz_edges.push_back(e);
                if (e.start().y() <= y) lower_horiz_edges.push_back(e);
            }

            for (const auto& e : vert_edges) {
                if (std::max(e.start().y(), e.end().y()) >= y) upper_vert_edges.push_back(e);
                if (std::min(e.start().y(), e.end().y()) <= y) lower_vert_edges.push_back(e);
            }

            const auto bboxes_split = bbox.split(Split::VERTICAL, ry);
            const auto& upper_bbox = bboxes_split.second;
            const auto& lower_bbox = bboxes_split.first;

//            for (const auto& room_poly : room_polys) {
//                const auto& poly = room_poly.first;
//                if (upper_bbox.encloses_poly(poly)) upper_room_polys.push_back(room_poly);
//                if (lower_bbox.encloses_poly(poly)) lower_room_polys.push_back(room_poly);
//            }

            return std::make_unique<FloorPlanGenomeSplit>(
                    Split::VERTICAL,
                    ry,
                    gen_node(out_type, lower_bbox, lower_horiz_edges, lower_vert_edges, room_polys),
                    gen_node(out_type, upper_bbox, upper_horiz_edges, upper_vert_edges, room_polys)
            );
        } else {
            const auto& edge = vert_edges.at(vert_edges.size() / 2);
            const auto x = edge.start().x();

            const auto rx = (x - bbox.x) / bbox.w;

            std::vector<Segment_2> right_horiz_edges;
            std::vector<Segment_2> left_horiz_edges;
            std::vector<Segment_2> right_vert_edges;
            std::vector<Segment_2> left_vert_edges;
//            std::vector<SimpleRoomPolygon> right_room_polys;
//            std::vector<SimpleRoomPolygon> left_room_polys;

            for (const auto& e : horiz_edges) {
                if (std::max(e.start().x(), e.end().x()) >= x) right_horiz_edges.push_back(e);
                if (std::min(e.start().x(), e.end().x()) <= x) left_horiz_edges.push_back(e);
            }

            for (const auto& e : vert_edges) {
                if (&e == &edge) continue;

                if (e.start().x() >= x) right_vert_edges.push_back(e);
                if (e.start().x() <= x) left_vert_edges.push_back(e);
            }

            const auto bboxes_split = bbox.split(Split::HORIZONTAL, rx);
            const auto& right_bbox = bboxes_split.second;
            const auto& left_bbox = bboxes_split.first;

//            for (const auto& room_poly : room_polys) {
//                const auto& poly = room_poly.first;
//                if (right_bbox.encloses_poly(poly)) right_room_polys.push_back(room_poly);
//                if (left_bbox.encloses_poly(poly)) left_room_polys.push_back(room_poly);
//            }

            return std::make_unique<FloorPlanGenomeSplit>(
                    Split::HORIZONTAL,
                    rx,
                    gen_node(out_type, left_bbox, left_horiz_edges, left_vert_edges, room_polys),
                    gen_node(out_type, right_bbox, right_horiz_edges, right_vert_edges, room_polys)
            );
        }
    }

    const FPInput::RoomType* assigned_room_type = out_type;
    unsigned int assigned_instance = 1;

    for (const auto& room_poly : room_polys) {
        const auto& poly = room_poly.first;
        const auto& room_inst = room_poly.second;

        if (bbox.is_in_poly(poly)) {
            assigned_room_type = room_inst.first;
            assigned_instance = room_inst.second;
            break;
        }
    }

    return std::make_unique<FloorPlanGenomeRoom>(*assigned_room_type, assigned_instance);
}

FloorPlanGenome Quinoa::load_floor_plan(
        const FPInput::FloorPlanRequirements& reqs,
        const FPInput::RoomType* out_type,
        const Rect& bounds,
        const std::filesystem::path &path
) {
    xml_schema::Properties props;
    props.schema_location("urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed",
            std::filesystem::absolute(input_schema_path.value_or("input.xsd"))
    );
    props.schema_location("urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4",
            std::filesystem::absolute(output_schema_path.value_or("output.xsd"))
    );

    std::unique_ptr<FPOutput::FloorPlan> fp(FPOutput::FloorPlan_(path, 0, props));

    return represent_floor_plan(reqs, out_type, bounds, *fp);
}

FloorPlanGenome Quinoa::represent_floor_plan(
        const FPInput::FloorPlanRequirements& reqs,
        const FPInput::RoomType* out_type,
        const Rect& bounds,
        const FPOutput::FloorPlan& fp
) {
    std::unordered_map<std::string, const FPInput::RoomType*> room_types_by_name;
    for (const auto& type : reqs.RoomTypes().RoomType()) {
        room_types_by_name.insert(std::make_pair(std::string(type.name()), &type));
    }

    std::unordered_map<const FPOutput::Room*, std::vector<Polygon_2>> room_polys;
    std::vector<std::pair<Polygon_2, std::pair<const FPInput::RoomType*, unsigned int>>> room_polys_named;
    for (const auto& r : fp.Floor()[0].Room()) {
        // The convex partitioner uses a slightly different polygon type...
        std::vector<Polygon_2> polys;
        for (const auto& geom : r.Geometry()) {
            Polygon_2 poly;
            for (const auto& point : geom.Point()) {
                const auto x = round_to_mm(point.x());
                const auto y = round_to_mm(point.y());

                poly.push_back({x, y});
            }
            if (poly.container().front() == poly.container().back()) poly.container().pop_back();
            if (!poly.is_simple()) {
                [[unlikely]]
                throw std::runtime_error("Room " + r.type() + " " + std::to_string(r.instance()) + " has strange geometry");
            }
            partition_rectilinear_polygon(poly, std::back_inserter(polys));
        }

        room_polys.insert(std::make_pair(&r, polys));
    }

    for (const auto& p : room_polys) {
        for (const auto& poly : p.second) {
            room_polys_named.emplace_back(poly,
                    std::make_pair(room_types_by_name.at(p.first->type()), p.first->instance())
            );
        }
    }

    std::vector<Segment_2> horiz_edges;
    std::vector<Segment_2> vert_edges;

    // Partition all polygons into horizontal and vertical edges
    for (const auto& r_poly_set : room_polys) {
        for (const auto& poly : r_poly_set.second) {
            for (auto it = poly.edges_begin(); it != poly.edges_end(); ++it) {
                const auto& e = *it;

                if (e.is_horizontal()) horiz_edges.push_back(e);
                else if (e.is_vertical()) vert_edges.push_back(e);
                else {
                    throw std::runtime_error(
                            "Room "s
                            + r_poly_set.first->type()
                            + " "
                            + std::to_string(r_poly_set.first->instance())
                            + " has a non-rectilinear geometry (it contains an angled edge)"
                    );
                }
            }
        }
    }

    // Remove outer edges
    std::erase_if(horiz_edges, [&bounds](const auto& e) {
        return e.start().y() <= bounds.y || e.start().y() >= bounds.y + bounds.h;
    });
    std::erase_if(vert_edges, [&bounds](const auto& e) {
        return e.start().x() <= bounds.x || e.start().x() >= bounds.x + bounds.w;
    });

//    // Sort increasing by perpendicular axis, remove duplicates
//    sort(horiz_edges, [](const auto& ea, const auto& eb) {
//        return ea.start().y() < eb.start().y() || (ea.start().y() == eb.start().y() && ea.start().x() < eb.start().x());
//    });
//
//    std::cout << "Horiz edges" << std::endl;
//    for (const auto& e : horiz_edges) {
//        std::cout << e << std::endl;
//    }
//
//    auto horiz_edges_dedup_it = unique(horiz_edges);
//    horiz_edges.erase(horiz_edges_dedup_it.begin(), horiz_edges_dedup_it.end());
//
//    sort(vert_edges, [](const auto& ea, const auto& eb) {
//        return ea.start().x() < eb.start().x() || (ea.start().x() == eb.start().x() && ea.start().y() < eb.start().y());
//    });
//
//    std::cout << "Vert edges" << std::endl;
//    for (const auto& e : vert_edges) {
//        std::cout << e << std::endl;
//    }
//
//    auto vert_edges_dedup_it = unique(vert_edges);
//    vert_edges.erase(vert_edges_dedup_it.begin(), vert_edges_dedup_it.end());

    std::unordered_map<double, Segment_2> horiz_edges_merged;
    std::unordered_map<double, Segment_2> vert_edges_merged;

    for (const auto& e : horiz_edges) {
        const auto y = e.start().y();

        auto it = horiz_edges_merged.find(y);
        if (it == horiz_edges_merged.end()) {
            horiz_edges_merged.insert({y, e});
            continue;
        }

        auto& oe = it->second;
        Segment_2 ne(
                Point_2(std::min({e.start().x(), e.end().x(), oe.start().x(), oe.end().x()}), y),
                Point_2(std::max({e.start().x(), e.end().x(), oe.start().x(), oe.end().x()}), y)
        );

        horiz_edges_merged[y] = ne;
    }

    for (const auto& e : vert_edges) {
        const auto x = e.start().x();

        auto it = vert_edges_merged.find(x);
        if (it == vert_edges_merged.end()) {
            vert_edges_merged.insert({x, e});
            continue;
        }

        auto& oe = it->second;
        Segment_2 ne(
                Point_2(x, std::min({e.start().y(), e.end().y(), oe.start().y(), oe.end().y()})),
                Point_2(x, std::max({e.start().y(), e.end().y(), oe.start().y(), oe.end().y()}))
        );

        vert_edges_merged[x] = ne;
    }

    std::vector<Segment_2> horiz_edges_final;
    horiz_edges_final.reserve(horiz_edges_merged.size());
    std::vector<Segment_2> vert_edges_final;
    vert_edges_final.reserve(vert_edges_merged.size());

    transform(horiz_edges_merged, std::back_inserter(horiz_edges_final), [](const auto& p) { return p.second; });
    transform(vert_edges_merged, std::back_inserter(vert_edges_final), [](const auto& p) { return p.second; });

    return FloorPlanGenome(gen_node(out_type, bounds, horiz_edges_final, vert_edges_final, room_polys_named));
}
