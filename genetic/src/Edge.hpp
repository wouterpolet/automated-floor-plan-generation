#pragma once

#include "CardinalDirection.hpp"
#include "util.hpp"

#include <cmath>
#include <ostream>

namespace Quinoa {

struct Edge {
    double length;
    CardinalDirection face;

    friend auto operator<<(std::ostream& os, const Edge& e) -> std::ostream& {
        os << '{' << e.length << ", " << e.face << '}';
        return os;
    }

    [[nodiscard]] auto operator==(const Edge& o) const -> bool {
        return this->face == o.face && fleq(this->length, o.length);
    }
};

}
