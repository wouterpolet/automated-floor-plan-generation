#pragma once

#include "FloorPlanGenome.hpp"
#include "HyperparamSupport.hpp"
#include "util.hpp"

#include "NSGA2.hpp"

#include <cstdint>

namespace Quinoa {

enum class MutationType {
    INSERT_LEAF,
    REMOVE_LEAF,
    CHANGE_LEAF,
    CHANGE_NODE,
    ROTATE_SUBTREE,
    REFLECT_SUBTREE,
    SWAP_SUBTREES,
    SWAP_ROOMS,
    last
};

auto operator++(MutationType& type) -> MutationType {
    return type = static_cast<MutationType>(static_cast<std::underlying_type<MutationType>::type>(type) + 1);
}

template <std::size_t D, std::uniform_random_bit_generator R>
class FloorPlanMutation {
private:
    std::shared_ptr<R> rng;
    std::shared_ptr<FPInput::FloorPlanRequirements> reqs;

    const FPInput::RoomType* void_type;
    const FPInput::RoomType* out_type;

    double mutation_chance;
    std::vector<MutationType> mut_type_occ_list;

    using Indiv = NSGA2::Individual<D, FloorPlanGenome>;
public:
    FloorPlanMutation(
            std::shared_ptr<R> rng,
            std::shared_ptr<FPInput::FloorPlanRequirements> reqs,
            const FPInput::RoomType* void_type,
            const FPInput::RoomType* out_type,
            double mutation_chance = MUTATION_CHANCE,
            const std::unordered_map<MutationType, unsigned int>& mut_type_occ_map = {}
    ) :
            rng(std::move(rng)),
            reqs(std::move(reqs)),
            void_type(void_type),
            out_type(out_type),
            mutation_chance(mutation_chance)
    {
        for (auto type = MutationType::INSERT_LEAF; type < MutationType::last; ++type) {
            if (!mut_type_occ_map.contains(type)) {
                this->mut_type_occ_list.push_back(type);
                continue;
            }

            for (unsigned int i = 0; i < mut_type_occ_map.at(type); ++i) {
                this->mut_type_occ_list.push_back(type);
            }
        }
    }

    void operator()(NSGA2::Population<D, FloorPlanGenome>& population) {
        std::uniform_real_distribution<double> chance_dist;
        std::uniform_int_distribution<unsigned int> type_dist(0, this->mut_type_occ_list.size() - 1);
        for (auto& indiv : population) {
            assert(indiv.g.get_root().has_parent() ^ indiv.g.get_root().has_container());

            if (chance_dist(*this->rng) > this->mutation_chance) {
                continue;
            }

            const auto& type = this->mut_type_occ_list[type_dist(*this->rng)];
            switch (type) {
                case MutationType::INSERT_LEAF:
                    insert_leaf(indiv);
                    break;
                case MutationType::REMOVE_LEAF:
                    remove_leaf(indiv);
                    break;
                case MutationType::CHANGE_LEAF:
                    change_leaf(indiv);
                    break;
                case MutationType::CHANGE_NODE:
                    change_node(indiv);
                    break;
                case MutationType::ROTATE_SUBTREE:
                    rotate_subtree(indiv);
                    break;
                case MutationType::REFLECT_SUBTREE:
                    reflect_subtree(indiv);
                    break;
                case MutationType::SWAP_SUBTREES:
                    swap_subtrees(indiv);
                    break;
                case MutationType::SWAP_ROOMS:
                    swap_rooms(indiv);
                    break;
                default:
                    QUINOA_UNREACHABLE
            }

            assert(indiv.g.get_root().has_parent() ^ indiv.g.get_root().has_container());
        }
    }

private:
    void insert_leaf(Indiv& indiv) {
        const auto split_dist = gen_split_dist(*this->rng);
        const auto split = gen_bool(*this->rng) ? Split::HORIZONTAL : Split::VERTICAL;
        const auto move_to_left = gen_bool(*this->rng);

        auto n = indiv.g.template get_random_node(*this->rng);

        const auto rtp = this->get_random_room_type();

        std::unique_ptr<FloorPlanGenomeNode> rnode = std::make_unique<FloorPlanGenomeRoom>(rtp.first, rtp.second);
        std::unique_ptr<FloorPlanGenomeNode> nnode = std::make_unique<FloorPlanGenomeSplit>(
                split, split_dist, move_to_left ? std::move(rnode) : n->clone(), move_to_left ? n->clone() : std::move(rnode)
        );

        n->replace(std::move(nnode));
    }

    void remove_leaf(Indiv& indiv) {
        auto l = indiv.g.get_random_leaf(*this->rng);
        if (!l->has_parent()) return;

        const auto p = l->get_parent();
        p->replace(l->is_left() ? p->extract_right() : p->extract_left());
    }

    void change_leaf(Indiv& indiv) {
        auto l = indiv.g.get_random_leaf(*this->rng);
        const auto rtp = this->get_random_room_type();

        l->set_room_type(&rtp.first, rtp.second);
    }

    void change_node(Indiv& indiv) {
        auto s = indiv.g.get_random_split(*this->rng);
        if (!s) return;
        if (gen_bool(*this->rng)) {
            s->set_split(s->get_split() == Split::HORIZONTAL ? Split::VERTICAL : Split::HORIZONTAL);
        } else {
            s->set_split_dist(gen_split_dist_mut(*this->rng, s->get_split_dist()));
        }
    }

    void rotate_subtree(Indiv& indiv) {
        auto s = indiv.g.get_random_split(*this->rng);
        if (!s) return;

        std::uniform_int_distribution<unsigned int> rot_dist(1, 3);
        const auto rot = rot_dist(*this->rng) * 90;
        switch (rot) {
            case 90:
                if (s->get_split() == Split::HORIZONTAL) {
                    s->set_split(Split::VERTICAL);
                    s->set_split_dist(1 - s->get_split_dist());
                } else {
                    s->set_split(Split::HORIZONTAL);
                }
                break;
            case 180:
                s->set_split_dist(1 - s->get_split_dist());
                break;
            case 270:
                if (s->get_split() == Split::HORIZONTAL) {
                    s->set_split(Split::VERTICAL);
                } else {
                    s->set_split(Split::HORIZONTAL);
                    s->set_split_dist(1 - s->get_split_dist());
                }
                break;
            default:
                QUINOA_UNREACHABLE
        }
    }

    void reflect_subtree(Indiv& indiv) {
        auto s = indiv.g.get_random_split(*this->rng);
        if (!s) return;

        auto reflect_h = false;
        auto reflect_v = false;
        while (!reflect_h && !reflect_v) {
            reflect_h = gen_bool(*this->rng);
            reflect_v = gen_bool(*this->rng);
        }

        using std::swap;

        auto it = FloorPlanIteratorFilter<FloorPlanGenomeNode*, FloorPlanGenomeSplit*>(s->begin());
        FloorPlanIteratorFilter<FloorPlanGenomeNode*, FloorPlanGenomeSplit*> end;
        for (; it != end; ++it) {
            const auto& n = *it;
            if ((n->get_split() == Split::HORIZONTAL && reflect_h) || (n->get_split() == Split::VERTICAL && reflect_v)) {
                swap(n->get_left_p(), n->get_right_p());
                n->get_left_p()->set_left(true);
                n->get_right_p()->set_left(false);
            }
        }
    }

    void swap_subtrees(Indiv& indiv) {
        // TODO: solve memory leak and funky trees :(

        auto a = indiv.g.get_random_split(*this->rng);
        auto b = indiv.g.get_random_split(*this->rng);
        if (!a || !b) return;

        a->swap(*b);
    }

    void swap_rooms(Indiv& indiv) {
        auto rt_a = this->get_random_room_type();
        auto rt_b = this->get_random_room_type();

        std::uniform_int_distribution<unsigned int> rt_a_inst_dist(1, std::stoul(rt_a.first.Count()->max()));
        std::uniform_int_distribution<unsigned int> rt_b_inst_dist(1, std::stoul(rt_b.first.Count()->max()));

        std::vector<FloorPlanGenomeRoom*> a_rooms;
        std::vector<FloorPlanGenomeRoom*> b_rooms;

        auto& r = indiv.g.get_root();
        auto it = FloorPlanIteratorFilter<FloorPlanGenomeNode*, FloorPlanGenomeRoom*>(r.begin());
        FloorPlanIteratorFilter<FloorPlanGenomeNode*, FloorPlanGenomeRoom*> end;
        for (; it != end; ++it) {
            if ((*it)->get_room_type() == rt_a.first) a_rooms.push_back(*it);
            else if ((*it)->get_room_type() == rt_b.first) b_rooms.push_back(*it);
        }

        for (auto p : a_rooms) {
            p->set_room_type(&rt_b.first, rt_b_inst_dist(*this->rng));
        }

        for (auto p : b_rooms) {
            p->set_room_type(&rt_a.first, rt_a_inst_dist(*this->rng));
        }
    }

    auto get_random_room_type() const -> std::pair<const FPInput::RoomType&, unsigned int> {
        std::uniform_int_distribution<unsigned int> rt_idx_dist(0, this->reqs->RoomTypes().RoomType().size() - 1);
        const auto rt_idx = rt_idx_dist(*this->rng);
        const auto& rt = this->reqs->RoomTypes().RoomType()[rt_idx];

        if (&rt == this->void_type || &rt == this->out_type) {
            return this->get_random_room_type();
        }

        std::uniform_int_distribution<unsigned int> rt_inst_dist(1, std::stoul(rt.Count()->max()));
        const auto inst = rt_inst_dist(*this->rng);

        return { rt, inst };
    }
};

}
