#pragma once

#include "FloorPlanGenome.hpp"
#include "FloorPlanGeometry.hpp"
#include "RoomMetrics.hpp"

#include "NSGA2.hpp"
#include "input.hpp"

#include <deque>
#include <unordered_map>
#include <unordered_set>
#include <memory>

constexpr auto D = 10;

namespace {

template <typename T>
inline auto calc_sq_error(T v, T min, T max) -> T {
    if (v < min) return (min - v) * (min - v);
    if (v > max) return (v - max) * (v - max);
    return 0;
}

template <typename T>
inline auto calc_abs_error(T v, T min, T max) -> T {
    if (v < min) return min - v;
    if (v > max) return v - max;
    return 0;
}

}

namespace Quinoa {

struct FitnessComponents {
    double absence_error = 0;
    double dimension_error = 0;
    double aspect_ratio_error = 0;
    double relative_area_error = 0;
    double external_wall_error = 0;
    unsigned int adjacency_error = 0;
    unsigned int disjointedness = 0;
    unsigned int strange_shape_error = 0;
    unsigned int reachability_error = 0;
    unsigned int out_of_bounds_error = 0;
};

class FloorPlanFitness {
private:
    std::shared_ptr<FPInput::FloorPlanRequirements> reqs;
    const FloorPlanGeometry& fp_geo;

    std::unordered_map<std::string, const FPInput::RoomType*> rts_by_name;

    std::unordered_map<const FPInput::RoomType*, std::unordered_set<const FPInput::RoomType*>> adjacency_reqs;
    std::unordered_map<const FPInput::RoomType*, std::unordered_map<const FPInput::RoomType*, std::pair<double, double>>> area_ratio_reqs;

    std::unordered_set<const FPInput::RoomType*> main_room_types;
    const FPInput::RoomType& void_type;
    const FPInput::RoomType& out_type;

public:
    FloorPlanFitness(
            std::shared_ptr<FPInput::FloorPlanRequirements> reqs,
            const FloorPlanGeometry& fp_geo,
            const FPInput::RoomType& void_type,
            const FPInput::RoomType& out_type
    ) :
            reqs(std::move(reqs)),
            fp_geo(fp_geo),
            void_type(void_type),
            out_type(out_type)
    {
        for (const auto& rt : this->reqs->RoomTypes().RoomType()) {
            this->rts_by_name[rt.name()] = &rt;
            this->adjacency_reqs[&rt].reserve(this->reqs->RoomTypes().RoomType().size());

            if (rt.main()) this->main_room_types.insert(&rt);
        }

        if (this->main_room_types.empty()) {
            for (const auto& rt : this->reqs->RoomTypes().RoomType()) {
                if (rt.name().find("living") != std::string::npos
                || rt.name().find("Living") != std::string::npos
                || rt.name().find("Hall") != std::string::npos
                || rt.name().find("hall") != std::string::npos) {
                    this->main_room_types.insert(&rt);
                }
            }
        }

        for (const auto& adj : this->reqs->RoomAdjacencies()->RoomAdjacency()) {
            const auto rt_a = this->rts_by_name.at(adj.roomA());
            const auto rt_b = this->rts_by_name.at(adj.roomB());
            this->adjacency_reqs[rt_a].insert(rt_b);
            this->adjacency_reqs[rt_b].insert(rt_a);
        }

        for (const auto& rar : this->reqs->RoomAreaRatios()->RoomAreaRatio()) {
            const auto min = static_cast<double>(rar.min());
            const auto max = std::stod(rar.max());

            const auto rt_a = this->rts_by_name.at(rar.roomA());
            const auto rt_b = this->rts_by_name.at(rar.roomB());
            this->area_ratio_reqs[rt_a][rt_b] = std::make_pair(min, max);
            this->area_ratio_reqs[rt_b][rt_a] = std::make_pair(1 / max, 1 / min);
        }
    }

    auto calc_fitness_components(const FloorPlanGenome& g) const {
        MetricsMap metrics;
        collect_metrics(metrics, g.get_root(), this->fp_geo.get_full_plan_rect());

        std::unordered_map<const FPInput::RoomType*, std::unordered_set<unsigned int>> rt_occs;
        for (const auto& rt : this->reqs->RoomTypes().RoomType()) {
            rt_occs[&rt] = {};
        }

        FitnessComponents fc;

        std::vector<std::pair<RoomInst, RoomMetrics*>> metrics_v;
        metrics_v.reserve(metrics.size());
        std::transform(metrics.begin(), metrics.end(), std::back_inserter(metrics_v), [](auto& p) {
            return std::make_pair(p.first, &p.second);
        });

        for (unsigned int ip = 0; ip < metrics_v.size(); ++ip) {
            auto& p = metrics_v[ip];
            const auto& r = p.first;
            const auto rt = r.first;
            const auto ii = r.second;

            rt_occs[rt].insert(ii);

            const double rt_min_area = rt->Area()->min();
            const double rt_max_area = std::stod(rt->Area()->max());
            const double rt_min_len = rt->Length()->min();
            const double rt_max_len = std::stod(rt->Length()->max());
            const double rt_min_width = rt->Width()->min();
            const double rt_max_width = std::stod(rt->Width()->max());
            const double rt_min_ar = rt->AspectRatio()->min();
            const double rt_max_ar = std::stod(rt->AspectRatio()->max());

            auto& m = *p.second;
            fc.dimension_error += calc_sq_error(m.area, rt_min_area, rt_max_area);

            const auto w = std::max(m.max_x - m.min_x, QUINOA_EPSILON);
            fc.dimension_error += calc_sq_error(w, rt_min_width, rt_max_width);

            const auto h = std::max(m.max_y - m.min_y, QUINOA_EPSILON);
            fc.dimension_error += calc_sq_error(h, rt_min_len, rt_max_len);

            fc.aspect_ratio_error += calc_sq_error(std::max(w / h, h / w), rt_min_ar, rt_max_ar);

            for (unsigned int i = 0; i < m.rects.size(); ++i) {
                const auto& ri = m.rects[i];

                const auto riw = std::max(ri.w, QUINOA_EPSILON);
                const auto rih = std::max(ri.h, QUINOA_EPSILON);

                fc.aspect_ratio_error += calc_sq_error(std::max(riw / rih, rih / riw), rt_min_ar, rt_max_ar);

                if (rt != &this->out_type && rt != &this->void_type) {
                    for (unsigned int k = i + 1; k < m.rects.size(); ++k) {
                        const auto& rk = m.rects[k];
                        auto e = ri.get_touching_length(rk);
                        if (!e || e->length < 0.79) ++fc.disjointedness;
                    }
                }

                if (rt == &this->out_type) {
                    bool at_edge = false;
                    for (const auto& outer_rect : this->fp_geo.get_out_rects()) {
                        if (ri.is_in_rect(outer_rect)) {
                            at_edge = true;
                            break;
                        }
                    }
                    if (!at_edge) ++fc.out_of_bounds_error;
                } else {
                    for (const auto& outer_rect : this->fp_geo.get_edge_rects_any_side()) {
                        if (ri.shrunk_slightly().overlaps(outer_rect)) {
                            ++fc.out_of_bounds_error;
                        }
                    }
                }

            }

            if (rt->ExternalWall()) {
                const auto& ewc = *rt->ExternalWall();

                std::unordered_map<CardinalDirection, double> outer_wall_lengths;
                double owl_total = 0;
                for (const auto& side_rect_p : fp_geo.get_edge_rects()) {
                    const auto& cd = side_rect_p.first;
                    const auto& outer_rect = side_rect_p.second;

                    if (!outer_wall_lengths.contains(cd)) outer_wall_lengths.insert({cd, 0.0});

                    for (const auto& rect : m.rects) {
                        // Skip degenerate rects
                        if (rect.w < QUINOA_EPSILON || rect.h < QUINOA_EPSILON) continue;

                        const auto ol_cd = rect.get_touching_length(outer_rect);
                        if (!ol_cd) continue;

                        if (ol_cd->face != cd) {
                            [[unlikely]]
                            throw std::runtime_error("Bad touching length cd");
                        }

                        outer_wall_lengths[cd] += ol_cd->length;
                        owl_total += ol_cd->length;
                    }
                }

                if (ewc.face() == FPInput::CardinalDirectionOrAny::any) {
                    fc.external_wall_error += calc_sq_error(
                            owl_total, ewc.minLength(), std::numeric_limits<double>::max()
                    );
                } else {
                    const auto tcd = cd_from_input(ewc.face());
                    fc.external_wall_error += calc_sq_error(
                            outer_wall_lengths.at(tcd), ewc.minLength(), std::numeric_limits<double>::max()
                    );
                }
            }

            for (unsigned int kp = ip + 1; kp < metrics_v.size(); ++kp) {
                auto& po = metrics_v[kp];
                const auto& ro = po.first;
                const auto rto = ro.first;
                auto& mo = *po.second;

                double total_touching_length = 0.0;
                for (const auto & ri : m.rects) {
                    for (const auto & rk : mo.rects) {
                        auto e = ri.get_touching_length(rk);
                        if (e) total_touching_length += e->length;
                    }
                }

                if (total_touching_length >= 0.79) {
                    m.adjacent_rooms.push_back(ro);
                    mo.adjacent_rooms.push_back(r);
                }

                const auto rtrar = this->area_ratio_reqs.find(rt);
                if (rtrar != this->area_ratio_reqs.end()) {
                    const auto rtorar = rtrar->second.find(rto);
                    if (rtorar != rtrar->second.end()) {
                        const auto rar = calc_sq_error(m.area / mo.area, rtorar->second.first, rtorar->second.second);
                        fc.relative_area_error += rar;
                    }
                }
            }

            fc.strange_shape_error += (m.rects.size() - 1) * (m.rects.size() - 1);
        }

        for (const auto& p : metrics_v) {
            const auto& r = p.first;
            const auto rt = r.first;
            const auto& m = p.second;

            const auto rtar = this->adjacency_reqs.find(rt);
            if (rtar != this->adjacency_reqs.end()) {
                std::unordered_set<const FPInput::RoomType*> adj_reqs = rtar->second;
                for (const auto& adj : m->adjacent_rooms) {
                    adj_reqs.erase(adj.first);
                }

                fc.adjacency_error += adj_reqs.size();
            }

            if (rt->publiclyAccessible()) {
                std::deque<RoomInst> pf_queue;
                std::unordered_set<RoomInst> pf_visited;
                pf_queue.push_back(r);
                bool living_reachable = false;

                while (!pf_queue.empty()) {
                    const auto pf_r = pf_queue.front();
                    pf_queue.pop_front();
                    pf_visited.insert(pf_r);
                    if (this->main_room_types.contains(pf_r.first)) {
                        living_reachable = true;
                        break;
                    }
                    if (pf_r.first == rt || pf_r.first->thoroughfare()) {
                        const auto& adj = metrics.at(pf_r).adjacent_rooms;
                        std::copy_if(adj.begin(), adj.end(), std::back_inserter(pf_queue), [&](const auto& ri) {
                            return !pf_visited.contains(ri);
                        });
                    }
                }

                if (!living_reachable) {
                    ++fc.reachability_error;
                }
            }
        }

        for (const auto& p : rt_occs) {
            const auto rt = p.first;
            const auto& occ = p.second;

            fc.absence_error += calc_sq_error<double>(occ.size(), rt->Count()->min(), std::stod(rt->Count()->max()));
        }

        return fc;
    }

    auto operator()(const FloorPlanGenome& g) -> std::array<double, D> {
        auto fc = this->calc_fitness_components(g);

        if (fc.adjacency_error > 0
                || fc.disjointedness > 0
                || fc.absence_error > QUINOA_EPSILON
                || fc.reachability_error > 0
                || fc.out_of_bounds_error > 0
        ) {
            std::array<double, D> f;
            f.fill(-std::numeric_limits<double>::infinity());
            return f;
        }

        return {
                -fc.dimension_error,
                -fc.aspect_ratio_error,
                -fc.relative_area_error,
                -fc.external_wall_error,
                -static_cast<double>(fc.strange_shape_error / 4),
                -(static_cast<double>(fc.adjacency_error) * static_cast<double>(fc.adjacency_error)),
                -static_cast<double>(fc.disjointedness),
                -fc.absence_error,
                -static_cast<double>(fc.reachability_error),
                -static_cast<double>(fc.out_of_bounds_error)
        };
    }
};

}
