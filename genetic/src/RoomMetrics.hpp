#pragma once

#include "input.hpp"

#include <limits>
#include <vector>
#include <unordered_map>
#include <utility>

namespace Quinoa {

using RoomInst = std::pair<const FPInput::RoomType *, unsigned int>;

}

namespace std {

template<>
struct hash<Quinoa::RoomInst> {
    auto operator()(const Quinoa::RoomInst &p) const -> std::size_t {
        return 14695981039346656037ULL + (reinterpret_cast<std::uintptr_t>(p.first) + p.second) * 1099511628211ULL;
    }
};

}

namespace Quinoa {

struct RoomMetrics {
    double area = 0;
    double min_x = std::numeric_limits<double>::max();
    double max_x = std::numeric_limits<double>::min();
    double min_y = std::numeric_limits<double>::max();
    double max_y = std::numeric_limits<double>::min();

    std::vector<Rect> rects;

    std::vector<RoomInst> adjacent_rooms;
};

using MetricsMap = std::unordered_map<RoomInst, RoomMetrics>;

static inline void collect_metrics(MetricsMap& metrics_map, const FloorPlanGenomeNode& node, const Rect& r) {
    if (typeid(node) == typeid(const FloorPlanGenomeSplit&)) {
        const auto& split = dynamic_cast<const FloorPlanGenomeSplit&>(node);
        const auto r_split = r.split(split.get_split(), split.get_split_dist());
        collect_metrics(metrics_map, split.get_left(), r_split.first);
        collect_metrics(metrics_map, split.get_right(), r_split.second);
    } else {
        const auto& room = dynamic_cast<const FloorPlanGenomeRoom&>(node);
        auto& metrics = metrics_map[std::make_pair(&room.get_room_type(), room.get_instance())];
        metrics.area += r.w * r.h;

        metrics.min_x = std::min(metrics.min_x, r.x);
        metrics.max_x = std::max(metrics.max_x, r.x + r.w);
        metrics.min_y = std::min(metrics.min_y, r.y);
        metrics.max_y = std::max(metrics.max_y, r.y + r.h);

        metrics.rects.push_back(r);
    }
}

}
