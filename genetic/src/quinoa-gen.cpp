#include "FloorPlanCrossover.hpp"
#include "FloorPlanFitness.hpp"
#include "FloorPlanGenome.hpp"
#include "FloorPlanGenomeGenerator.hpp"
#include "floor-plan-io.hpp"
#include "FloorPlanMutation.hpp"
#include "input-patching.hpp"
#include "Line.hpp"
#include "Point.hpp"

#include "ProgressBar.hpp"
#include "slopt/opt.h"
#include "input.hpp"
#include "output.hpp"
#include "NSGA2.hpp"

#include <filesystem>
#include <iostream>
#include <random>
#include <ctime>

using namespace Quinoa;
using namespace std::string_literals;

using Rng = std::mt19937_64;

auto seed = 6ULL;
auto tournament_size = 2UL;
auto pop_size = 500UL;
auto num_generations = 1000UL;

std::optional<std::string> input_path;
std::optional<std::string> output_path;
extern std::optional<std::filesystem::path> input_schema_path;
extern std::optional<std::filesystem::path> output_schema_path;
std::vector<std::filesystem::path> init_genome_paths;
long time_limit = std::numeric_limits<unsigned int>::max();

bool output_reqs = false;

static slopt_Option options[] = {
        { 's', "seed", SLOPT_REQUIRE_ARGUMENT },
        { 'p', "population-size", SLOPT_REQUIRE_ARGUMENT },
        { 'g', "generations", SLOPT_REQUIRE_ARGUMENT },
        { 't', "tournament-size", SLOPT_REQUIRE_ARGUMENT },
        { 'x', "input-schema-path", SLOPT_REQUIRE_ARGUMENT },
        { 'y', "output-schema-path", SLOPT_REQUIRE_ARGUMENT },
        { 'o', "output-reqs", SLOPT_DISALLOW_ARGUMENT },
        { 'i', "init", SLOPT_REQUIRE_ARGUMENT },
        { 'l', "time-limit", SLOPT_REQUIRE_ARGUMENT },
};

static void on_argument(int sw, char snam, const char *lnam, const char *val, [[maybe_unused]] void *pl) {
    switch (sw) {
        case SLOPT_OK1:
        case SLOPT_OK2:
            switch (snam) {
                case 's':
                    seed = std::stoull(val);
                    break;
                case 'p':
                    pop_size = std::stoul(val);
                    break;
                case 'g':
                    num_generations = std::stoul(val);
                    break;
                case 't':
                    tournament_size = std::stoul(val);
                    break;
                case 'x':
                    input_schema_path = val;
                    break;
                case 'y':
                    output_schema_path = val;
                    break;
                case 'o':
                    output_reqs = true;
                    break;
                case 'i':
                    init_genome_paths.emplace_back(val);
                    break;
                case 'l':
                    time_limit = std::stol(val);
                    break;
                default:
                    throw std::invalid_argument(
                            "Unrecognized option "s + static_cast<char>(sw) + snam
                            + " or " + static_cast<char>(sw) +  static_cast<char>(sw) + lnam
                    );
            }
            break;
        case SLOPT_DIRECT:
            if (!input_path.has_value()) {
                input_path = val;
                break;
            }
            if (!output_path.has_value()) {
                output_path = val;
                break;
            }
            throw std::invalid_argument("Unexpected argument "s + val);
        case SLOPT_MISSING_ARGUMENT:
            throw std::invalid_argument("Missing argument for "s + snam + "/" + lnam);
        case SLOPT_UNEXPECTED_ARGUMENT:
            throw std::invalid_argument("Unexpected argument for "s + snam + "/" + lnam);
        case SLOPT_UNKNOWN_LONG_OPTION:
            throw std::invalid_argument("Unknown option "s + lnam);
        case SLOPT_UNKNOWN_SHORT_OPTION:
            throw std::invalid_argument("Unknown option "s + snam);
        default:
            throw std::invalid_argument("Unknown option parser state " + std::to_string(sw));
    }
}

static bool is_valid(const NSGA2::Individual<D, FloorPlanGenome>& indiv) {
    return std::ranges::all_of(indiv.fitness, [](const auto& fc) {
        return fc != -std::numeric_limits<double>::infinity();
    });
}

static bool contains_valid(const NSGA2::Population<D, FloorPlanGenome>& pop) {
    return std::ranges::any_of(pop, is_valid);
}

int run(int argc, char **argv) {
    slopt_parse(argc - 1, argv + 1, options, on_argument, nullptr);

    if (!input_path) {
        fprintf(stderr, "Missing requirements file\n");
        return 1;
    }

    xml_schema::Properties props;
    props.schema_location("urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed",
            std::filesystem::absolute(input_schema_path.value_or("input.xsd"))
    );
    const auto reqs = std::shared_ptr(FPInput::FloorPlanRequirements_(input_path.value(), 0, props));
    std::for_each(reqs->RoomTypes().RoomType().begin(), reqs->RoomTypes().RoomType().end(), patch_room_type);

    if (!reqs->RoomAreaRatios()) {
        reqs->RoomAreaRatios(FPInput::RoomAreaRatios());
    }

    if (!reqs->RoomAdjacencies()) {
        reqs->RoomAdjacencies(FPInput::RoomAdjacencies());
    }

    if (!reqs->Objectives()) {
        reqs->Objectives(FPInput::Objectives());
    }

    auto& rars = reqs->RoomAreaRatios()->RoomAreaRatio();
    std::for_each(rars.begin(), rars.end(), patch_room_area_ratio_range);

    {
        FPInput::RoomType void_type_def("<void>");
        void_type_def.Count(FPInput::CountRange());
        void_type_def.Count()->min(0);
        void_type_def.Count()->max("0");
        patch_room_type(void_type_def);

        FPInput::RoomType out_type_def("<out>");
        out_type_def.Count(FPInput::CountRange());
        out_type_def.Count()->min(0);
        patch_room_type(out_type_def);

        reqs->RoomTypes().RoomType().push_back(void_type_def);
        reqs->RoomTypes().RoomType().push_back(out_type_def);
    }

    const FPInput::RoomType& void_type = reqs->RoomTypes().RoomType().at(reqs->RoomTypes().RoomType().size() - 2);
    const FPInput::RoomType& out_type = reqs->RoomTypes().RoomType().back();

    FloorPlanGeometry fp_geo{reqs, out_type};

    for (const auto& init_path : init_genome_paths) {
        auto genome = load_floor_plan(*reqs, &out_type, fp_geo.get_full_plan_rect(), init_path);
        auto out_path = init_path;
        out_path.replace_filename(init_path.filename().string() + "-rer");
        assert(init_path != out_path);
        save_floor_plan(genome, fp_geo, {}, &out_type, out_path);
        return 0;
    }

    auto rng = std::make_shared<Rng>(seed);
    using Genome = FloorPlanGenome;
    using GenomeGen = FloorPlanGenomeGenerator<Rng>;
    auto genome_gen = GenomeGen(rng, reqs, fp_geo, void_type, out_type);

    using FF = FloorPlanFitness;
    auto ff = FF(reqs, fp_geo, void_type, out_type);

    using SM = NSGA2::TournamentSelection<D, Rng, Genome>;
    auto sm = SM(rng, tournament_size);

    using CM = FloorPlanCrossover<D, Rng>;
    auto cm = CM(rng);

    using MM = FloorPlanMutation<D, Rng>;
    auto mm = MM(rng, reqs, &void_type, &out_type);

    auto nsga2 = NSGA2::NSGA2<D, Genome, FF, SM, CM, MM, Rng>(ff, sm, cm, mm, rng);
    auto pop = nsga2.init(pop_size, genome_gen);

    for (const auto& indiv : pop) {
        printf("INIT %s\n", std::string(indiv).c_str());
    }

    ProgressBar::ProgressBar pb(static_cast<double>(num_generations));
    pb.start("Evolving...");

    std::optional<long> first_valid_time;
    if (contains_valid(pop)) {
        first_valid_time = 0;
    }

    auto time_start = time(nullptr);
    auto time_end = time_start + time_limit;
    auto timeout = false;

    for (unsigned int i = 0; i < num_generations && !timeout; ++i) {
        pop = nsga2.evolve(pop);
        if (!first_valid_time && contains_valid(pop)) {
            first_valid_time = time(nullptr) - time_start;
        }
        timeout = time(nullptr) > time_end;
        pb.progress(static_cast<double>(i), "Evolving...");
    }

    pb.stop(timeout ? "Timeout" : "Done");

    if (!contains_valid(pop)) {
        printf("No valid solutions\n");
        return 1;
    }

    printf("Found first valid solution after %ld seconds\n", first_valid_time.value_or(-1L));

    const auto fronts = pop.sort();
    const auto all_best = fronts[0];
    std::unordered_set<NSGA2::Individual<D, Genome>, NSGA2::hash_individual<D, Genome>> best(
            all_best.begin(), all_best.end()
    );
    std::atomic_int si = 0;
    std::for_each(best.begin(), best.end(), [&fp_geo, &si, &reqs, &out_type](const auto& indiv) {
        const auto i = si++;

        save_floor_plan(
                indiv.g,
                fp_geo,
                output_reqs ? reqs : std::optional<decltype(reqs)>{},
                &out_type,
                output_path.value_or(".") + "/plan-" + std::to_string(i) + ".xml"
        );

        printf("#%u = %s\n", i, std::string(indiv).c_str());
    });

    return 0;
}

int main(int argc, char **argv) {
    try {
        run(argc, argv);
    } catch (const xml_schema::Exception& ex) {
        std::cerr << "Caught XML exception:" << std::endl << ex << std::endl;
    }
}
