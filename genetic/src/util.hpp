#pragma once

#include "HyperparamSupport.hpp"

#include <stdexcept>
#include <cassert>

#ifdef GNUC
    #define QUINOA_UNREACHABLE __builtin_unreachable();
#else
    #define QUINOA_UNREACHABLE throw std::runtime_error("unreachable");
#endif


namespace Quinoa {

static inline double round_to(double v, double m) {
    assert(3 * QUINOA_EPSILON < m && "Attempted to round to precision greater than internal epsilon + margin");
    return round(v / m) * m;
}

static inline double round_to_mm(double v) {
    return round_to(v, 0.001);
}

static inline double round_to_cm(double v) {
    return round_to(v, 0.01);
}

static inline bool fleq(double e, double s) {
    return std::abs(e - s) < 3 * QUINOA_EPSILON;
}

}
