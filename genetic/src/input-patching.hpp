#pragma once

#include "input.hpp"

namespace Quinoa {

void patch_room_type(FPInput::RoomType& type);
void patch_room_area_ratio_range(FPInput::RoomAreaRatioRange& r);
void patch_objective_weight(FPInput::Objective& o);

}
