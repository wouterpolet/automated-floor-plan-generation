#pragma once

#include "FloorPlanGenome.hpp"

#include "input.hpp"
#include "output.hpp"

#include <filesystem>
#include <utility>

namespace Quinoa {

class FloorPlanGeometry;

void save_floor_plan(
        const FloorPlanGenome& plan,
        const FloorPlanGeometry& fp_geo,
        std::optional<std::shared_ptr<FPInput::FloorPlanRequirements>> reqs,
        const FPInput::RoomType* out_type,
        const std::filesystem::path& path
);

FloorPlanGenome load_floor_plan(
        const FPInput::FloorPlanRequirements& reqs,
        const FPInput::RoomType* out_type,
        const Rect& bounds,
        const std::filesystem::path& path
);

FloorPlanGenome represent_floor_plan(
        const FPInput::FloorPlanRequirements& reqs,
        const FPInput::RoomType* out_type,
        const Rect& bounds,
        const FPOutput::FloorPlan& fp
);

}
