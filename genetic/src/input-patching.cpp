#include "input-patching.hpp"

template <typename O, typename T, typename V>
static void patch_positive_unbounded_range(O& o, V min = static_cast<V>(0), V max = std::numeric_limits<V>::max()) {
    const auto max_v_s = std::to_string(max);
    if (!o) {
        T t;
        t.min(min);
        t.max(max_v_s);
        o = t;
    } else if (o->max() == "unbounded") {
        o->max(max_v_s);
    }
}

void Quinoa::patch_room_type(FPInput::RoomType& type) {
    patch_positive_unbounded_range<FPInput::RoomType::CountOptional, FPInput::CountRange, unsigned int>(type.Count(), 1, 1);
    patch_positive_unbounded_range<FPInput::RoomType::WidthOptional, FPInput::Range, double>(type.Width());
    patch_positive_unbounded_range<FPInput::RoomType::WidthOptional, FPInput::Range, double>(type.Length());
    patch_positive_unbounded_range<FPInput::RoomType::WidthOptional, FPInput::Range, double>(type.Area());
    patch_positive_unbounded_range<FPInput::RoomType::WidthOptional, FPInput::Range, double>(type.AspectRatio());
}

void Quinoa::patch_room_area_ratio_range(FPInput::RoomAreaRatioRange &r) {
    if (r.max() == "unbounded") r.max(std::to_string(std::numeric_limits<double>::max()));
}
