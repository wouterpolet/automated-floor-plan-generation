#pragma once

#include "FloorPlanGenome.hpp"
#include "FloorPlanGeometry.hpp"
#include "HyperparamSupport.hpp"

#include "input.hpp"
#include "NSGA2.hpp"

#include <unordered_set>
#include <random>

namespace Quinoa {

template<std::uniform_random_bit_generator R>
class FloorPlanGenomeGenerator {
private:
    std::shared_ptr<R> rng;

    std::shared_ptr<const FPInput::FloorPlanRequirements> reqs;
    const FloorPlanGeometry& fp_geo;
    const FPInput::RoomType &void_type;
    const FPInput::RoomType &out_type;

    FloorPlanGenome base_plan;

public:
    FloorPlanGenomeGenerator(
            std::shared_ptr<R> rng,
            std::shared_ptr<const FPInput::FloorPlanRequirements> reqs,
            const FloorPlanGeometry& fp_geo,
            const FPInput::RoomType &void_type,
            const FPInput::RoomType &out_type
    ) :
            rng(std::move(rng)),
            reqs(std::move(reqs)),
            fp_geo(fp_geo),
            void_type(void_type),
            out_type(out_type)
    { }

    auto operator()() const {
        std::vector<RoomInst> required_types;
        for (const auto& type : this->reqs->RoomTypes().RoomType()) {
            for (unsigned int i = 0; i < type.Count()->min(); ++i) {
                required_types.push_back(std::make_pair(&type, i + 1));
            }
        }
        std::ranges::shuffle(required_types, *this->rng);

        FloorPlanGenome genome = this->fp_geo.get_init_genome();

        for (const auto& p : required_types) {
            const FPInput::RoomType& type = *p.first;
            const auto inst = p.second;

            auto void_replaced = false;
            for (unsigned int i = 0; i < 2; ++i) {
                // Try hard...
                auto n = genome.get_random_leaf(*this->rng);
                if (n->get_room_type().name() == "<void>") {
                    n->set_room_type(&type);
                    n->set_instance(inst);
                    void_replaced = true;
                    break;
                }
            }
            if (void_replaced) continue;
            auto n = genome.get_random_leaf(*this->rng);

            const auto split_dist = gen_split_dist(*this->rng);
            const auto split = gen_bool(*this->rng) ? Split::HORIZONTAL : Split::VERTICAL;
            const auto move_to_left = gen_bool(*this->rng);

            std::unique_ptr<FloorPlanGenomeNode> rnode = std::make_unique<FloorPlanGenomeRoom>(type, inst);
            std::unique_ptr<FloorPlanGenomeNode> nnode = std::make_unique<FloorPlanGenomeSplit>(
                    split, split_dist, move_to_left ? std::move(rnode) : n->clone(), move_to_left ? n->clone() : std::move(rnode)
            );

            n->replace(std::move(nnode));
        }

        return genome;
    }

private:
};

static_assert(
        NSGA2::GenomeGenerator<FloorPlanGenomeGenerator<std::mt19937_64>>,
        "The floor plan genome generator is not a valid genome generator"
);

}
