#pragma once

#include "Point.hpp"
#include "Edge.hpp"
#include "Split.hpp"

#include <optional>
#include <sstream>
#include <ostream>

namespace Quinoa {

/**
 * An axis-aligned rectangle.
 */
struct Rect {
    double x = 0; ///< The x-coordinate of the lower-left point.
    double y = 0; ///< The y-coordinate of the lower-left point.
    double w = 0; ///< The width of the rectangle.
    double h = 0; ///< The height of the rectangle.

    /**
     * Converts the rectangle to a string.
     *
     * The format is the same as for operator<<.
     *
     * @return the rectangle as a string
     */
    explicit operator std::string() const {
        std::stringstream ss;
        ss << *this;
        return ss.str();
    }

    /**
     * Prints the rectangle.
     *
     * The format is "{[x, x + w), [y, y + h)}", i.e. two ranges, one for each axis, representing the cover of the
     * rectangle.
     *
     * @param os the stream to print to
     * @param r the rectangle to print
     * @return the stream
     */
    friend auto operator<<(std::ostream& os, const Rect& r) -> std::ostream& {
        os << "{[" << r.x << "; " << r.x + r.w << "), [" << r.y << "; " << r.y + r.h << ")}";
        return os;
    }

    /**
     * Splits the rectangle into two.
     *
     * If the split is horizontal, the first rectangle will be the left rectangle and the second the right. If the
     * split is vertical, the first rectangle will be the lower rectangle and the second the upper.
     *
     * @param split the direction to split
     * @param split_dist the distance along the split axis where the rectangle should be split
     *
     * @return a pair of rectangles
     */
    [[nodiscard]] auto split(Split split, double split_dist) const -> std::pair<Rect, Rect> {
        switch (split) {
            case Split::HORIZONTAL:
                return {
                        { this->x, this->y, this->w * split_dist, this->h },
                        { this->x + this->w * split_dist, this->y, this->w * (1 - split_dist), this->h }
                };
            case Split::VERTICAL:
                return {
                        { this->x, this->y, this->w, this->h * split_dist },
                        { this->x, this->y + this->h * split_dist, this->w, this->h * (1 - split_dist) }
                };
            default:
                throw std::invalid_argument("Invalid split direction");
        }
    }

    /**
     * Checks if this rectangle overlaps the other.
     *
     * @param other the other rectangle
     *
     * @return true if the rectangles overlap, false otherwise
     */
    [[nodiscard("pure")]] auto overlaps(const Rect& other) const {
        auto [pt1, pt2] = this->to_corner_points();
        auto [po1, po2] = other.to_corner_points();

        return pt1.x <= po2.x && pt2.x >= po1.x && pt1.y <= po2.y && pt2.y >= po1.y;
    }

    /**
     * Checks if this rectangle touches the other.
     *
     * This check is fairly liberal in order to properly deal with floating point precision problems. The rectangles are
     * "grown" at all four points by the epsilon value and then they are checked for overlap. The original rectangles
     * are deemed to be touching if the grown rectangles overlap.
     *
     * This may return some false positives, for example rectangles where the corners touch, but the edges do not.
     * get_touching_length() is an alternative method that performs more checks and returns the length and face where
     * the rectangles touch, but is more computationally intensive.
     *
     * @param other the other rectangle
     * @param epsilon the precision to check with
     *
     * @return true if the rectangles touch, false otherwise
     */
    [[nodiscard("pure")]] auto touches(const Rect& other, double epsilon = QUINOA_EPSILON) const {
        return this->grown_slightly(epsilon).overlaps(other.grown_slightly(epsilon));
    }

    /**
     * Returns where and how much this rectangle touches another rectangle.
     *
     * This method is similar to the method touches(), but is more sensitive and returns more information while
     * requiring more computations.
     *
     * The return value is the length of the shared edge and where this edge is from the perspective of this rectangle.
     *
     * All checks are performed using the epsilon value to grow the rectangle like touches() does, and additionally
     * checks that the length of the shared edge is significant (approximately at least epsilon, though beware floating
     * point errors). This causes this function to reject some cases where touches() would return true, such as when
     * only corners touch.
     *
     * The returned length may have an error of about twice epsilon.
     *
     * @param other the other rectangle
     * @param epsilon the precision to check with
     *
     * @return an edge containing the length and face where the rectangles touch, a falsy empty optional otherwise
     */
    [[nodiscard]] auto get_touching_length(const Rect& other, double epsilon = QUINOA_EPSILON) const {
        using Ret = std::optional<Edge>;

        auto [pt1, pt2] = this->grown_slightly(epsilon).to_corner_points();
        auto [po1, po2] = other.grown_slightly(epsilon).to_corner_points();

        const auto xl = po2.x - pt1.x;
        const auto xr = pt2.x - po1.x;
        const auto yu = po2.y - pt1.y;
        const auto yl = pt2.y - po1.y;

        if (!(xl >= 0 && xr >= 0 && yu >= 0 && yl >= 0)) {
            return Ret{};
        }

        if (xl > 3 * epsilon && xr > 3 * epsilon) {
            const auto dir = (pt1.y >= po1.y) ? CardinalDirection::SOUTH : CardinalDirection::NORTH;
            const auto l = ((pt1.x <= po1.x) ? xr : xl) - 2 * epsilon;
            return Ret({l, dir});
        } else if (yu > 3 * epsilon && yl > 3 * epsilon) {
            const auto dir = (pt1.x >= po1.x) ? CardinalDirection::WEST : CardinalDirection::EAST;
            const auto l = ((pt1.y <= po1.y) ? yl : yu) - 2 * epsilon;
            return Ret({l, dir});
        }

        return Ret{};
    }

    /**
     * Checks if this rectangle is fully inside a polygon.
     *
     * The rectangle is shrunk slightly to correct for numerical errors.
     *
     * @param poly the polygon to check against
     * @param epsilon the amount to shrink this rectangle with
     *
     * @return true if this rectangle is fully inside the polygon, false otherwise
     */
    [[nodiscard("pure")]] auto is_in_poly(const Polygon_2& poly, double epsilon = QUINOA_EPSILON) const {
        auto [p1, p2, p3, p4] = this->shrunk_slightly(epsilon).to_points();

        return p1.is_in_poly(poly) && p2.is_in_poly(poly) && p3.is_in_poly(poly) && p4.is_in_poly(poly);
    }

    [[nodiscard("pure")]] auto is_in_rect(const Rect& other) const {
        const auto [pt1, pt2] = this->to_corner_points();
        const auto [po1, po2] = other.grown_slightly().to_corner_points();

        return pt1.x >= po1.x && pt1.x <= po2.x
            && pt2.x >= po1.x && pt2.x <= po2.x
            && pt1.y >= po1.y && pt1.y <= po2.y
            && pt2.y >= po1.y && pt2.y <= po2.y;
    }

    [[nodiscard("pure")]] auto encloses_rect(const Rect& other) const {
        return other.is_in_rect(*this);
    }

    /**
     * Checks if this rectangle fully encloses the given polygon.
     *
     * @param poly the polygon
     * @param epsilon the tolerance
     *
     * @return true if the polygon is fully inside this rectangle, false otherwise
     */
    [[nodiscard("pure")]] auto encloses_poly(const Polygon_2& poly, double epsilon = QUINOA_EPSILON) const {
        const auto og = this->grown_slightly(epsilon);
        const auto o_bbox = poly.bbox();
        return o_bbox.xmin() >= og.x && o_bbox.xmax() <= og.x + og.w
            && o_bbox.ymin() >= og.y && o_bbox.ymax() <= og.y + og.h;
    }

    /**
     * Returns this rectangle shrunk by an insignificant amount.
     *
     * This is useful for cases where a small margin is desired to compensate for FP numerical instabilities.
     *
     * The epsilon parameter controls by how much the rectangle shrinks. The four points are moved inwards by epsilon on
     * each axis. The total area reduction is 2ε(w + h) - 4ε^2.
     *
     * @param epsilon the amount to shrink by
     *
     * @return the shrunken rectangle
     */
    [[nodiscard("pure")]] Rect shrunk_slightly(double epsilon = QUINOA_EPSILON) const {
        return Rect{this->x + epsilon, this->y + epsilon, this->w - 2 * epsilon, this->h - 2 * epsilon};
    }

    /**
     * Returns this rectangle grown by an insignificant amount.
     *
     * This is useful for cases where a small margin is desired to compensate for FP numerical instabilities.
     *
     * The epsilon parameter controls by how much the rectangle grows. The four points are moved outwards by epsilon on
     * each axis. The total area increase is 2ε(w + h) + 4ε^2.
     *
     * @param epsilon
     * @return
     */
    [[nodiscard("pure")]] Rect grown_slightly(double epsilon = QUINOA_EPSILON) const {
        return this->shrunk_slightly(-epsilon);
    }

    [[nodiscard("pure")]] std::tuple<Point, Point, Point, Point> to_points() const {
        return {
                {this->x, this->y},
                {this->x + this->w, this->y},
                {this->x + this->w, this->y + this->h },
                {this->x, this->y + this->h},
        };
    }

    [[nodiscard("pure")]] std::pair<Point, Point> to_corner_points() const {
        return {
            {this->x, this->y},
            {this->x + this->w, this->y + this->h}
        };
    }

    explicit operator Polygon_2() const {
        Polygon_2 poly;
        poly.push_back({this->x, this->y});
        poly.push_back({this->x + this->w, this->y});
        poly.push_back({this->x + this->w, this->y + this->h});
        poly.push_back({this->x, this->y + this->h});
        return poly;
    }

    explicit operator CGAL::Bbox_2() const {
        return {this->x, this->y, this->x + this->w, this->y + this->h};
    }
};

}
