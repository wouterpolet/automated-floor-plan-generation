#pragma once

#include "Point.hpp"

#include <ostream>
#include <utility>

namespace Quinoa {

struct Line {
    Point start;
    Point end;

    Line() = default;
    Line(const Point& start, const Point& end) : start(start), end(end) {}
    Line(const Line& other) = default;
    explicit Line(const Segment_2& other): start(other.start()), end(other.end()) {}

    friend auto operator<<(std::ostream& os, const Line& l) -> std::ostream& {
        os << '[' << l.start << ", " << l.end << ']';
        return os;
    }
};

}

namespace std {

template <>
struct hash<Quinoa::Line> {
    auto operator()(const Quinoa::Line& l) const -> std::size_t {
        const std::hash<Quinoa::Point> hp;
        return 1099511628211ULL * (hp(l.start) ^ hp(l.end));
    }
};

}
