#pragma once

#include "FloorPlanGenome.hpp"

#include "NSGA2.hpp"

#include <random>

namespace Quinoa {

template <std::size_t D, std::uniform_random_bit_generator R>
class FloorPlanCrossover {
private:
    std::shared_ptr<R> rng;

    using Indiv = NSGA2::Individual<D, FloorPlanGenome>;
    using Pop = NSGA2::Population<D, FloorPlanGenome>;
public:
    explicit FloorPlanCrossover(std::shared_ptr<R> rng) : rng(std::move(rng)) {}

    auto operator()(Pop& pop) {
        for (unsigned int i = 0; i + 1 < pop.size(); i += 2) {
            auto& indiv_a = pop[i];
            auto& indiv_b = pop[i + 1];

            auto n_a = this->get_random_node(indiv_a);
            auto n_b = this->get_random_node(indiv_b);

            n_a->swap(*n_b);
        }
    }
private:
    auto get_random_node(Indiv& indiv) -> FloorPlanGenomeNode* {
        auto& r = indiv.g.get_root();
        const auto node_count = r.get_node_count();
        if (node_count == 0) return nullptr;
        std::uniform_int_distribution<unsigned int> l_idx_dist(0, node_count - 1);
        const auto l_idx = l_idx_dist(*this->rng);
        auto it = r.begin();
        for (unsigned int i = 0; i < l_idx; ++i) ++it;
        return *it;
    }
};

}
