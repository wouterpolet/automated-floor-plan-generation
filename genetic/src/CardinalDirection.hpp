#pragma once

#include "util.hpp"

#include "input.hpp"

#include <ostream>

namespace Quinoa {

enum class CardinalDirection {
    NORTH,
    EAST,
    SOUTH,
    WEST
};

inline std::ostream& operator<<(std::ostream& os,  CardinalDirection cd) {
    switch (cd) {
        case CardinalDirection::NORTH:
            os << "NORTH";
            break;
        case CardinalDirection::EAST:
            os << "EAST";
            break;
        case CardinalDirection::SOUTH:
            os << "SOUTH";
            break;
        case CardinalDirection::WEST:
            os << "WEST";
            break;
        default:
            QUINOA_UNREACHABLE
    }
    return os;
}

static inline auto opposite_cd(CardinalDirection cd) {
    switch (cd) {
        case CardinalDirection::NORTH:
            return CardinalDirection::SOUTH;
        case CardinalDirection::EAST:
            return CardinalDirection::WEST;
        case CardinalDirection::SOUTH:
            return CardinalDirection::NORTH;
        case CardinalDirection::WEST:
            return CardinalDirection::EAST;
    }

    QUINOA_UNREACHABLE
}

static inline auto cd_from_input(const FPInput::CardinalDirectionOrAny& cd) {
    switch (cd) {
        case FPInput::CardinalDirectionOrAny::north:
            return CardinalDirection::NORTH;
        case FPInput::CardinalDirectionOrAny::east:
            return CardinalDirection::EAST;
        case FPInput::CardinalDirectionOrAny::south:
            return CardinalDirection::SOUTH;
        case FPInput::CardinalDirectionOrAny::west:
            return CardinalDirection::WEST;
        case FPInput::CardinalDirectionOrAny::any:
            throw std::invalid_argument("Cannot translate 'any' to cardinal direction");
    }

    QUINOA_UNREACHABLE
}

}
