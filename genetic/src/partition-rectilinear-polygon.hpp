#pragma once

#include "CGAL.hpp"
#include "util.hpp"

#include <vector>

namespace Quinoa {

namespace {

struct VertSegmentSortedByX {
    [[nodiscard("pure")]] bool operator()(const auto& e1, const auto& e2) const {
        const auto x1 = e1.start().x();
        const auto x2 = e2.start().x();
        const auto y1 = e1.start().y();
        const auto y2 = e2.start().y();
//        return x1 - x2 < -QUINOA_EPSILON || (fleq(x1, x2) && y1 - y2 < -QUINOA_EPSILON);
        return x1 < x2 || (x1 == x2 && y1 < y2);
    }
};

}

template <typename IO>
void partition_rectilinear_polygon(const Polygon_2& poly_c, IO out) {
    Polygon_2 poly{poly_c};

    if (poly.orientation() != CGAL::Orientation::POSITIVE) poly.reverse_orientation();
    if (poly.orientation() != CGAL::Orientation::POSITIVE) {
        throw std::invalid_argument(
                "Polygon has strange orientation #" + std::to_string(poly.orientation())
        );
    }

    std::set<Segment_2, VertSegmentSortedByX> active_segments;
    std::set<Segment_2, VertSegmentSortedByX> vert_edges;
    for (auto it = poly.edges_begin(); it != poly.edges_end(); ++it) {
        if (it->is_vertical()) vert_edges.insert(*it);
    }

    for (const auto& e : vert_edges) {
        if (e.start().y() > e.end().y()) {
            // Going down, activating
            active_segments.insert(e);
            continue;
        }

        const auto e_top = e.end().y();
        const auto e_bottom = e.start().y();
        const auto e_x = e.start().x();

        // Going up, deactivating. Find overlapping active segments
        auto next_active_segments = active_segments;
        for (const auto& ae : active_segments) {
            const auto ae_top = ae.start().y();
            const auto ae_bottom = ae.end().y();
            const auto ae_x = ae.start().x();

            if (e_top <= ae_top && e_top >= ae_bottom) {
                if (e_bottom >= ae_bottom && e_bottom <= ae_top) {
                    // Completely overlapped by activating edge. This will be the only activation
                    Polygon_2 poly;
                    poly.push_back({ae_x, e_bottom});
                    poly.push_back(e.start());
                    poly.push_back(e.end());
                    poly.push_back({ae_x, e_top});
                    assert(poly.is_simple());

                    *out++ = poly;

                    next_active_segments.erase(ae);

                    Segment_2 residual_top = {ae.start(), {ae_x, e_top}};
                    if (!residual_top.is_degenerate()) next_active_segments.insert(residual_top);

                    Segment_2 residual_bottom = {{ae_x, e_bottom}, ae.end()};
                    if (!residual_bottom.is_degenerate()) next_active_segments.insert(residual_bottom);
                    break;
                }

                // Partial overlap, only top
                Polygon_2 poly;
                poly.push_back(ae.end());
                poly.push_back({e_x, ae_bottom});
                poly.push_back(e.end());
                poly.push_back({ae_x, e_top});
                assert(poly.is_simple());

                *out++ = poly;

                next_active_segments.erase(ae);

                Segment_2 residual_top = {ae.start(), {ae_x, e_top}};
                if (!residual_top.is_degenerate()) next_active_segments.insert(residual_top);
            } else if (e_bottom >= ae_bottom && e_bottom <= ae_top) {
                // Partial overlap, only bottom
                Polygon_2 poly;
                poly.push_back({ae_x, e_bottom});
                poly.push_back(e.start());
                poly.push_back({e_x, ae_top});
                poly.push_back(ae.start());
                assert(poly.is_simple());

                *out++ = poly;

                next_active_segments.erase(ae);

                Segment_2 residual_bottom = {{ae_x, e_bottom}, ae.end()};
                if (!residual_bottom.is_degenerate()) next_active_segments.insert(residual_bottom);
            }
        }

        active_segments = next_active_segments;
    }
}

}
