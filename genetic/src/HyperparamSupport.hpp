#pragma once

#include <random>
#include <limits>

namespace Quinoa {

constexpr auto MUTATION_CHANCE = 1.0;
constexpr auto QUINOA_EPSILON = 0.000'000'1;

static std::normal_distribution<double> split_dist_dist(0.5, 0.1);
static std::uniform_int_distribution<int> bool_dist(0, 1);
static std::uniform_real_distribution<double> split_dist_mut_dist(-0.25, 0.25);

static inline auto gen_split_dist(auto &rng) {
    return std::clamp(split_dist_dist(rng), 0., 1.);
}

static inline auto gen_split_dist_mut(auto &rng, double old_split_dist) {
    return std::clamp(split_dist_mut_dist(rng) + old_split_dist, 0., 1.);
}

static inline bool gen_bool(auto &rng) {
    return bool_dist(rng);
}

}
