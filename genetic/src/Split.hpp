#pragma once

namespace Quinoa {

/**
 * A split direction.
 */
enum class Split {
    /**
     * A horizontal split, where a rectangle is split into a left and right rectangle.
     */
    HORIZONTAL,

    /**
     * A vertical split, where a rectangle is split into a lower and upper rectangle.
     */
    VERTICAL
};

}
