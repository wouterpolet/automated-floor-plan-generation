#pragma once

#include "FloorPlanGenome.hpp"
#include "RoomMetrics.hpp"
#include "floor-plan-io.hpp"

#include "input.hpp"
#include "output.hpp"
#include "CGAL.hpp"

namespace Quinoa {

class FloorPlanGeometry {
private:
    std::shared_ptr<const FPInput::FloorPlanRequirements> reqs;

    std::vector<std::pair<double, double>> point_list;
    Rect full_plan_rect;

    std::unordered_multimap<CardinalDirection, Rect> edge_rects;
    std::vector<Rect> edge_rects_any_side;
    std::vector<Rect> out_rects;

    FloorPlanGenome init_genome;

public:
    FloorPlanGeometry(
            std::shared_ptr<FPInput::FloorPlanRequirements> reqs,
            const FPInput::RoomType& out_type
    ) : reqs(std::move(reqs)) {
        Polygon_2 poly;

        double cx = 0, cy = 0;
        const auto& floor = this->reqs->BuildingGeometry().Floor()[0];
        for (const auto& point : floor.Point()) {
            cx = point.x();
            if (cx < this->full_plan_rect.x) this->full_plan_rect.x = cx;
            if (cx > this->full_plan_rect.w) this->full_plan_rect.w = cx;

            cy = point.y();
            if (cy < this->full_plan_rect.y) this->full_plan_rect.y = cy;
            if (cy > this->full_plan_rect.h) this->full_plan_rect.h = cy;

            poly.push_back({cx, cy});
            point_list.emplace_back(cx, cy);
        }

        if (poly.orientation() != CGAL::Orientation::POSITIVE) poly.reverse_orientation();
        if (poly.orientation() != CGAL::Orientation::POSITIVE) {
            throw std::invalid_argument(
                    "Building geometry has strange orientation #" + std::to_string(poly.orientation())
            );
        }

        for (auto it = poly.edges_begin(); it != poly.edges_end(); ++it) {
            const auto e = *it;
            const auto dir = e.direction();
            const auto dx = dir.dx();
            const auto dy = dir.dy();
            if ((std::abs(dx) < QUINOA_EPSILON) == (std::abs(dy) < QUINOA_EPSILON)) {
                throw std::invalid_argument(
                        "Bad building geometry: contains edge with dx = " + std::to_string(dx) + "; dy = "
                        + std::to_string(dy)
                        + "; std::abs(dx) < QUINOA_EPSILON = " + std::to_string(std::abs(dx) < QUINOA_EPSILON)
                        + "; std::abs(dy) < QUINOA_EPSILON = " + std::to_string(std::abs(dy) < QUINOA_EPSILON)
                );
            }

            if (dx >= QUINOA_EPSILON) {
                // Outside south
                Rect out = { e.start().x(), e.start().y() - QUINOA_EPSILON, dx, QUINOA_EPSILON };
                edge_rects.insert({CardinalDirection::SOUTH, out});
            } else if (dx <= -QUINOA_EPSILON) {
                // Outside north
                Rect out = { e.end().x(), e.end().y(), -dx, QUINOA_EPSILON };
                edge_rects.insert({CardinalDirection::NORTH, out});
            } else if (dy >= QUINOA_EPSILON) {
                // Outside east
                Rect out = { e.start().x(), e.start().y(), QUINOA_EPSILON, dy };
                edge_rects.insert({CardinalDirection::EAST, out});
            } else if (dy <= -QUINOA_EPSILON) {
                // Outside west
                Rect out = { e.end().x() - QUINOA_EPSILON, e.end().y(), QUINOA_EPSILON, -dy };
                edge_rects.insert({CardinalDirection::WEST, out});
            } else {
                std::stringstream ss;
                [[unlikely]]
                ss << "Unprocessable building geometry involving edge " << e << " and direction " << dir;
                throw std::invalid_argument(ss.str());
            }
        }

        this->full_plan_rect.w -= this->full_plan_rect.x;
        this->full_plan_rect.h -= this->full_plan_rect.y;

        std::ranges::transform(edge_rects, std::back_inserter(edge_rects_any_side), [](const auto& p) {
            return p.second;
        });

        FPOutput::Room root_room("<void>");
        root_room.instance(1);
        root_room.Geometry().push_back(floor);

        FPOutput::Floor ofloor;
        ofloor.Room().push_back(root_room);

        FPOutput::FloorPlan fp;
        fp.Floor().push_back(ofloor);

        this->init_genome = represent_floor_plan(*this->reqs, &out_type, this->full_plan_rect, fp);
        save_floor_plan(this->init_genome, *this, {}, &out_type, "contour.xml");

        MetricsMap metrics;
        collect_metrics(metrics, this->init_genome.get_root(), this->full_plan_rect);

        for (const auto& p : metrics) {
            const auto& ri = p.first;
            if (ri.first == &out_type) {
                for (const auto& r : p.second.rects) {
                    this->out_rects.push_back(r);
                }
            }
        }
    }

    auto get_point_list() const -> const std::vector<std::pair<double, double>>& {
        return this->point_list;
    }

    auto get_full_plan_rect() const -> const Rect& {
        return this->full_plan_rect;
    }

    auto get_edge_rects() const -> const std::unordered_multimap<CardinalDirection, Rect>& {
        return this->edge_rects;
    }

    auto get_edge_rects_any_side() const -> const std::vector<Rect>& {
        return this->edge_rects_any_side;
    }

    auto get_init_genome() const -> const FloorPlanGenome& {
        return this->init_genome;
    }

    auto get_out_rects() const -> const std::vector<Rect>& {
        return this->out_rects;
    }
};

}
