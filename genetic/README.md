# Quinoa

A genetic FPG (floor plan generation) algorithm.

Based on the paper by Darcy Chia and Lyndon While (2014): [_Automated Design of Architectural Layouts Using a 
Multi-Objective Evolutionary Algorithm_](https://dl.acm.org/doi/10.1007/978-3-319-13563-2_64). In Proceedings of the 
10th International Conference on Simulated Evolution and Learning - Volume 8886 (SEAL 2014), pp. 760–772. 
DOI: https://doi.org/10.1007/978-3-319-13563-2_64.

## Submodules

Before proceeding, make sure you have cloned the project recursively (i.e., with submodules), or have have run the
following command:

```shell
git submodule update --init
```

## Building

To simply build the fastest but still correct version of the program, run from this directory:

```shell
# Build out-of-source
mkdir build
cd build

# Create output directory
mkdir out

# Profile program
cmake -DCMAKE_BUILD_TYPE=Release -DQUINOA_AGGRESSIVE_OPTIMIZATION=ON -DQUINOA_PROFILE=gen ..
make
./quinoa-gen ../../doc/schemas/input-real.xml -x schema/input.xsd -y schema/output.xsd --generations 1000 --population-size 500 out
./render -x ./schema/input.xsd -y ./schema/output.xsd out/*.xml

# Compile final program
cmake -DCMAKE_BUILD_TYPE=Release -DQUINOA_AGGRESSIVE_OPTIMIZATION=ON -DQUINOA_PROFILE=use ..
make
```

### Dependencies

The project should be built with CMake 3.18 or later. A (partially) C++20-compliant compiler is required; GCC 10 is
sufficient. Later versions of GCC and modern versions of Clang should also work.

The following tools and libraries are required. The package names are for Debian; other distros may use different names.

* TBB (libtbb2, >= 2020.3-1)
* CodeSynthesis XSD (xsdcxx, >= 4.0.0)
* XercesC (libxerces-c-dev, >= 3.2.3)
* CGAL (libcgal-dev, >= 5.2)
* Cairo (libcairo2-dev, >= 1.16.0)
* The URW Gothic font (fonts-urw-base35, >= 20200910)

Optionally, for documentation generation, Doxygen and its dependencies can be installed:

* Doxygen (doxygen, >= 1.9.1) 
* Mscgen (mscgen, >= 0.20)
* Graphviz (graphviz, >= 2.42.2)
* Dia (dia, >= 0.97.3)

### Compile-time parameters

The standard variable `CMAKE_BUILD_TYPE` controls whether the project is compiled in debugging mode or release mode.
The accepted values (case-sensitive!) are:

* `Debug`: disable optimizations and include debugging symbols.
* `Release`: enable optimizations and exclude debugging symbols.
* `RelWithDebInfo`: enable optimizations and include debugging symbols.
* `MinSizeRel`: enable optimizations and exclude debugging symbols and any extra symbols and relocation information. 
  Also optimize the code for size instead of speed.

If the build type is `Debug`, then `QUINOA_PARANOID_DEBUGGING` can be used to enable Valgrind-like validation at 
run-time, at a significant performance penalty.

For the release-type builds, additional optimizations can be enabled:
* `QUINOA_AGGRESSIVE_OPTIMIZATION` simply enables more optimizations that may or may not speed up the program.
* `QUINOA_UNSAFE_MATH` allows the compiler to violate the IEEE 754 standard and optimize even further, at a 
  correctness cost.
  
### Profile-guided optimization (PGO)

The compiler can be instructed to use profiling information to further improve the program's optimizations. Because
information needs to be collected, it is a two-step process. First, the program should be compiled using 
`QUINOA_PROFILE` set to `gen`. Then, the program should be given a fairly sizeable problem to solve. Finally, the 
program is compiled again using `QUINOA_PROFILE` set to `use`. 

See the start of the "Building" section for a how-to.


## Usage

The floor plan generator has a few command line arguments.

    Usage: ./quinoa-gen [OPTION]... REQS OUT
    Generate floor plans based on requirements file REQS and write solutions to OUT.

    Options:
       -s, --seed=SEED                use this seed for the random number generator (defaults to 6)
       -p, --population-size=POP      keep this amount of individuals in each generation (defaults to 500)
       -g, --generations=GENS         evolve for this number of generations (defaults to 1000)
       -t, --tournament-size=TSZ      use a TSZ-ary tournament as a selection method (defaults to 2, or binary)
       -x, --input-schema-path=ISP    look at this location for the XSD of the requirements file
       -y, --output-schema-path=OSP   look at this location for the XSD of the output files
       -o, --output-reqs              add the requirements to each output file
       -l, --time-limit=SEC           perform evolution for at most SEC seconds, then output current population 
                                      (defaults to 2^32 - 1, a bit more than 136 years)

Make sure that the full path leading to OUT exists, otherwise no solutions will be written.


### Renderer

The floor plan renderer has a similar set of arguments.

    Usage: ./render [OPTION]... FILE...
    Render floor plans to SVG.

    Options:
       -x, --input-schema-path=ISP    look at this location for the XSD of the requirements file
       -y, --output-schema-path=OSP   look at this location for the XSD of the output files
       -r, --input=REQS               use requirements file REQS to calculate each plan's fitness

If an output file includes the requirements (as added by `quinoa-gen -o ...`), then the renderer will also calculate
the fitness components and output a final weighted "cost" based on the input's objective weights. If a weight is
missing, a value of 1 is used.


### Hyperparameters

The default values (500 individuals, 1000 generations, binary tournaments) should suffice. Due to the complexity of
the problem, both a large population and many generations are necessary to 1) find valid solutions and 2) further 
explore the solution space. The large number of individuals allows _some_ diversity once a valid solution is found,
while the number of generations adds extra luck to generate some more valid generations.

In the end, the diversity of the population is often fairly low. One could try with an extremely large population, but
then the generation process will be extremely slow as well.
