#include "catch.hpp"

#include "FloorPlanGenome.hpp"

using namespace Quinoa;

TEST_CASE("touching rects are properly detected") {
    SECTION("non-touching rects") {
        Rect t{0, 0, 1, 1};
        Rect o{2, 2, 1, 1};
        CHECK_FALSE(t.touches(o));
        CHECK_FALSE(o.touches(t));
        CHECK_FALSE(t.get_touching_length(o));
        CHECK_FALSE(o.get_touching_length(t));
    }

    SECTION("rects side-by-side, y-aligned") {
        Rect t{2, 3, 2, 2};
        Rect o{4, 3, 1, 2};
        CHECK(t.touches(o));
        CHECK(o.touches(t));
        CHECK(t.get_touching_length(o) == Edge{2.0, CardinalDirection::EAST});
        CHECK(o.get_touching_length(t) == Edge{2.0, CardinalDirection::WEST});
    }

    SECTION("rects side-by-side, y-misaligned, right higher") {
        Rect t{1, 3, 0.5, 2};
        Rect o{1.5, 4, 4, 3};
        CHECK(t.touches(o));
        CHECK(o.touches(t));
        CHECK(t.get_touching_length(o) == Edge{1, CardinalDirection::EAST});
        CHECK(o.get_touching_length(t) == Edge{1, CardinalDirection::WEST});
    }

    SECTION("rects side-by-side, y-misaligned, right lower") {
        Rect t{1, 3, 0.5, 9};
        Rect o{1.5, 1, 4, 5};
        CHECK(t.touches(o));
        CHECK(o.touches(t));
        CHECK(t.get_touching_length(o) == Edge{3, CardinalDirection::EAST});
        CHECK(o.get_touching_length(t) == Edge{3, CardinalDirection::WEST});
    }

    SECTION("rects stacked, x-aligned") {
        Rect t{-4, 2, 1, 3};
        Rect o{-4, 5, 1, 3};
        CHECK(t.touches(o));
        CHECK(o.touches(t));
        CHECK(t.get_touching_length(o) == Edge{1, CardinalDirection::NORTH});
        CHECK(o.get_touching_length(t) == Edge{1, CardinalDirection::SOUTH});
    }

    SECTION("rects stacked, x-misaligned, upper to the right") {
        Rect t{-10, -9, 4, 3};
        Rect o{-9, -6, 6, 1};
        CHECK(t.touches(o));
        CHECK(o.touches(t));
        CHECK(t.get_touching_length(o) == Edge{3, CardinalDirection::NORTH});
        CHECK(o.get_touching_length(t) == Edge{3, CardinalDirection::SOUTH});
    }

    SECTION("rects stacked, x-misaligned, upper to the left") {
        Rect t{4.1, 1.9, 5.4, 2.9};
        Rect o{1.7, 4.8, 4.9, 3.0};
        CHECK(t.touches(o));
        CHECK(o.touches(t));
        CHECK(t.get_touching_length(o) == Edge{2.5, CardinalDirection::NORTH});
        CHECK(o.get_touching_length(t) == Edge{2.5, CardinalDirection::SOUTH});
    }

    SECTION("touching corners") {
        Rect t{0, 0, 1, 1};
        Rect o{1, 1, 1, 1};
        CHECK(t.touches(o));
        CHECK(o.touches(t));
        CHECK_FALSE(t.get_touching_length(o));
        CHECK_FALSE(o.get_touching_length(t));
    }
}
