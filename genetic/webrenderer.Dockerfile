FROM debian:testing-slim

RUN apt update \
 && apt -y upgrade \
 && apt -y install php7.4 php7.4-fpm php7.4-zip nginx libtbb2 libcairo2 libxerces-c3.2

COPY cmake-build-debug/render /usr/local/bin/render
COPY cmake-build-debug/schema/* /var/lib/afpg-render/
COPY websrc/htdocs/* /var/www/html/
COPY websrc/config/nginx.conf /etc/nginx/

CMD service php7.4-fpm start && service nginx start && sleep inf
