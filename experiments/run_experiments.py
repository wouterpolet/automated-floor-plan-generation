import os
import subprocess
from datetime import datetime


def run(do_command, output_folder):
    real = [os.path.join('samples/real', file) for file in os.listdir('samples/real') if file.endswith('.xml')]
    print(f'Found {len(real)} real instances')
    for i, input_path in enumerate(real):
        print(f'Running {input_path}')
        res_folder = os.path.splitext(os.path.basename(input_path))[0]
        out_folder = f'{output_folder}/real/{res_folder}'
        subprocess.run(f'mkdir -p {out_folder}', shell=True)
        do_command(input_path, f'{out_folder}/result.xml', f'{out_folder}/stdout.txt')
        print(f"Done {i+1}/{len(real)} at {datetime.now().strftime('%H:%M:%S')}")

    synthetic = [os.path.join('samples/synthetic', file) for file in os.listdir('samples/synthetic') if file.endswith('.xml')]
    print(f'\nFound {len(synthetic)} synthetic instances')
    for i, input_path in enumerate(synthetic):
        print(f'Running {input_path}')
        res_folder = os.path.splitext(os.path.basename(input_path))[0]
        out_folder = f'{output_folder}/synthetic/{res_folder}'
        subprocess.run(f'mkdir -p {out_folder}', shell=True)
        do_command(input_path, f'{out_folder}/result.xml', f'{out_folder}/stdout.txt')
        print(f"Done {i+1}/{len(synthetic)} at {datetime.now().strftime('%H:%M:%S')}")


def run_integer(xml_in, xml_out, std_out):
    subprocess.run(f'(time python ../python/integer/solver.py {xml_in} {xml_out}) &>> {std_out}', shell=True)


def run_genetic(xml_in, xml_out, std_out):
    out_path = os.path.splitext(xml_out)[0]
    subprocess.run(f'mkdir -p {out_path}', shell=True)
    subprocess.run(f'(time ../genetic/build/quinoa-gen -x ../doc/schemas/input.xsd -y ../doc/schemas/output.xsd --time-limit 1200 {xml_in} {out_path}) &>> {std_out}', shell=True)


def run_max_sat(xml_in, xml_out, std_out):
    subprocess.run(f'(time python ../python/max-sat/max-sat.py {xml_in} {xml_out}) &>> {std_out}', shell=True)


def run_constraint(xml_in, xml_out, std_out):
    minizinc_path = os.path.join(os.path.dirname(std_out), 'mini_output.txt')
    subprocess.run(f'(time python ../python/constraint/run.py {xml_in} {xml_out} 1 {minizinc_path}) &>> {std_out}', shell=True)


results_folder = 'results'
print('\nRunning integer')
run(run_integer, os.path.join(results_folder, 'integer'))

print('\nRunning genetic')
run(run_genetic, os.path.join(results_folder, 'genetic'))

print('\nRunning max sat')
run(run_max_sat, os.path.join(results_folder, 'max-sat'))

print('\nRunning constraint')
run(run_constraint, os.path.join(results_folder, 'constraint'))

