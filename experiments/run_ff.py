import re
import subprocess
import csv
from pathlib import Path

try:
    import tqdm
except:
    pass


results_path = Path('results')
samples_path = Path('samples')
fitness_path = Path('fitness')
renderer_dir = Path('../genetic/build')
sample_path_re = re.compile(r'samples/(real|synthetic)/([^.]+).xml')

solver_order = ['LS', 'IP', 'CP', 'MS']


def guess_solver_id(path):
    if 'genetic' in path:
        return 'LS'
    if 'integer' in path:
        return 'IP'
    if 'constraint' in path:
        return 'CP'
    if 'max-sat' in path:
        return 'MS'
    raise RuntimeError(f'Bad solver ID {path}')

def ss(solvers):
    return sorted([*solvers], key=lambda s: solver_order.index(s))

def sl(solvers):
    return ', '.join(ss(solvers))


def main():
    fitness_path.mkdir(exist_ok=True)

    summary = []

    samples = [*samples_path.glob('*/*.xml')]
    if tqdm:
        samples = tqdm.tqdm(samples)
    for sample in samples:
        m = sample_path_re.match(str(sample))
        sample_rw  = m.group(1)
        sample_name = m.group(2)

        # results = results_path.glob(f'*/{sample_rw}/{sample_name}/*.xml')
        results = results_path.glob(f'*/{sample_rw}/{sample_name}/**/*.xml')
        
        cmd = [
            renderer_dir / 'render',
            '-x', renderer_dir / 'schema/input.xsd',
            '-y', renderer_dir / 'schema/output.xsd',
            '-r', sample,
            *results,
        ]
        proc = subprocess.run(cmd, capture_output=True, encoding='UTF-8', check=True)

        fitness_reader = csv.DictReader(proc.stdout.split('\n'), delimiter=';')
        fitness_o = [*fitness_reader]
        fitness = []
        first_genetic_seen = False
        for f in fitness_o:
            if '/genetic/' in f['file']:
                if not first_genetic_seen:
                    fitness.append(f)
                    first_genetic_seen = True
            else:
                fitness.append(f)

        fitness.sort(key=lambda r: float(r['total']))

        hard_constraints = ['adjacency', 'disjointedness', 'reachability', 'bounds']
        hard_constraints_ex_pp = ['adjacency', 'disjointedness', 'bounds']
        hard_constraints_ex_bounds = ['adjacency', 'disjointedness', 'reachability']
        hard_constraints_ex_pp_bounds = ['adjacency', 'disjointedness']

        perfect = set()
        valid = set()
        valid_ex_pp = set()
        valid_ex_bounds = set()
        valid_ex_pp_bounds = set()
        ranking = [set(), set(), set(), set()]

        ffile_path = fitness_path / f'{sample_rw}-{sample_name}.csv'
        with ffile_path.open(mode='w', encoding='UTF-8') as ffile:
            writer = csv.DictWriter(ffile, fitness[0].keys())
            writer.writeheader()
            prev_best = -1
            prev_idx = -1
            for f in fitness:
                sid = guess_solver_id(f['file'])
                if float(f['total']) < 0.00001:
                    perfect.add(sid)
                if all(float(f[k]) < 0.00001 for k in hard_constraints):
                    valid.add(sid)
                if all(float(f[k]) < 0.00001 for k in hard_constraints_ex_pp):
                    valid_ex_pp.add(sid)
                if all(float(f[k]) < 0.00001 for k in hard_constraints_ex_bounds):
                    valid_ex_bounds.add(sid)
                if all(float(f[k]) < 0.00001 for k in hard_constraints_ex_pp_bounds):
                    valid_ex_pp_bounds.add(sid)
                if abs(float(f['total']) - prev_best) > 0.00001:
                    prev_idx += 1
                ranking[prev_idx].add(sid)
                prev_best = float(f['total'])

                writer.writerow(f)

        summary.append({
            'Instance': f'{sample_rw} {sample_name}',
            'Perfect': sl(perfect),
            'Valid': sl(valid),
            'Valid ex. p/p': sl(valid_ex_pp),
            'Valid ex. bounds': sl(valid_ex_bounds),
            'Valid ex. p/p & bounds': sl(valid_ex_pp_bounds),
            '1st': sl(ranking[0]),
            '2nd': sl(ranking[1]),
            '3rd': sl(ranking[2]),
            '4th': sl(ranking[3]),
        })

    summary.sort(key=lambda s: s['Instance'])

    sfile_path = fitness_path / 'summary.csv'
    with sfile_path.open(mode='w', encoding='UTF-8') as sfile:
        writer = csv.DictWriter(sfile, summary[0].keys())
        writer.writeheader()

        for s in summary:
            writer.writerow(s)



if __name__ == '__main__':
    main()
