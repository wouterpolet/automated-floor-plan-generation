import math
import sys

#read in parameters
tile_size = 1 #meters
numrooms = int(sys.argv[1])
n = int(int(sys.argv[2])/tile_size)
m = int(int(sys.argv[3])/tile_size)

min_area = []
max_area = []

for room in range(0, numrooms):
    min_area.append(int(math.ceil(float(sys.argv[4 + 2*room]))/tile_size))
    max_area.append(int(math.ceil(float(sys.argv[5 + 2*room]))/tile_size))

print(min_area, max_area)
one_hot_vars = {}
roomvarmap = {}
numvar_one_hot = n*m*numrooms
num_var_per_roomtype = n*m
clauses = []
atmosts = []

#Create the first clause so others can be built from it
initial_clause = []
initclause = ""
for i in range(0, numrooms):
    initial_clause.append(i+1)
    initclause += str(i+1) + " "
one_hot_vars[1] = initial_clause
clauses.append(initclause + "0\n")
atmosts.append(initclause + "<= 1\n")

#Create clauses used to denote the onehot vars that are assigned to each tile
for i in range(1, int(n*m)):
    clausestr = ""
    clause = []
    for j in range(0, numrooms):
        clausestr += str(initial_clause[j] + numrooms*i) + " "
        clause.append(initial_clause[j] + numrooms*i)
    one_hot_vars[i+1] = clause
    clauses.append(clausestr + "0\n")
    atmosts.append(clausestr + "<= 1\n")

# print(one_hot_vars)

#Create atmost clauses to limit the area a room can be
for i in range(0, numrooms):
    at_most = [i+1]
    cardstr = ""
    for j in range(1, int(n*m)):
        at_most.append(at_most[len(at_most) - 1] + numrooms)
    for j in at_most:
        cardstr = cardstr + str(j) + " "

    atmosts.append(cardstr + "<= " + str(max_area[i]) + "\n")
    atmosts.append(cardstr + ">= " + str(min_area[i]) + "\n")

#Create the cardinality for the corner per room variables
for i in range(0, numrooms):
    vars_roomtype = list(range(int(numvar_one_hot + 1 + i * num_var_per_roomtype), int(numvar_one_hot + (i + 1) * num_var_per_roomtype+1)))
    roomvarmap[i] = vars_roomtype
    cardstr = ""
    for j in vars_roomtype:
        cardstr += str(j) + " "
    atmosts.append(cardstr + "<= 2\n")
    atmosts.append(cardstr + ">= 2\n")

# print(roomvarmap)
#Cardinality clause to only allow a corner position to be set to one room
for i in range(0, int(n*m)):
    cardstr = ""
    for room in roomvarmap.keys():
        cardstr += str(roomvarmap.get(room)[i]) + " "
    cardstr += "<= 1\n"
    atmosts.append(cardstr)

#Create 'corners => one-hot vars' clauses for all corner possibilities
for room in roomvarmap.keys():
    for i in roomvarmap.get(room):
        for j in roomvarmap.get(room):
            # if j == i:
            #     implied_vars = [j - (numrooms + room) * num_var_per_roomtype]
            #     for tile in one_hot_vars.keys():
            #         var = one_hot_vars.get(tile)[room]
            #         if var not in implied_vars:
            #             implied_vars.append(-var)
            #     for var in implied_vars:
            #         clauses.append(str(-i) + " " + str(var) + " 0\n")
            if j > i:
                y_i = math.floor((i - (numrooms + room) * num_var_per_roomtype) / n)
                y_j = math.floor((j - (numrooms + room) * num_var_per_roomtype) / n)
                left = 0
                bottom = j
                if i % n == 0 and j % n == 0:
                    left = i
                    y_i -= 1
                    y_j -= 1
                elif i % n == 0 and j % n != 0:
                    left = j
                    y_i -= 1
                elif j % n == 0 and i % n != 0:
                    left = i
                    y_j -= 1
                elif i % n < j % n:
                    left = i
                else:
                    left = j
                y_diff = abs(y_i - y_j)
                x_diff = abs((j-y_diff*n) - i)
                implied_vars = []
                start_num = left - (numrooms + room) * num_var_per_roomtype
                if bottom == left:
                    start_num = left - (numrooms + room) * num_var_per_roomtype - y_diff * n
                for y in range(0, int(y_diff) + 1):
                    for x in range(0, int(x_diff) + 1):
                        implied_vars.append(one_hot_vars.get(y * n + x + start_num)[room])
                for tile in one_hot_vars.keys():
                    var = one_hot_vars.get(tile)[room]
                    if var not in implied_vars:
                        implied_vars.append(-var)
                for var in implied_vars:
                    clauses.append(str(-i) + " " + str(-j) + " " + str(var) + " 0\n")

clauses[-1] = clauses[-1][:-1]
numvar = numvar_one_hot + num_var_per_roomtype*numrooms
first_line = "p cnf+ " + str(numvar) + " " + str(len(clauses) + len(atmosts)) + "\n"
f = open('dimacs.cnfp', 'w+')
f.write(first_line)
for i in atmosts:
    f.write(i)
for i in clauses:
    f.write(i)
