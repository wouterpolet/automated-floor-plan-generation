import math
import os
import sys
import time
from pysat.solvers import *
from pysat.formula import CNFPlus
import xml.etree.ElementTree as ET
from threading import Timer

#Parameter reading from input xml
if len(sys.argv) != 3:
    sys.exit("please provide input and output file")

input_params = ET.parse(sys.argv[1]).getroot()
xlmns = input_params.tag[:-21]
floor = input_params.iter(xlmns + 'Floor')
n = 0
m = 0
for elem in floor:
    for point1 in elem:
        for point2 in elem:
            x1 = int(math.floor(float(point1.attrib.get('x'))))
            x2 = int(math.floor(float(point2.attrib.get('x'))))
            y1 = int(math.floor(float(point1.attrib.get('y'))))
            y2 = int(math.floor(float(point2.attrib.get('y'))))
            n = max(abs(x1-x2), n)
            m = max(abs(y1-y2), m)

n = math.ceil(n)
m = math.ceil(m)
numrooms = 0
min_max_call = ""
roomtypes = []
for elem in input_params.findall(xlmns + 'RoomTypes'):
    for elem2 in elem:
        numrooms += 1
        roomtypes.append(elem2.attrib.get('name'))
        min_max_call += " " + elem2.find(xlmns + 'Area').attrib.get('min') + " " + elem2.find(xlmns + 'Area').attrib.get('max')

#Generate required dimacs file
print("generating dimacs")
call = "python " + os.path.join(os.path.dirname(__file__), "dimacs_gen.py ") + str(numrooms) + " " + str(n) + " " + str(m) + min_max_call
print(call)
os.system(call)
cnf = CNFPlus(from_file='dimacs.cnfp')
print("done generating dimacs")

s = Solver(name='gluecard4', bootstrap_with=cnf.clauses)
for atm in cnf.atmosts:
    s.add_atmost(*atm)

# 20 minutes timeout
timer = Timer(1200, lambda x: x.interrupt(), [s])

print("solving")
start_time = time.time()
timer.start()
print(s.solve_limited(expect_interrupt=True))
print("--- %s seconds ---" % (time.time() - start_time))
print(s.get_model())

# Corner determination in order to create output xml
room_corner_map = {}
for j in range(0, numrooms):
    corners = []
    for i in s.get_model():
        if cnf.nv/2 + j * n * m < i <= cnf.nv/2 + (j+1) * n * m:
            corners.append(i)
    room_corner_map[j] = corners

polygons = {}
for room, item in room_corner_map.items():
    corners = []
    corner1 = item[0]
    corner2 = item[1]
    left = 0
    bottom = corner2
    y_1 = math.floor((corner1 - (numrooms + room) * n*m) / n)
    y_2 = math.floor((corner2 - (numrooms + room) * n*m) / n)
    if corner1 % n == 0 and corner2 % n == 0:
        left = corner1
        y_1 -= 1
        y_2 -= 1
    elif corner1 % n == 0 and corner2 % n != 0:
        left = corner2
        y_1 -= 1
    elif corner2 % n == 0 and corner1 % n != 0:
        left = corner1
        y_2 -= 1
    elif corner1 % n < corner2 % n:
        left = corner1
    else:
        left = corner2
    y_diff = abs(y_1 - y_2)
    x_diff = abs((corner2 - y_diff * n) - corner1)
    print(corner1 - n*m*numrooms - (room * n * m) ,corner2 - n*m*numrooms - (room * n * m), left - n*m*numrooms - (room * n * m))
    if left == bottom:
        topleft = left - y_diff * n - n*m*numrooms - (room * n * m)
        row = math.ceil(topleft / n) - 1
        col = topleft - (row * n+1)
        corners.append([col, m-row])
        col += x_diff + 1
        corners.append([col, m-row])
        row += y_diff+1
        corners.append([col, m-row])
        col -= x_diff + 1
        corners.append([col, m - row])
    else:
        topleft = left - n * m * numrooms - (room * n * m)
        row = math.ceil(topleft / n) - 1
        col = topleft - row * n - 1
        corners.append([col, m - row])
        col += x_diff + 1
        corners.append([col, m - row])
        row += y_diff + 1
        corners.append([col, m - row])
        col -= x_diff + 1
        corners.append([col, m - row])
    if roomtypes[room] not in polygons:
        polygons[roomtypes[room]] = corners
    else:
        mapping = polygons.get(roomtypes[room])
        for corner in corners:
            mapping.append(corner)
        polygons[roomtypes[room]] = mapping

s.delete()
# This code can probably be replaced by a library, for now it suffices
with open(sys.argv[2], 'w') as out_file:
    out_file.write("""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<fpo:FloorPlan xmlns:fpo="urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4" xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed input.xsd urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4 output.xsd">
    <fpo:Floor>\n""")
    for keys, items in polygons.items():
        for room in range(0, int(len(items)/4)):
            room_xml = "          <fpo:Room instance=\"" + str(room + 1) +  "\" type=\"" + keys + "\">\n" \
                        "             <fpo:Geometry>\n"
            for point in range((room+1)*4 -1, room*4 -1, -1):
                if items[point][0] == 0:
                    items[point][0] = "-0"
                if items[point][1] == 0:
                    items[point][1] = "-0"
                room_xml += "                 <fpi:Point x=\"" + str(items[point][0]) + "\" y=\"" + str(items[point][1]) + "\"/>\n"
            room_xml += """             </fpo:Geometry>
          </fpo:Room>\n"""
            out_file.write(room_xml)
    out_file.write("""    </fpo:Floor>
</fpo:FloorPlan>""")
