import sys
import time
from typing import List, Tuple

import gurobipy as gp
from gurobipy import GRB
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.patches as patches

import reader
import writer

def callback(model, where):
    if where == GRB.Callback.MIPSOL and not first_sol_found:
        sol_times.append(time.time() - start)


def generate_layout(w_s: int, d_s: int, n: int, c: int, rooms: List[str],
                    min_w: np.ndarray, max_w: np.ndarray, min_d: np.ndarray,
                    max_d: np.ndarray, min_r: np.ndarray, max_r: np.ndarray,
                    pref_w: np.ndarray, pref_d: np.ndarray,
                    adj: List[List[int]], public: List[int], voids: List[Tuple], front_door: int,
                    output_file: str, min_areas: np.ndarray, max_areas: np.ndarray, scale:int, take_first_solution, timelimit, visualize, num=1):
    start_solver = time.time()

    M = 2 * w_s + 2 * d_s
    old_n = n

    e = 0.001

    # Update input with void areas as rooms
    for x_temp, y_temp, w_temp, d_temp in voids:
        n = n + 1
        rooms.append("void")
        min_w = np.append(min_w, w_temp)
        max_w = np.append(max_w, w_temp)
        min_d = np.append(min_d, d_temp)
        max_d = np.append(max_d, d_temp)
        min_r = np.append(min_r, 1)
        max_r = np.append(max_r, 2 * max(w_temp, d_temp) / min(w_temp, d_temp))
        pref_w = np.append(pref_w, w_temp)
        pref_d = np.append(pref_d, d_temp)
        min_areas = np.append(min_areas, w_temp * d_temp)
        max_areas = np.append(max_areas, w_temp * d_temp)
        for l in adj:
            l.append(0)
        adj.append([0] * n)
        public.append(False)

    try:
        # Create a new model
        m = gp.Model("AFPG")
        m.setParam('NonConvex', 2)
        if num > 1:
            m.setParam('PoolSearchMode', 2)
            m.setParam('PoolSolutions', num)

        if take_first_solution:
            m.setParam('SolutionLimit', 1)

        m.setParam('TimeLimit', timelimit)

        # Create variables
        # Room layout
        x = m.addMVar(shape=(n,), lb=0, ub=w_s, vtype=GRB.INTEGER, name="x")
        y = m.addMVar(shape=(n,), lb=0, ub=d_s, vtype=GRB.INTEGER, name="y")
        w = m.addMVar(shape=(n,), lb=0, ub=w_s, vtype=GRB.INTEGER, name="w")
        d = m.addMVar(shape=(n,), lb=0, ub=d_s, vtype=GRB.INTEGER, name="d")

        # Room non-overlap
        sigma_right = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="sigma_right")
        sigma_left = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="sigma_left")
        sigma_front = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="sigma_front")
        sigma_back = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="sigma_back")

        # Room aspect ratio (direction of the room)
        sigma = m.addMVar(shape=(n,), vtype=GRB.BINARY, name="room_vertical")

        # Room adjacency
        theta = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="theta")

        adjacent_right_up = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")
        adjacent_right_down = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")
        adjacent_left_up = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")
        adjacent_left_down = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")
        adjacent_front_right = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")
        adjacent_front_left = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")
        adjacent_back_right = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")
        adjacent_back_left = m.addMVar(shape=(n, n), vtype=GRB.BINARY, name="adjacent")

        adjacent = m.addMVar(shape=(n,n), vtype=GRB.BINARY, name="adjacent")

        connected = m.addMVar(shape=(n,), vtype=GRB.BINARY, name="connected")

        diff_w = m.addMVar(shape=(n,), lb=-w_s, ub=w_s, vtype=GRB.CONTINUOUS, name="diff_w")
        diff_d = m.addMVar(shape=(n,), lb=-d_s, ub=d_s, vtype=GRB.CONTINUOUS, name="diff_d")
        abs_w = m.addMVar(shape=(n,), lb=-w_s, ub=w_s, vtype=GRB.CONTINUOUS, name="abs_w")
        abs_d = m.addMVar(shape=(n,), lb=-d_s, ub=d_s, vtype=GRB.CONTINUOUS, name="abs_d")

        adjacent_objective = -adjacent.sum()

        coverage_objective = w_s * d_s - sum(w[i] @ d[i] for i in range(n))

        size_objective = abs_w.sum() + abs_d.sum()

        connected_objective = -connected.sum()

        # Set objective
        m.setObjective(coverage_objective + size_objective + adjacent_objective + connected_objective, GRB.MINIMIZE)

        # Add constaints
        m.addConstr(0 <= x, name="x_in_lower_bound")
        m.addConstr(x <= w_s, name="x_in_upper_bound")
        m.addConstr(0 <= y, name="y_in_lower_bound")
        m.addConstr(y <= d_s, name="y_in_upper_bound")
        m.addConstr(x + w <= w_s, name="x_total_in_bound")
        m.addConstr(y + d <= d_s, name="y_total_in_bound")

        # Add constraints for non overlap
        for i in range(n):
            for j in range(i + 1, n):
                m.addConstr(x[i] - w[j] >= x[j] - M * (1 - sigma_right[i][j]), "non_overlap_right_{}_{}".format(i, j))
                m.addConstr(x[i] + w[i] <= x[j] + M * (1 - sigma_left[i][j]), "non_overlap_left_{}_{}".format(i, j))
                m.addConstr(y[i] - d[j] >= y[j] - M * (1 - sigma_front[i][j]), "non_overlap_front_{}_{}".format(i, j))
                m.addConstr(y[i] + d[i] <= y[j] + M * (1 - sigma_back[i][j]), "non_overlap_back_{}_{}".format(i, j))
                m.addConstr(sigma_right[i][j] + sigma_left[i][j] + sigma_front[i][j] + sigma_back[i][j] >= 1,
                            name="at_least_one_sigma_{}_{}".format(i, j))

        # Add constraints for room size
        m.addConstr(min_w <= w, name="min_w_size")
        m.addConstr(w <= max_w, name="max_w_size")
        m.addConstr(min_d <= d, name="min_d_size")
        m.addConstr(d <= max_d, name="max_d_size")

        # Add constraints for room aspect ratio
        for i in range(n):
            m.addConstr(min_r[i] * d[i] <= w[i] + M * sigma[i], name="aspect_ratio_min_horizontal_{}".format(i))
            m.addConstr(max_r[i] * d[i] >= w[i] - M * sigma[i], name="aspect_ratio_max_horizontal_{}".format(i))
            m.addConstr(min_r[i] * w[i] <= d[i] + M * (1 - sigma[i]), name="aspect_ratio_min_vertical_{}".format(i))
            m.addConstr(max_r[i] * w[i] >= d[i] - M * (1 - sigma[i]), name="aspect_ratio_max_vertical_{}".format(i))

        # Add constraints for area
        for i in range(n):
            m.addConstr(w[i] @ d[i] >= min_areas[i])
            m.addConstr(w[i] @ d[i] <= max_areas[i])

        # Add constraints for room adjacency
        for i in range(n - 1):
            for j in range(i + 1, n):
                if adj[i][j]:
                    m.addConstr(x[i] <= x[j] + w[j] - c * theta[i, j],
                                name="adjacency_horizontal_{}_{}_{}".format(j, i, j))
                    m.addConstr(x[i] + w[i] >= x[j] + c * theta[i, j],
                                name="adjacency_horizontal_{}_{}_{}".format(i, i, j))
                    m.addConstr(y[i] <= y[j] + d[j] - c * (1 - theta[i, j]),
                                name="adjacency_vertical_{}_{}_{}".format(j, i, j))
                    m.addConstr(y[i] + d[i] >= y[j] + c * (1 - theta[i, j]),
                                name="adjacency_vertical_{}_{}_{}".format(i, i, j))

        # Hardcoded constraints for void area
        for idx, (x_temp, y_temp, w_temp, d_temp) in enumerate(voids):
            m.addConstr(x[old_n + idx] == x_temp, name="void_x")
            m.addConstr(y[old_n + idx] == y_temp, name="void_y")
            m.addConstr(w[old_n + idx] == w_temp, name="void_w")
            m.addConstr(d[old_n + idx] == d_temp, name="void_d")

        for i in range(n):
            for j in range(n):
                m.addConstr((adjacent_right_up[i][j].tolist()[0] == 1) >> (
                        x[i].tolist()[0] == x[j].tolist()[0] + w[j].tolist()[0]))
                m.addConstr((adjacent_right_down[i][j].tolist()[0] == 1) >> (
                        x[i].tolist()[0] == x[j].tolist()[0] + w[j].tolist()[0]))
                m.addConstr((adjacent_right_up[i][j].tolist()[0] == 1) >> (
                        y[i].tolist()[0] <= y[j].tolist()[0] + d[j].tolist()[0] - c))
                m.addConstr((adjacent_right_down[i][j].tolist()[0] == 1) >> (
                        y[j].tolist()[0] <= y[i].tolist()[0] + d[i].tolist()[0] - c))
                m.addConstr((adjacent_right_up[i][j].tolist()[0] == 1) >> (y[i].tolist()[0] >= y[j].tolist()[0]))
                m.addConstr((adjacent_right_down[i][j].tolist()[0] == 1) >> (y[j].tolist()[0] >= y[i].tolist()[0]))

                m.addConstr((adjacent_left_up[i][j].tolist()[0] == 1) >> (
                        x[j].tolist()[0] == x[i].tolist()[0] + w[i].tolist()[0]))
                m.addConstr((adjacent_left_down[i][j].tolist()[0] == 1) >> (
                        x[j].tolist()[0] == x[i].tolist()[0] + w[i].tolist()[0]))
                m.addConstr((adjacent_left_up[i][j].tolist()[0] == 1) >> (
                        y[i].tolist()[0] <= y[j].tolist()[0] + d[j].tolist()[0] - c))
                m.addConstr((adjacent_left_down[i][j].tolist()[0] == 1) >> (
                        y[j].tolist()[0] <= y[i].tolist()[0] + d[i].tolist()[0] - c))
                m.addConstr((adjacent_left_up[i][j].tolist()[0] == 1) >> (y[i].tolist()[0] >= y[j].tolist()[0]))
                m.addConstr((adjacent_left_down[i][j].tolist()[0] == 1) >> (y[j].tolist()[0] >= y[i].tolist()[0]))

                m.addConstr((adjacent_front_right[i][j].tolist()[0] == 1) >> (
                        y[i].tolist()[0] == y[j].tolist()[0] + d[j].tolist()[0]))
                m.addConstr((adjacent_front_left[i][j].tolist()[0] == 1) >> (
                        y[i].tolist()[0] == y[j].tolist()[0] + d[j].tolist()[0]))
                m.addConstr((adjacent_front_right[i][j].tolist()[0] == 1) >> (
                        x[i].tolist()[0] <= x[j].tolist()[0] + w[j].tolist()[0] - c))
                m.addConstr((adjacent_front_left[i][j].tolist()[0] == 1) >> (
                        x[j].tolist()[0] <= x[i].tolist()[0] + w[i].tolist()[0] - c))
                m.addConstr((adjacent_front_right[i][j].tolist()[0] == 1) >> (
                        x[i].tolist()[0] >= x[j].tolist()[0]))
                m.addConstr((adjacent_front_left[i][j].tolist()[0] == 1) >> (
                        x[j].tolist()[0] >= x[i].tolist()[0]))

                m.addConstr((adjacent_back_right[i][j].tolist()[0] == 1) >> (
                        y[j].tolist()[0] == y[i].tolist()[0] + d[i].tolist()[0]))
                m.addConstr((adjacent_back_left[i][j].tolist()[0] == 1) >> (
                        y[j].tolist()[0] == y[i].tolist()[0] + d[i].tolist()[0]))
                m.addConstr((adjacent_back_right[i][j].tolist()[0] == 1) >> (
                        x[i].tolist()[0] <= x[j].tolist()[0] + w[j].tolist()[0] - c))
                m.addConstr((adjacent_back_left[i][j].tolist()[0] == 1) >> (
                        x[j].tolist()[0] <= x[i].tolist()[0] + w[i].tolist()[0] - c))
                m.addConstr((adjacent_back_right[i][j].tolist()[0] == 1) >> (
                        x[i].tolist()[0] >= x[j].tolist()[0]))
                m.addConstr((adjacent_back_left[i][j].tolist()[0] == 1) >> (
                        x[j].tolist()[0] >= x[i].tolist()[0]))

        for i in range(n):
            if public[i]:
                m.addConstr(sum(public[j] * adjacent_right_up[i][j] for j in range(n)) +
                            sum(public[j] * adjacent_right_down[i][j] for j in range(n)) +
                            sum(public[j] * adjacent_left_up[i][j] for j in range(n)) +
                            sum(public[j] * adjacent_left_down[i][j] for j in range(n)) +
                            sum(public[j] * adjacent_front_right[i][j] for j in range(n)) +
                            sum(public[j] * adjacent_front_left[i][j] for j in range(n)) +
                            sum(public[j] * adjacent_back_right[i][j] for j in range(n)) +
                            sum(public[j] * adjacent_back_left[i][j] for j in range(n)) >= 1)

        m.addConstr(diff_w == w - pref_w)
        m.addConstr(diff_d == d - pref_d)

        for i in range(n):
            for j in range(n):
                m.addConstr(adjacent_right_up[i][j] + adjacent_right_down[i][j] + adjacent_left_up[i][j] + adjacent_left_down[i][j]
                    + adjacent_front_right[i][j] + adjacent_front_left[i][j] + adjacent_back_right[i][j] + adjacent_back_left[i][j]
                            <= 1 - e + (n * n - 1 + e) * adjacent[i][j])
                m.addConstr(adjacent_right_up[i][j] + adjacent_right_down[i][j] + adjacent_left_up[i][j] +
                            adjacent_left_down[i][j]
                            + adjacent_front_right[i][j] + adjacent_front_left[i][j] + adjacent_back_right[i][j] +
                            adjacent_back_left[i][j]
                            >= adjacent[i][j])

        for i in range(n):
            m.addGenConstrAbs(abs_w[i].tolist()[0], diff_w[i].tolist()[0])
            m.addGenConstrAbs(abs_d[i].tolist()[0], diff_d[i].tolist()[0])

        publics = [i for i, x in enumerate(public) if x == 1]
        for k in range(len(publics)):
            if k != publics.index(front_door):
                flow = m.addMVar(shape=(len(publics),len(publics)), lb=-1, ub=1, vtype=GRB.INTEGER, name="flow")
                for i in range(len(publics)):
                    for j in range(len(publics)):
                        m.addConstr(flow[i][j] <= adjacent[publics[i]][publics[j]])
                        m.addConstr(-flow[i][j] <= adjacent[publics[i]][publics[j]])
                        if publics[j] != front_door and j != k:
                            m.addConstr(sum([flow[x][j] for x in range(len(publics))]) - sum([flow[j][x] for x in range(len(publics))]) == 0)
                        m.addConstr(sum([flow[x][publics.index(front_door)] for x in range(len(publics))]) - sum([flow[publics.index(front_door)][x] for x in range(len(publics))]) == -1)
                        m.addConstr(sum([flow[x][k] for x in range(len(publics))]) - sum([flow[k][x] for x in range(len(publics))]) == 1)

        # Optimize model
        m.optimize(callback)

        print("Runtime Total Solver: " + str(time.time() - start_solver))
        print("Runtime optimizer: " + str(m.Runtime))
        print("Solution times: " + str(sol_times[0] if len(sol_times) > 0 else None))
        print("Solution times: " + str(sol_times))

        if num > 1:
            for i in range(m.getAttr(GRB.Attr.SolCount)):
                m.setParam('SolutionNumber', i)
                if visualize:
                    plot(x, y, w, d, n, w_s, d_s, rooms)
                writer.write_output(rooms[:old_n], x.X[:old_n], y.X[:old_n], w.X[:old_n], d.X[:old_n],
                                    output_file.rstrip(".xml") + "_" + str(i) + ".xml", scale)
        else:
            if visualize:
                plot(x, y, w, d, n, w_s, d_s, rooms)
            writer.write_output(rooms[:old_n], x.X[:old_n], y.X[:old_n], w.X[:old_n], d.X[:old_n], output_file, scale)
    except gp.GurobiError as e:
        print('Error codax.add_patch(e ' + str(e.errno) + ":) " + str(e))


def plot(x, y, w, d, n, w_s, d_s, rooms):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cmap = plt.cm.get_cmap('hsv', n)

    ax.scatter(x=[0, w_s], y=[0, d_s], s=0)

    ax.add_patch(patches.Rectangle(xy=(0, 0), width=w_s, height=d_s, color='gray', fill=True))

    for i in range(n):
        if rooms[i] == "void":
            ax.add_patch(
                patches.Rectangle(xy=(x.Xn[i], y.Xn[i]), width=w.Xn[i], height=d.Xn[i], color='black', fill=True))
        else:
            ax.add_patch(patches.Rectangle(xy=(x.Xn[i], y.Xn[i]), width=w.Xn[i], height=d.Xn[i], edgecolor=cmap(i),
                                           facecolor='white', fill=True))
            ax.annotate(rooms[i], (x.Xn[i] + w.Xn[i] / 2, y.Xn[i] + d.Xn[i] / 2), weight='bold', ha='center',
                        va='center')

    plt.xlim((0, max(w_s, d_s)))
    plt.ylim((0, max(w_s, d_s)))

    if w_s > d_s:
        ax.add_patch(patches.Rectangle(xy=(0, d_s), width=w_s, height=w_s - d_s, color='black', fill=True))
    elif d_s > w_s:
        ax.add_patch(patches.Rectangle(xy=(w_s, 0), width=d_s - w_s, height=d_s, color='black', fill=True))

    plt.show()


def solve(input_file, output_file, num, scale, timelimit, take_first_solution, visualize):
    input_w, input_d, input_c, input_n, input_rooms, \
    input_public, input_max_w, input_min_w, \
    input_pref_w, input_max_d, input_min_d, \
    input_pref_d, input_min_r, input_max_r, \
    input_adj, input_voids, front_door,\
        input_min_areas, input_max_areas = reader.read_input(input_file, scale)

    generate_layout(input_w, input_d, input_n, input_c, input_rooms,
                    input_min_w, input_max_w, input_min_d, input_max_d,
                    input_min_r, input_max_r, input_pref_w, input_pref_d,
                    input_adj, input_public, input_voids, front_door, output_file, input_min_areas, input_max_areas, scale, take_first_solution, timelimit, visualize, num)


if __name__ == '__main__':
    first_sol_found = False
    sol_times = []
    start = time.time()
    solve(sys.argv[1], sys.argv[2], 1, 100, 1200, False, False)
    # solve("input/real/input-real5.xml", "output.xml", 1, 100, 600, False, True)
