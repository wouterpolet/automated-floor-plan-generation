import numpy as np
from shapely.geometry.polygon import Polygon
from xml_io import input

def read_input(input_file, scale):
    xml_in = input.parse(input_file)

    floor = xml_in.BuildingGeometry.Floor[0].Point

    min_x = floor[0].x
    min_y = floor[0].y
    max_x = floor[0].x
    max_y = floor[0].y

    for p in floor:
        if p.x < min_x:
            min_x = p.x
        if p.y < min_y:
            min_y = p.y
        if p.x > max_x:
            max_x = p.x
        if p.y > max_y:
            max_y = p.y

    width = (max_x - min_x) * scale
    depth = (max_y - min_y) * scale

    voids = []

    if len(floor) > 4:
        li = []

        for p in floor:
            li.append((p.x * scale, p.y * scale))
        polygon1 = Polygon(li)
        polygon2 = Polygon([(0, 0), (width, 0), (width, depth), (0, depth)])
        nonoverlaps = polygon1.symmetric_difference(polygon2)
        if type(nonoverlaps) == Polygon:
            x, y = nonoverlaps.exterior.xy
            void_x = min(x)
            void_y = min(y)
            void_w = max(x) - min(x)
            void_d = max(y) - min(y)
            voids.append((void_x, void_y, void_w, void_d))
        else:
            for p in nonoverlaps:
                x, y = p.exterior.xy
                void_x = min(x)
                void_y = min(y)
                void_w = max(x) - min(x)
                void_d = max(y) - min(y)
                voids.append((void_x, void_y, void_w, void_d))

    c = 0.8 * scale

    roomTypes = xml_in.RoomTypes.RoomType
    temp = []
    for e in roomTypes:
        if e.Count is not None:
            if e.Count.min > 1:
                temp.append(e)
    roomTypes = roomTypes + temp

    rooms = [r.name for r in roomTypes]

    n = len(rooms)

    public = [int(r.publiclyAccessible) for r in roomTypes]
    min_width = [r.Width.min for r in roomTypes]
    max_width = [r.Width.max for r in roomTypes]

    min_depth = [r.Length.min for r in roomTypes]
    max_depth = [r.Length.max for r in roomTypes]

    min_areas = [float(r.Area.min) * scale * scale for r in roomTypes]
    max_areas = [float(r.Area.max) * scale * scale for r in roomTypes]

    min_ratio = [r.AspectRatio.min for r in roomTypes]
    max_ratio = [r.AspectRatio.max for r in roomTypes]



    for idx, e in enumerate(min_width):
        if e == 'unbounded':
            min_width[idx] = 1
        else:
            min_width[idx] = float(e) * scale

    for idx, e in enumerate(max_width):
        if e == 'unbounded':
            max_width[idx] = width
        else:
            max_width[idx] = float(e) * scale

    for idx, e in enumerate(min_depth):
        if e == 'unbounded':
            min_depth[idx] = 1
        else:
            min_depth[idx] = float(e) * scale

    for idx, e in enumerate(max_depth):
        if e == 'unbounded':
            max_depth[idx] = depth
        else:
            max_depth[idx] = float(e) * scale

    for idx, e in enumerate(min_ratio):
        if e == 'unbounded':
            min_ratio[idx] = 1
        else:
            min_ratio[idx] = float(e)

    for idx, e in enumerate(max_ratio):
        if e == 'unbounded':
            # Magic number :tada:
            max_ratio[idx] = 10
        else:
            max_ratio[idx] = float(e)

    pref_width = [r.Width.target for r in roomTypes]
    pref_depth = [r.Length.target for r in roomTypes]

    # pref_width = [int((min_width[i] + max_width[i]) / 2) for i in range(n)]
    # pref_depth = [int((min_depth[i] + max_depth[i]) / 2) for i in range(n)]

    adjacencies = [[0 for _ in range(n)] for _ in range(n)]

    if xml_in.RoomAdjacencies is not None:
        adj = xml_in.RoomAdjacencies.RoomAdjacency
        for ad in adj:
            for a in [i for i, x in enumerate(rooms) if x == ad.roomA]:
                for b in [i for i, x in enumerate(rooms) if x == ad.roomB]:
                    adjacencies[a][b] = 1
                    adjacencies[b][a] = 1

    front_door = 0

    for idx, e in enumerate(roomTypes):
        if e.main:
            front_door = idx

    return width, depth, c, n, rooms, public, np.array(max_width), np.array(min_width), np.array(pref_width), np.array(max_depth), np.array(min_depth), np.array(pref_depth), np.array(min_ratio), np.array(max_ratio), adjacencies, voids, front_door, np.array(min_areas), np.array(max_areas)

if __name__ == '__main__':
    read_input('input/input-real5.xml', 100)