from xml_io import output

class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __mul__(self, other):
        return Point(self.x * other.x, self.y + other.y)

    def __abs__(self):
        return Point(abs(self.x), abs(self.y))

    def __str__(self):
        return f'({self.x}, {self.y})'

    def __eq__(self, other):
        return other is not None and self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))


def make_polygon(rooms, x, y, w, d):
    polygons = {}
    for idx in range(len(x)):
        li = [Point(x[idx], y[idx]),
              Point(x[idx] + w[idx], y[idx]),
              Point(x[idx] + w[idx], y[idx] + d[idx]),
              Point(x[idx], y[idx] + d[idx])]
        polygons[rooms[idx]] = li
    return polygons


def create_output(rooms, polygons, scale):
    floorplan = output.FloorPlan()
    floor = output.FloorType()
    floorplan.add_Floor(floor)
    xml_rooms = []
    floor.set_Room(xml_rooms)
    for r in rooms:
        room = output.RoomType()
        room.set_type(r)
        room.set_ns_prefix_('fpo')

        point_list = []
        for p in polygons[r]:
            point = output.PointType()
            point.set_ns_prefix_('fpi')
            point.set_x(p.x / scale)
            point.set_y(p.y / scale)
            point_list.append(point)
        geometry = output.PointList()
        geometry.set_ns_prefix_('fpo')
        room.add_Geometry(geometry)
        geometry.set_Point(point_list)
        xml_rooms.append(room)
    return floorplan


def write_output(rooms, x, y, w, d, output_file, scale):
    polygons = make_polygon(rooms, x, y, w, d)
    xml_out = create_output(rooms, polygons, scale)
    with open(output_file, 'w') as out_file:
        out_file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
        xml_out.export(out_file, level=0,
                       namespacedef_='xmlns:fpo="urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4" xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed input.xsd urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4 output.xsd"')
