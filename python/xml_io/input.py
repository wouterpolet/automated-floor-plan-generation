#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# Generated Tue Jun  1 11:36:16 2021 by generateDS.py version 2.38.6.
# Python 3.9.5 (default, May  4 2021, 03:36:27)  [Clang 12.0.0 (clang-1200.0.32.29)]
#
# Command line options:
#   ('-o', 'constraint/python/input.py')
#   ('-s', 'constraint/python/inputsubs.py')
#
# Command line arguments:
#   doc/schemas/input.xsd
#
# Command line:
#   /usr/local/bin/generateDS -o "constraint/python/input.py" -s "constraint/python/inputsubs.py" doc/schemas/input.xsd
#
# Current working directory (os.getcwd()):
#   automated-floor-plan-generation
#

import sys
try:
    ModulenotfoundExp_ = ModuleNotFoundError
except NameError:
    ModulenotfoundExp_ = ImportError
from six.moves import zip_longest
import os
import re as re_
import base64
import datetime as datetime_
import decimal as decimal_
try:
    from lxml import etree as etree_
except ModulenotfoundExp_ :
    from xml.etree import ElementTree as etree_


Validate_simpletypes_ = True
SaveElementTreeNode = True
if sys.version_info.major == 2:
    BaseStrType_ = basestring
else:
    BaseStrType_ = str


def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Namespace prefix definition table (and other attributes, too)
#
# The module generatedsnamespaces, if it is importable, must contain
# a dictionary named GeneratedsNamespaceDefs.  This Python dictionary
# should map element type names (strings) to XML schema namespace prefix
# definitions.  The export method for any class for which there is
# a namespace prefix definition, will export that definition in the
# XML representation of that element.  See the export method of
# any generated element type class for an example of the use of this
# table.
# A sample table is:
#
#     # File: generatedsnamespaces.py
#
#     GenerateDSNamespaceDefs = {
#         "ElementtypeA": "http://www.xxx.com/namespaceA",
#         "ElementtypeB": "http://www.xxx.com/namespaceB",
#     }
#
# Additionally, the generatedsnamespaces module can contain a python
# dictionary named GenerateDSNamespaceTypePrefixes that associates element
# types with the namespace prefixes that are to be added to the
# "xsi:type" attribute value.  See the exportAttributes method of
# any generated element type and the generation of "xsi:type" for an
# example of the use of this table.
# An example table:
#
#     # File: generatedsnamespaces.py
#
#     GenerateDSNamespaceTypePrefixes = {
#         "ElementtypeC": "aaa:",
#         "ElementtypeD": "bbb:",
#     }
#

try:
    from generatedsnamespaces import GenerateDSNamespaceDefs as GenerateDSNamespaceDefs_
except ModulenotfoundExp_ :
    GenerateDSNamespaceDefs_ = {}
try:
    from generatedsnamespaces import GenerateDSNamespaceTypePrefixes as GenerateDSNamespaceTypePrefixes_
except ModulenotfoundExp_ :
    GenerateDSNamespaceTypePrefixes_ = {}

#
# You can replace the following class definition by defining an
# importable module named "generatedscollector" containing a class
# named "GdsCollector".  See the default class definition below for
# clues about the possible content of that class.
#
try:
    from generatedscollector import GdsCollector as GdsCollector_
except ModulenotfoundExp_ :

    class GdsCollector_(object):

        def __init__(self, messages=None):
            if messages is None:
                self.messages = []
            else:
                self.messages = messages

        def add_message(self, msg):
            self.messages.append(msg)

        def get_messages(self):
            return self.messages

        def clear_messages(self):
            self.messages = []

        def print_messages(self):
            for msg in self.messages:
                print("Warning: {}".format(msg))

        def write_messages(self, outstream):
            for msg in self.messages:
                outstream.write("Warning: {}\n".format(msg))


#
# The super-class for enum types
#

try:
    from enum import Enum
except ModulenotfoundExp_ :
    Enum = object

#
# The root super-class for element type classes
#
# Calls to the methods in these classes are generated by generateDS.py.
# You can replace these methods by re-implementing the following class
#   in a module named generatedssuper.py.

try:
    from generatedssuper import GeneratedsSuper
except ModulenotfoundExp_ as exp:
    
    class GeneratedsSuper(object):
        __hash__ = object.__hash__
        tzoff_pattern = re_.compile(r'(\+|-)((0\d|1[0-3]):[0-5]\d|14:00)$')
        class _FixedOffsetTZ(datetime_.tzinfo):
            def __init__(self, offset, name):
                self.__offset = datetime_.timedelta(minutes=offset)
                self.__name = name
            def utcoffset(self, dt):
                return self.__offset
            def tzname(self, dt):
                return self.__name
            def dst(self, dt):
                return None
        def gds_format_string(self, input_data, input_name=''):
            return input_data
        def gds_parse_string(self, input_data, node=None, input_name=''):
            return input_data
        def gds_validate_string(self, input_data, node=None, input_name=''):
            if not input_data:
                return ''
            else:
                return input_data
        def gds_format_base64(self, input_data, input_name=''):
            return base64.b64encode(input_data)
        def gds_validate_base64(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_integer(self, input_data, input_name=''):
            return '%d' % input_data
        def gds_parse_integer(self, input_data, node=None, input_name=''):
            try:
                ival = int(input_data)
            except (TypeError, ValueError) as exp:
                raise_parse_error(node, 'Requires integer value: %s' % exp)
            return ival
        def gds_validate_integer(self, input_data, node=None, input_name=''):
            try:
                value = int(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Requires integer value')
            return value
        def gds_format_integer_list(self, input_data, input_name=''):
            if len(input_data) > 0 and not isinstance(input_data[0], BaseStrType_):
                input_data = [str(s) for s in input_data]
            return '%s' % ' '.join(input_data)
        def gds_validate_integer_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    int(value)
                except (TypeError, ValueError):
                    raise_parse_error(node, 'Requires sequence of integer values')
            return values
        def gds_format_float(self, input_data, input_name=''):
            return ('%.15f' % input_data).rstrip('0')
        def gds_parse_float(self, input_data, node=None, input_name=''):
            try:
                fval_ = float(input_data)
            except (TypeError, ValueError) as exp:
                raise_parse_error(node, 'Requires float or double value: %s' % exp)
            return fval_
        def gds_validate_float(self, input_data, node=None, input_name=''):
            try:
                value = float(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Requires float value')
            return value
        def gds_format_float_list(self, input_data, input_name=''):
            if len(input_data) > 0 and not isinstance(input_data[0], BaseStrType_):
                input_data = [str(s) for s in input_data]
            return '%s' % ' '.join(input_data)
        def gds_validate_float_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    float(value)
                except (TypeError, ValueError):
                    raise_parse_error(node, 'Requires sequence of float values')
            return values
        def gds_format_decimal(self, input_data, input_name=''):
            return_value = '%s' % input_data
            if '.' in return_value:
                return_value = return_value.rstrip('0')
                if return_value.endswith('.'):
                    return_value = return_value.rstrip('.')
            return return_value
        def gds_parse_decimal(self, input_data, node=None, input_name=''):
            try:
                decimal_value = decimal_.Decimal(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Requires decimal value')
            return decimal_value
        def gds_validate_decimal(self, input_data, node=None, input_name=''):
            try:
                value = decimal_.Decimal(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Requires decimal value')
            return value
        def gds_format_decimal_list(self, input_data, input_name=''):
            if len(input_data) > 0 and not isinstance(input_data[0], BaseStrType_):
                input_data = [str(s) for s in input_data]
            return ' '.join([self.gds_format_decimal(item) for item in input_data])
        def gds_validate_decimal_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    decimal_.Decimal(value)
                except (TypeError, ValueError):
                    raise_parse_error(node, 'Requires sequence of decimal values')
            return values
        def gds_format_double(self, input_data, input_name=''):
            return '%s' % input_data
        def gds_parse_double(self, input_data, node=None, input_name=''):
            try:
                fval_ = float(input_data)
            except (TypeError, ValueError) as exp:
                raise_parse_error(node, 'Requires double or float value: %s' % exp)
            return fval_
        def gds_validate_double(self, input_data, node=None, input_name=''):
            try:
                value = float(input_data)
            except (TypeError, ValueError):
                raise_parse_error(node, 'Requires double or float value')
            return value
        def gds_format_double_list(self, input_data, input_name=''):
            if len(input_data) > 0 and not isinstance(input_data[0], BaseStrType_):
                input_data = [str(s) for s in input_data]
            return '%s' % ' '.join(input_data)
        def gds_validate_double_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                try:
                    float(value)
                except (TypeError, ValueError):
                    raise_parse_error(
                        node, 'Requires sequence of double or float values')
            return values
        def gds_format_boolean(self, input_data, input_name=''):
            return ('%s' % input_data).lower()
        def gds_parse_boolean(self, input_data, node=None, input_name=''):
            if input_data in ('true', '1'):
                bval = True
            elif input_data in ('false', '0'):
                bval = False
            else:
                raise_parse_error(node, 'Requires boolean value')
            return bval
        def gds_validate_boolean(self, input_data, node=None, input_name=''):
            if input_data not in (True, 1, False, 0, ):
                raise_parse_error(
                    node,
                    'Requires boolean value '
                    '(one of True, 1, False, 0)')
            return input_data
        def gds_format_boolean_list(self, input_data, input_name=''):
            if len(input_data) > 0 and not isinstance(input_data[0], BaseStrType_):
                input_data = [str(s) for s in input_data]
            return '%s' % ' '.join(input_data)
        def gds_validate_boolean_list(
                self, input_data, node=None, input_name=''):
            values = input_data.split()
            for value in values:
                value = self.gds_parse_boolean(value, node, input_name)
                if value not in (True, 1, False, 0, ):
                    raise_parse_error(
                        node,
                        'Requires sequence of boolean values '
                        '(one of True, 1, False, 0)')
            return values
        def gds_validate_datetime(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_datetime(self, input_data, input_name=''):
            if input_data.microsecond == 0:
                _svalue = '%04d-%02d-%02dT%02d:%02d:%02d' % (
                    input_data.year,
                    input_data.month,
                    input_data.day,
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                )
            else:
                _svalue = '%04d-%02d-%02dT%02d:%02d:%02d.%s' % (
                    input_data.year,
                    input_data.month,
                    input_data.day,
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                    ('%f' % (float(input_data.microsecond) / 1000000))[2:],
                )
            if input_data.tzinfo is not None:
                tzoff = input_data.tzinfo.utcoffset(input_data)
                if tzoff is not None:
                    total_seconds = tzoff.seconds + (86400 * tzoff.days)
                    if total_seconds == 0:
                        _svalue += 'Z'
                    else:
                        if total_seconds < 0:
                            _svalue += '-'
                            total_seconds *= -1
                        else:
                            _svalue += '+'
                        hours = total_seconds // 3600
                        minutes = (total_seconds - (hours * 3600)) // 60
                        _svalue += '{0:02d}:{1:02d}'.format(hours, minutes)
            return _svalue
        @classmethod
        def gds_parse_datetime(cls, input_data):
            tz = None
            if input_data[-1] == 'Z':
                tz = GeneratedsSuper._FixedOffsetTZ(0, 'UTC')
                input_data = input_data[:-1]
            else:
                results = GeneratedsSuper.tzoff_pattern.search(input_data)
                if results is not None:
                    tzoff_parts = results.group(2).split(':')
                    tzoff = int(tzoff_parts[0]) * 60 + int(tzoff_parts[1])
                    if results.group(1) == '-':
                        tzoff *= -1
                    tz = GeneratedsSuper._FixedOffsetTZ(
                        tzoff, results.group(0))
                    input_data = input_data[:-6]
            time_parts = input_data.split('.')
            if len(time_parts) > 1:
                micro_seconds = int(float('0.' + time_parts[1]) * 1000000)
                input_data = '%s.%s' % (
                    time_parts[0], "{}".format(micro_seconds).rjust(6, "0"), )
                dt = datetime_.datetime.strptime(
                    input_data, '%Y-%m-%dT%H:%M:%S.%f')
            else:
                dt = datetime_.datetime.strptime(
                    input_data, '%Y-%m-%dT%H:%M:%S')
            dt = dt.replace(tzinfo=tz)
            return dt
        def gds_validate_date(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_date(self, input_data, input_name=''):
            _svalue = '%04d-%02d-%02d' % (
                input_data.year,
                input_data.month,
                input_data.day,
            )
            try:
                if input_data.tzinfo is not None:
                    tzoff = input_data.tzinfo.utcoffset(input_data)
                    if tzoff is not None:
                        total_seconds = tzoff.seconds + (86400 * tzoff.days)
                        if total_seconds == 0:
                            _svalue += 'Z'
                        else:
                            if total_seconds < 0:
                                _svalue += '-'
                                total_seconds *= -1
                            else:
                                _svalue += '+'
                            hours = total_seconds // 3600
                            minutes = (total_seconds - (hours * 3600)) // 60
                            _svalue += '{0:02d}:{1:02d}'.format(
                                hours, minutes)
            except AttributeError:
                pass
            return _svalue
        @classmethod
        def gds_parse_date(cls, input_data):
            tz = None
            if input_data[-1] == 'Z':
                tz = GeneratedsSuper._FixedOffsetTZ(0, 'UTC')
                input_data = input_data[:-1]
            else:
                results = GeneratedsSuper.tzoff_pattern.search(input_data)
                if results is not None:
                    tzoff_parts = results.group(2).split(':')
                    tzoff = int(tzoff_parts[0]) * 60 + int(tzoff_parts[1])
                    if results.group(1) == '-':
                        tzoff *= -1
                    tz = GeneratedsSuper._FixedOffsetTZ(
                        tzoff, results.group(0))
                    input_data = input_data[:-6]
            dt = datetime_.datetime.strptime(input_data, '%Y-%m-%d')
            dt = dt.replace(tzinfo=tz)
            return dt.date()
        def gds_validate_time(self, input_data, node=None, input_name=''):
            return input_data
        def gds_format_time(self, input_data, input_name=''):
            if input_data.microsecond == 0:
                _svalue = '%02d:%02d:%02d' % (
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                )
            else:
                _svalue = '%02d:%02d:%02d.%s' % (
                    input_data.hour,
                    input_data.minute,
                    input_data.second,
                    ('%f' % (float(input_data.microsecond) / 1000000))[2:],
                )
            if input_data.tzinfo is not None:
                tzoff = input_data.tzinfo.utcoffset(input_data)
                if tzoff is not None:
                    total_seconds = tzoff.seconds + (86400 * tzoff.days)
                    if total_seconds == 0:
                        _svalue += 'Z'
                    else:
                        if total_seconds < 0:
                            _svalue += '-'
                            total_seconds *= -1
                        else:
                            _svalue += '+'
                        hours = total_seconds // 3600
                        minutes = (total_seconds - (hours * 3600)) // 60
                        _svalue += '{0:02d}:{1:02d}'.format(hours, minutes)
            return _svalue
        def gds_validate_simple_patterns(self, patterns, target):
            # pat is a list of lists of strings/patterns.
            # The target value must match at least one of the patterns
            # in order for the test to succeed.
            found1 = True
            for patterns1 in patterns:
                found2 = False
                for patterns2 in patterns1:
                    mo = re_.search(patterns2, target)
                    if mo is not None and len(mo.group(0)) == len(target):
                        found2 = True
                        break
                if not found2:
                    found1 = False
                    break
            return found1
        @classmethod
        def gds_parse_time(cls, input_data):
            tz = None
            if input_data[-1] == 'Z':
                tz = GeneratedsSuper._FixedOffsetTZ(0, 'UTC')
                input_data = input_data[:-1]
            else:
                results = GeneratedsSuper.tzoff_pattern.search(input_data)
                if results is not None:
                    tzoff_parts = results.group(2).split(':')
                    tzoff = int(tzoff_parts[0]) * 60 + int(tzoff_parts[1])
                    if results.group(1) == '-':
                        tzoff *= -1
                    tz = GeneratedsSuper._FixedOffsetTZ(
                        tzoff, results.group(0))
                    input_data = input_data[:-6]
            if len(input_data.split('.')) > 1:
                dt = datetime_.datetime.strptime(input_data, '%H:%M:%S.%f')
            else:
                dt = datetime_.datetime.strptime(input_data, '%H:%M:%S')
            dt = dt.replace(tzinfo=tz)
            return dt.time()
        def gds_check_cardinality_(
                self, value, input_name,
                min_occurs=0, max_occurs=1, required=None):
            if value is None:
                length = 0
            elif isinstance(value, list):
                length = len(value)
            else:
                length = 1
            if required is not None :
                if required and length < 1:
                    self.gds_collector_.add_message(
                        "Required value {}{} is missing".format(
                            input_name, self.gds_get_node_lineno_()))
            if length < min_occurs:
                self.gds_collector_.add_message(
                    "Number of values for {}{} is below "
                    "the minimum allowed, "
                    "expected at least {}, found {}".format(
                        input_name, self.gds_get_node_lineno_(),
                        min_occurs, length))
            elif length > max_occurs:
                self.gds_collector_.add_message(
                    "Number of values for {}{} is above "
                    "the maximum allowed, "
                    "expected at most {}, found {}".format(
                        input_name, self.gds_get_node_lineno_(),
                        max_occurs, length))
        def gds_validate_builtin_ST_(
                self, validator, value, input_name,
                min_occurs=None, max_occurs=None, required=None):
            if value is not None:
                try:
                    validator(value, input_name=input_name)
                except GDSParseError as parse_error:
                    self.gds_collector_.add_message(str(parse_error))
        def gds_validate_defined_ST_(
                self, validator, value, input_name,
                min_occurs=None, max_occurs=None, required=None):
            if value is not None:
                try:
                    validator(value)
                except GDSParseError as parse_error:
                    self.gds_collector_.add_message(str(parse_error))
        def gds_str_lower(self, instring):
            return instring.lower()
        def get_path_(self, node):
            path_list = []
            self.get_path_list_(node, path_list)
            path_list.reverse()
            path = '/'.join(path_list)
            return path
        Tag_strip_pattern_ = re_.compile(r'\{.*\}')
        def get_path_list_(self, node, path_list):
            if node is None:
                return
            tag = GeneratedsSuper.Tag_strip_pattern_.sub('', node.tag)
            if tag:
                path_list.append(tag)
            self.get_path_list_(node.getparent(), path_list)
        def get_class_obj_(self, node, default_class=None):
            class_obj1 = default_class
            if 'xsi' in node.nsmap:
                classname = node.get('{%s}type' % node.nsmap['xsi'])
                if classname is not None:
                    names = classname.split(':')
                    if len(names) == 2:
                        classname = names[1]
                    class_obj2 = globals().get(classname)
                    if class_obj2 is not None:
                        class_obj1 = class_obj2
            return class_obj1
        def gds_build_any(self, node, type_name=None):
            # provide default value in case option --disable-xml is used.
            content = ""
            content = etree_.tostring(node, encoding="unicode")
            return content
        @classmethod
        def gds_reverse_node_mapping(cls, mapping):
            return dict(((v, k) for k, v in mapping.items()))
        @staticmethod
        def gds_encode(instring):
            if sys.version_info.major == 2:
                if ExternalEncoding:
                    encoding = ExternalEncoding
                else:
                    encoding = 'utf-8'
                return instring.encode(encoding)
            else:
                return instring
        @staticmethod
        def convert_unicode(instring):
            if isinstance(instring, str):
                result = quote_xml(instring)
            elif sys.version_info.major == 2 and isinstance(instring, unicode):
                result = quote_xml(instring).encode('utf8')
            else:
                result = GeneratedsSuper.gds_encode(str(instring))
            return result
        def __eq__(self, other):
            def excl_select_objs_(obj):
                return (obj[0] != 'parent_object_' and
                        obj[0] != 'gds_collector_')
            if type(self) != type(other):
                return False
            return all(x == y for x, y in zip_longest(
                filter(excl_select_objs_, self.__dict__.items()),
                filter(excl_select_objs_, other.__dict__.items())))
        def __ne__(self, other):
            return not self.__eq__(other)
        # Django ETL transform hooks.
        def gds_djo_etl_transform(self):
            pass
        def gds_djo_etl_transform_db_obj(self, dbobj):
            pass
        # SQLAlchemy ETL transform hooks.
        def gds_sqa_etl_transform(self):
            return 0, None
        def gds_sqa_etl_transform_db_obj(self, dbobj):
            pass
        def gds_get_node_lineno_(self):
            if (hasattr(self, "gds_elementtree_node_") and
                    self.gds_elementtree_node_ is not None):
                return ' near line {}'.format(
                    self.gds_elementtree_node_.sourceline)
            else:
                return ""
    
    
    def getSubclassFromModule_(module, class_):
        '''Get the subclass of a class from a specific module.'''
        name = class_.__name__ + 'Sub'
        if hasattr(module, name):
            return getattr(module, name)
        else:
            return None


#
# If you have installed IPython you can uncomment and use the following.
# IPython is available from http://ipython.scipy.org/.
#

## from IPython.Shell import IPShellEmbed
## args = ''
## ipshell = IPShellEmbed(args,
##     banner = 'Dropping into IPython',
##     exit_msg = 'Leaving Interpreter, back to program.')

# Then use the following line where and when you want to drop into the
# IPython shell:
#    ipshell('<some message> -- Entering ipshell.\nHit Ctrl-D to exit')

#
# Globals
#

ExternalEncoding = ''
# Set this to false in order to deactivate during export, the use of
# name space prefixes captured from the input document.
UseCapturedNS_ = True
CapturedNsmap_ = {}
Tag_pattern_ = re_.compile(r'({.*})?(.*)')
String_cleanup_pat_ = re_.compile(r"[\n\r\s]+")
Namespace_extract_pat_ = re_.compile(r'{(.*)}(.*)')
CDATA_pattern_ = re_.compile(r"<!\[CDATA\[.*?\]\]>", re_.DOTALL)

# Change this to redirect the generated superclass module to use a
# specific subclass module.
CurrentSubclassModule_ = None

#
# Support/utility functions.
#


def showIndent(outfile, level, pretty_print=True):
    if pretty_print:
        for idx in range(level):
            outfile.write('    ')


def quote_xml(inStr):
    "Escape markup chars, but do not modify CDATA sections."
    if not inStr:
        return ''
    s1 = (isinstance(inStr, BaseStrType_) and inStr or '%s' % inStr)
    s2 = ''
    pos = 0
    matchobjects = CDATA_pattern_.finditer(s1)
    for mo in matchobjects:
        s3 = s1[pos:mo.start()]
        s2 += quote_xml_aux(s3)
        s2 += s1[mo.start():mo.end()]
        pos = mo.end()
    s3 = s1[pos:]
    s2 += quote_xml_aux(s3)
    return s2


def quote_xml_aux(inStr):
    s1 = inStr.replace('&', '&amp;')
    s1 = s1.replace('<', '&lt;')
    s1 = s1.replace('>', '&gt;')
    return s1


def quote_attrib(inStr):
    s1 = (isinstance(inStr, BaseStrType_) and inStr or '%s' % inStr)
    s1 = s1.replace('&', '&amp;')
    s1 = s1.replace('<', '&lt;')
    s1 = s1.replace('>', '&gt;')
    if '"' in s1:
        if "'" in s1:
            s1 = '"%s"' % s1.replace('"', "&quot;")
        else:
            s1 = "'%s'" % s1
    else:
        s1 = '"%s"' % s1
    return s1


def quote_python(inStr):
    s1 = inStr
    if s1.find("'") == -1:
        if s1.find('\n') == -1:
            return "'%s'" % s1
        else:
            return "'''%s'''" % s1
    else:
        if s1.find('"') != -1:
            s1 = s1.replace('"', '\\"')
        if s1.find('\n') == -1:
            return '"%s"' % s1
        else:
            return '"""%s"""' % s1


def get_all_text_(node):
    if node.text is not None:
        text = node.text
    else:
        text = ''
    for child in node:
        if child.tail is not None:
            text += child.tail
    return text


def find_attr_value_(attr_name, node):
    attrs = node.attrib
    attr_parts = attr_name.split(':')
    value = None
    if len(attr_parts) == 1:
        value = attrs.get(attr_name)
    elif len(attr_parts) == 2:
        prefix, name = attr_parts
        if prefix == 'xml':
            namespace = 'http://www.w3.org/XML/1998/namespace'
        else:
            namespace = node.nsmap.get(prefix)
        if namespace is not None:
            value = attrs.get('{%s}%s' % (namespace, name, ))
    return value


def encode_str_2_3(instr):
    return instr


class GDSParseError(Exception):
    pass


def raise_parse_error(node, msg):
    if node is not None:
        msg = '%s (element %s/line %d)' % (msg, node.tag, node.sourceline, )
    raise GDSParseError(msg)


class MixedContainer:
    # Constants for category:
    CategoryNone = 0
    CategoryText = 1
    CategorySimple = 2
    CategoryComplex = 3
    # Constants for content_type:
    TypeNone = 0
    TypeText = 1
    TypeString = 2
    TypeInteger = 3
    TypeFloat = 4
    TypeDecimal = 5
    TypeDouble = 6
    TypeBoolean = 7
    TypeBase64 = 8
    def __init__(self, category, content_type, name, value):
        self.category = category
        self.content_type = content_type
        self.name = name
        self.value = value
    def getCategory(self):
        return self.category
    def getContenttype(self, content_type):
        return self.content_type
    def getValue(self):
        return self.value
    def getName(self):
        return self.name
    def export(self, outfile, level, name, namespace,
               pretty_print=True):
        if self.category == MixedContainer.CategoryText:
            # Prevent exporting empty content as empty lines.
            if self.value.strip():
                outfile.write(self.value)
        elif self.category == MixedContainer.CategorySimple:
            self.exportSimple(outfile, level, name)
        else:    # category == MixedContainer.CategoryComplex
            self.value.export(
                outfile, level, namespace, name_=name,
                pretty_print=pretty_print)
    def exportSimple(self, outfile, level, name):
        if self.content_type == MixedContainer.TypeString:
            outfile.write('<%s>%s</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeInteger or \
                self.content_type == MixedContainer.TypeBoolean:
            outfile.write('<%s>%d</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeFloat or \
                self.content_type == MixedContainer.TypeDecimal:
            outfile.write('<%s>%f</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeDouble:
            outfile.write('<%s>%g</%s>' % (
                self.name, self.value, self.name))
        elif self.content_type == MixedContainer.TypeBase64:
            outfile.write('<%s>%s</%s>' % (
                self.name,
                base64.b64encode(self.value),
                self.name))
    def to_etree(self, element, mapping_=None, nsmap_=None):
        if self.category == MixedContainer.CategoryText:
            # Prevent exporting empty content as empty lines.
            if self.value.strip():
                if len(element) > 0:
                    if element[-1].tail is None:
                        element[-1].tail = self.value
                    else:
                        element[-1].tail += self.value
                else:
                    if element.text is None:
                        element.text = self.value
                    else:
                        element.text += self.value
        elif self.category == MixedContainer.CategorySimple:
            subelement = etree_.SubElement(
                element, '%s' % self.name)
            subelement.text = self.to_etree_simple()
        else:    # category == MixedContainer.CategoryComplex
            self.value.to_etree(element)
    def to_etree_simple(self, mapping_=None, nsmap_=None):
        if self.content_type == MixedContainer.TypeString:
            text = self.value
        elif (self.content_type == MixedContainer.TypeInteger or
                self.content_type == MixedContainer.TypeBoolean):
            text = '%d' % self.value
        elif (self.content_type == MixedContainer.TypeFloat or
                self.content_type == MixedContainer.TypeDecimal):
            text = '%f' % self.value
        elif self.content_type == MixedContainer.TypeDouble:
            text = '%g' % self.value
        elif self.content_type == MixedContainer.TypeBase64:
            text = '%s' % base64.b64encode(self.value)
        return text
    def exportLiteral(self, outfile, level, name):
        if self.category == MixedContainer.CategoryText:
            showIndent(outfile, level)
            outfile.write(
                'model_.MixedContainer(%d, %d, "%s", "%s"),\n' % (
                    self.category, self.content_type,
                    self.name, self.value))
        elif self.category == MixedContainer.CategorySimple:
            showIndent(outfile, level)
            outfile.write(
                'model_.MixedContainer(%d, %d, "%s", "%s"),\n' % (
                    self.category, self.content_type,
                    self.name, self.value))
        else:    # category == MixedContainer.CategoryComplex
            showIndent(outfile, level)
            outfile.write(
                'model_.MixedContainer(%d, %d, "%s",\n' % (
                    self.category, self.content_type, self.name,))
            self.value.exportLiteral(outfile, level + 1)
            showIndent(outfile, level)
            outfile.write(')\n')


class MemberSpec_(object):
    def __init__(self, name='', data_type='', container=0,
            optional=0, child_attrs=None, choice=None):
        self.name = name
        self.data_type = data_type
        self.container = container
        self.child_attrs = child_attrs
        self.choice = choice
        self.optional = optional
    def set_name(self, name): self.name = name
    def get_name(self): return self.name
    def set_data_type(self, data_type): self.data_type = data_type
    def get_data_type_chain(self): return self.data_type
    def get_data_type(self):
        if isinstance(self.data_type, list):
            if len(self.data_type) > 0:
                return self.data_type[-1]
            else:
                return 'xs:string'
        else:
            return self.data_type
    def set_container(self, container): self.container = container
    def get_container(self): return self.container
    def set_child_attrs(self, child_attrs): self.child_attrs = child_attrs
    def get_child_attrs(self): return self.child_attrs
    def set_choice(self, choice): self.choice = choice
    def get_choice(self): return self.choice
    def set_optional(self, optional): self.optional = optional
    def get_optional(self): return self.optional


def _cast(typ, value):
    if typ is None or value is None:
        return value
    return typ(value)

#
# Data representation classes.
#


class AllInteger(str, Enum):
    """<p>
    An integer or the value "unbounded".
    </p>"""
    UNBOUNDED='unbounded'


class AllNNI(str, Enum):
    """<p>
    A non-negative integer or the value "unbounded".
    </p>"""
    UNBOUNDED='unbounded'


class AllPositiveDouble(str, Enum):
    """<p>
    An IEEE 754 double-precision (64-bit) float with a minimum value of 0.
    Additionally, the value "unbounded"
    indicates that there is no value.
    </p>"""
    UNBOUNDED='unbounded'


class CardinalDirectionOrAny(str, Enum):
    """<p>
    A cardinal direction (north, east, south, west) or "any" direction.
    </p><p>
    Cardinal directions indicate the faces of a building. Going from north to
    south means decreasing Y, while going
    from east to west means decreasing X. A face is always on the outside, i.e.
    a north face indicates an edge
    where slightly south of it is a building and slightly north is outside.
    </p><p>
    "any" means that there is no preference and that the algorithm is free to
    choose a face.
    </p>"""
    NORTH='north'
    EAST='east'
    SOUTH='south'
    WEST='west'
    ANY='any'


class ObjectiveName(str, Enum):
    UNUSED_AREA='unusedArea'
    SIZE='size'
    ADJACENCY='adjacency'
    CORNERS='corners'
    ROOM_AREA='roomArea'
    CONNECTIVITY='connectivity'
    BOUNDING_BOX_AREA='boundingBoxArea'
    BOUNDING_BOX_DEPTH='boundingBoxDepth'
    WINDOW_AREA='windowArea'


class Range(GeneratedsSuper):
    """<p>
    A range of doubles.
    </p><p>
    The minimum and maximum are inclusive.
    </p><p>
    Both the minimum and the maximum are optional values. The default minimum
    is zero, while the default maximum
    is "unbounded". The target, also optional, must be at least the minimum
    value and, if the range is
    not unbounded, at most the maximum value. The default target is zero.
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, min='0', max='unbounded', target='0', extensiontype_=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = "fpi"
        self.min = _cast(float, min)
        self.min_nsprefix_ = None
        self.max = _cast(None, max)
        self.max_nsprefix_ = None
        self.target = _cast(float, target)
        self.target_nsprefix_ = None
        self.extensiontype_ = extensiontype_
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, Range)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if Range.subclass:
            return Range.subclass(*args_, **kwargs_)
        else:
            return Range(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_min(self):
        return self.min
    def set_min(self, min):
        self.min = min
    def get_max(self):
        return self.max
    def set_max(self, max):
        self.max = max
    def get_target(self):
        return self.target
    def set_target(self, target):
        self.target = target
    def get_extensiontype_(self): return self.extensiontype_
    def set_extensiontype_(self, extensiontype_): self.extensiontype_ = extensiontype_
    def validate_PositiveDouble(self, value):
        # Validate type fpi:PositiveDouble, a restriction on xs:double.
        if value is not None and Validate_simpletypes_ and self.gds_collector_ is not None:
            if not isinstance(value, float):
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s is not of the correct base simple type (float)' % {"value": value, "lineno": lineno, })
                return False
            if value < 0:
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s does not match xsd minInclusive restriction on PositiveDouble' % {"value": value, "lineno": lineno} )
                result = False
    def validate_AllPositiveDouble(self, value):
        # Validate type fpi:AllPositiveDouble, a restriction on xs:string.
        pass
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='Range', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('Range')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'Range':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='Range')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='Range', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='Range'):
        if self.min != 0 and 'min' not in already_processed:
            already_processed.add('min')
            outfile.write(' min="%s"' % self.gds_format_double(self.min, input_name='min'))
        if self.max != "unbounded" and 'max' not in already_processed:
            already_processed.add('max')
            outfile.write(' max=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.max), input_name='max')), ))
        if self.target != 0 and 'target' not in already_processed:
            already_processed.add('target')
            outfile.write(' target="%s"' % self.gds_format_double(self.target, input_name='target'))
        if self.extensiontype_ is not None and 'xsi:type' not in already_processed:
            already_processed.add('xsi:type')
            outfile.write(' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"')
            if ":" not in self.extensiontype_:
                imported_ns_type_prefix_ = GenerateDSNamespaceTypePrefixes_.get(self.extensiontype_, '')
                outfile.write(' xsi:type="%s%s"' % (imported_ns_type_prefix_, self.extensiontype_))
            else:
                outfile.write(' xsi:type="%s"' % self.extensiontype_)
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='Range', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('min', node)
        if value is not None and 'min' not in already_processed:
            already_processed.add('min')
            value = self.gds_parse_double(value, node, 'min')
            self.min = value
            self.validate_PositiveDouble(self.min)    # validate type PositiveDouble
        value = find_attr_value_('max', node)
        if value is not None and 'max' not in already_processed:
            already_processed.add('max')
            self.max = value
            self.validate_AllPositiveDouble(self.max)    # validate type AllPositiveDouble
        value = find_attr_value_('target', node)
        if value is not None and 'target' not in already_processed:
            already_processed.add('target')
            value = self.gds_parse_double(value, node, 'target')
            self.target = value
            self.validate_PositiveDouble(self.target)    # validate type PositiveDouble
        value = find_attr_value_('xsi:type', node)
        if value is not None and 'xsi:type' not in already_processed:
            already_processed.add('xsi:type')
            self.extensiontype_ = value
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class Range


class CountRange(GeneratedsSuper):
    """<p>
    A range for a count.
    </p><p>
    The minimum and maximum are inclusive.
    </p><p>
    Both the minimum and maximum are optional values; the defaults for both are
    1. The maximum can also be
    "unbounded", meaning that there is no maximum. Neither value can be
    negative. The target, also optional,
    must be at leat the minimum value and, if the range is not unbounded, at
    most the maximum value. The
    default target is 1.
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, min=1, max='1', target=1, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = "fpi"
        self.min = _cast(int, min)
        self.min_nsprefix_ = None
        self.max = _cast(None, max)
        self.max_nsprefix_ = None
        self.target = _cast(int, target)
        self.target_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, CountRange)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if CountRange.subclass:
            return CountRange.subclass(*args_, **kwargs_)
        else:
            return CountRange(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_min(self):
        return self.min
    def set_min(self, min):
        self.min = min
    def get_max(self):
        return self.max
    def set_max(self, max):
        self.max = max
    def get_target(self):
        return self.target
    def set_target(self, target):
        self.target = target
    def validate_AllNNI(self, value):
        # Validate type fpi:AllNNI, a restriction on xs:string.
        pass
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='CountRange', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('CountRange')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'CountRange':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='CountRange')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='CountRange', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='CountRange'):
        if self.min != 1 and 'min' not in already_processed:
            already_processed.add('min')
            outfile.write(' min="%s"' % self.gds_format_integer(self.min, input_name='min'))
        if self.max != "1" and 'max' not in already_processed:
            already_processed.add('max')
            outfile.write(' max=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.max), input_name='max')), ))
        if self.target != 1 and 'target' not in already_processed:
            already_processed.add('target')
            outfile.write(' target="%s"' % self.gds_format_integer(self.target, input_name='target'))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='CountRange', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('min', node)
        if value is not None and 'min' not in already_processed:
            already_processed.add('min')
            self.min = self.gds_parse_integer(value, node, 'min')
            if self.min < 0:
                raise_parse_error(node, 'Invalid NonNegativeInteger')
        value = find_attr_value_('max', node)
        if value is not None and 'max' not in already_processed:
            already_processed.add('max')
            self.max = value
            self.validate_AllNNI(self.max)    # validate type AllNNI
        value = find_attr_value_('target', node)
        if value is not None and 'target' not in already_processed:
            already_processed.add('target')
            self.target = self.gds_parse_integer(value, node, 'target')
            if self.target < 0:
                raise_parse_error(node, 'Invalid NonNegativeInteger')
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class CountRange


class IntRange(GeneratedsSuper):
    """<p>
    A range for a count.
    </p><p>
    The minimum and maximum are inclusive.
    </p><p>
    Both the minimum and maximum are optional values; the defaults for both are
    "unbounded".
    The value "unbounded" means that there is no minimum or maximum. The
    target, also optional,
    has a default value of 1.
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, min='unbounded', max='unbounded', target='1', gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = "fpi"
        self.min = _cast(None, min)
        self.min_nsprefix_ = None
        self.max = _cast(None, max)
        self.max_nsprefix_ = None
        self.target = _cast(None, target)
        self.target_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, IntRange)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if IntRange.subclass:
            return IntRange.subclass(*args_, **kwargs_)
        else:
            return IntRange(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_min(self):
        return self.min
    def set_min(self, min):
        self.min = min
    def get_max(self):
        return self.max
    def set_max(self, max):
        self.max = max
    def get_target(self):
        return self.target
    def set_target(self, target):
        self.target = target
    def validate_AllInteger(self, value):
        # Validate type fpi:AllInteger, a restriction on xs:string.
        pass
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='IntRange', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('IntRange')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'IntRange':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='IntRange')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='IntRange', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='IntRange'):
        if self.min != "unbounded" and 'min' not in already_processed:
            already_processed.add('min')
            outfile.write(' min=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.min), input_name='min')), ))
        if self.max != "unbounded" and 'max' not in already_processed:
            already_processed.add('max')
            outfile.write(' max=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.max), input_name='max')), ))
        if self.target != "1" and 'target' not in already_processed:
            already_processed.add('target')
            outfile.write(' target=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.target), input_name='target')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='IntRange', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('min', node)
        if value is not None and 'min' not in already_processed:
            already_processed.add('min')
            self.min = value
            self.validate_AllInteger(self.min)    # validate type AllInteger
        value = find_attr_value_('max', node)
        if value is not None and 'max' not in already_processed:
            already_processed.add('max')
            self.max = value
            self.validate_AllInteger(self.max)    # validate type AllInteger
        value = find_attr_value_('target', node)
        if value is not None and 'target' not in already_processed:
            already_processed.add('target')
            self.target = value
            self.validate_AllInteger(self.target)    # validate type AllInteger
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class IntRange


class RoomAreaRatioRange(Range):
    """[
    <p>
    A range of room area ratios between two room types.
    </p><p>
    See the Range type for range semantics.
    </p><p>
    The constraint specifies that the area of room A over the area of room B is
    within the given range.
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = Range
    def __init__(self, min='0', max='unbounded', target='0', roomA=None, roomB=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = "fpi"
        super(globals().get("RoomAreaRatioRange"), self).__init__(min, max, target,  **kwargs_)
        self.roomA = _cast(None, roomA)
        self.roomA_nsprefix_ = None
        self.roomB = _cast(None, roomB)
        self.roomB_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, RoomAreaRatioRange)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if RoomAreaRatioRange.subclass:
            return RoomAreaRatioRange.subclass(*args_, **kwargs_)
        else:
            return RoomAreaRatioRange(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_roomA(self):
        return self.roomA
    def set_roomA(self, roomA):
        self.roomA = roomA
    def get_roomB(self):
        return self.roomB
    def set_roomB(self, roomB):
        self.roomB = roomB
    def validate_RoomName(self, value):
        # Validate type fpi:RoomName, a restriction on xs:string.
        if value is not None and Validate_simpletypes_ and self.gds_collector_ is not None:
            if not isinstance(value, str):
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s is not of the correct base simple type (str)' % {"value": value, "lineno": lineno, })
                return False
            if len(value) < 1:
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s does not match xsd minLength restriction on RoomName' % {"value" : encode_str_2_3(value), "lineno": lineno} )
                result = False
    def hasContent_(self):
        if (
            super(RoomAreaRatioRange, self).hasContent_()
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAreaRatioRange', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('RoomAreaRatioRange')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'RoomAreaRatioRange':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='RoomAreaRatioRange')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='RoomAreaRatioRange', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='RoomAreaRatioRange'):
        super(RoomAreaRatioRange, self).exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='RoomAreaRatioRange')
        if self.roomA is not None and 'roomA' not in already_processed:
            already_processed.add('roomA')
            outfile.write(' roomA=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.roomA), input_name='roomA')), ))
        if self.roomB is not None and 'roomB' not in already_processed:
            already_processed.add('roomB')
            outfile.write(' roomB=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.roomB), input_name='roomB')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAreaRatioRange', fromsubclass_=False, pretty_print=True):
        super(RoomAreaRatioRange, self).exportChildren(outfile, level, namespaceprefix_, namespacedef_, name_, True, pretty_print=pretty_print)
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('roomA', node)
        if value is not None and 'roomA' not in already_processed:
            already_processed.add('roomA')
            self.roomA = value
            self.validate_RoomName(self.roomA)    # validate type RoomName
        value = find_attr_value_('roomB', node)
        if value is not None and 'roomB' not in already_processed:
            already_processed.add('roomB')
            self.roomB = value
            self.validate_RoomName(self.roomB)    # validate type RoomName
        super(RoomAreaRatioRange, self).buildAttributes(node, attrs, already_processed)
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        super(RoomAreaRatioRange, self).buildChildren(child_, node, nodeName_, True)
        pass
# end class RoomAreaRatioRange


class PointList(GeneratedsSuper):
    """<p>
    A list of points that specify the outline of the building.
    </p><p>
    The list starts at the first point as the origin. From there, each
    point draws a straight line from the previous point to there. The final
    line will always
    be drawn from the last point to the origin to close the building.
    </p><p>
    Algorithms are free to reject polygons that are not rectilinear or even
    rectangular.
    </p><p>
    For example, to draw a 7x9 rectangle the following point list is
    sufficient:
    </p>
    <pre>
    &lt;Point x="0" y="0" /&gt;
    &lt;Point x="7" y="0" /&gt;
    &lt;Point x="7" y="9" /&gt;
    &lt;Point x="0" y="9" /&gt;
    </pre>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, Point=None, extensiontype_=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = "fpi"
        if Point is None:
            self.Point = []
        else:
            self.Point = Point
        self.Point_nsprefix_ = "fpi"
        self.extensiontype_ = extensiontype_
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, PointList)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if PointList.subclass:
            return PointList.subclass(*args_, **kwargs_)
        else:
            return PointList(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_Point(self):
        return self.Point
    def set_Point(self, Point):
        self.Point = Point
    def add_Point(self, value):
        self.Point.append(value)
    def insert_Point_at(self, index, value):
        self.Point.insert(index, value)
    def replace_Point_at(self, index, value):
        self.Point[index] = value
    def get_extensiontype_(self): return self.extensiontype_
    def set_extensiontype_(self, extensiontype_): self.extensiontype_ = extensiontype_
    def hasContent_(self):
        if (
            self.Point
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='PointList', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('PointList')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'PointList':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='PointList')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='PointList', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='PointList'):
        if self.extensiontype_ is not None and 'xsi:type' not in already_processed:
            already_processed.add('xsi:type')
            outfile.write(' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"')
            if ":" not in self.extensiontype_:
                imported_ns_type_prefix_ = GenerateDSNamespaceTypePrefixes_.get(self.extensiontype_, '')
                outfile.write(' xsi:type="%s%s"' % (imported_ns_type_prefix_, self.extensiontype_))
            else:
                outfile.write(' xsi:type="%s"' % self.extensiontype_)
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='PointList', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for Point_ in self.Point:
            namespaceprefix_ = self.Point_nsprefix_ + ':' if (UseCapturedNS_ and self.Point_nsprefix_) else ''
            Point_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Point', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('xsi:type', node)
        if value is not None and 'xsi:type' not in already_processed:
            already_processed.add('xsi:type')
            self.extensiontype_ = value
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'Point':
            obj_ = PointType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Point.append(obj_)
            obj_.original_tagname_ = 'Point'
# end class PointList


class FloorPlanRequirements(GeneratedsSuper):
    """<p>
    A set of floor plan requirements.
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, BuildingGeometry=None, RoomTypes=None, RoomAreaRatios=None, RoomAdjacencies=None, Objectives=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = "fpi"
        self.BuildingGeometry = BuildingGeometry
        self.BuildingGeometry_nsprefix_ = "fpi"
        self.RoomTypes = RoomTypes
        self.RoomTypes_nsprefix_ = "fpi"
        self.RoomAreaRatios = RoomAreaRatios
        self.RoomAreaRatios_nsprefix_ = "fpi"
        self.RoomAdjacencies = RoomAdjacencies
        self.RoomAdjacencies_nsprefix_ = "fpi"
        self.Objectives = Objectives
        self.Objectives_nsprefix_ = "fpi"
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, FloorPlanRequirements)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if FloorPlanRequirements.subclass:
            return FloorPlanRequirements.subclass(*args_, **kwargs_)
        else:
            return FloorPlanRequirements(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_BuildingGeometry(self):
        return self.BuildingGeometry
    def set_BuildingGeometry(self, BuildingGeometry):
        self.BuildingGeometry = BuildingGeometry
    def get_RoomTypes(self):
        return self.RoomTypes
    def set_RoomTypes(self, RoomTypes):
        self.RoomTypes = RoomTypes
    def get_RoomAreaRatios(self):
        return self.RoomAreaRatios
    def set_RoomAreaRatios(self, RoomAreaRatios):
        self.RoomAreaRatios = RoomAreaRatios
    def get_RoomAdjacencies(self):
        return self.RoomAdjacencies
    def set_RoomAdjacencies(self, RoomAdjacencies):
        self.RoomAdjacencies = RoomAdjacencies
    def get_Objectives(self):
        return self.Objectives
    def set_Objectives(self, Objectives):
        self.Objectives = Objectives
    def hasContent_(self):
        if (
            self.BuildingGeometry is not None or
            self.RoomTypes is not None or
            self.RoomAreaRatios is not None or
            self.RoomAdjacencies is not None or
            self.Objectives is not None
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='FloorPlanRequirements', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('FloorPlanRequirements')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'FloorPlanRequirements':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='FloorPlanRequirements')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='FloorPlanRequirements', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='FloorPlanRequirements'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='FloorPlanRequirements', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.BuildingGeometry is not None:
            namespaceprefix_ = self.BuildingGeometry_nsprefix_ + ':' if (UseCapturedNS_ and self.BuildingGeometry_nsprefix_) else ''
            self.BuildingGeometry.export(outfile, level, namespaceprefix_, namespacedef_='', name_='BuildingGeometry', pretty_print=pretty_print)
        if self.RoomTypes is not None:
            namespaceprefix_ = self.RoomTypes_nsprefix_ + ':' if (UseCapturedNS_ and self.RoomTypes_nsprefix_) else ''
            self.RoomTypes.export(outfile, level, namespaceprefix_, namespacedef_='', name_='RoomTypes', pretty_print=pretty_print)
        if self.RoomAreaRatios is not None:
            namespaceprefix_ = self.RoomAreaRatios_nsprefix_ + ':' if (UseCapturedNS_ and self.RoomAreaRatios_nsprefix_) else ''
            self.RoomAreaRatios.export(outfile, level, namespaceprefix_, namespacedef_='', name_='RoomAreaRatios', pretty_print=pretty_print)
        if self.RoomAdjacencies is not None:
            namespaceprefix_ = self.RoomAdjacencies_nsprefix_ + ':' if (UseCapturedNS_ and self.RoomAdjacencies_nsprefix_) else ''
            self.RoomAdjacencies.export(outfile, level, namespaceprefix_, namespacedef_='', name_='RoomAdjacencies', pretty_print=pretty_print)
        if self.Objectives is not None:
            namespaceprefix_ = self.Objectives_nsprefix_ + ':' if (UseCapturedNS_ and self.Objectives_nsprefix_) else ''
            self.Objectives.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Objectives', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'BuildingGeometry':
            obj_ = BuildingGeometryType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.BuildingGeometry = obj_
            obj_.original_tagname_ = 'BuildingGeometry'
        elif nodeName_ == 'RoomTypes':
            obj_ = RoomTypesType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.RoomTypes = obj_
            obj_.original_tagname_ = 'RoomTypes'
        elif nodeName_ == 'RoomAreaRatios':
            obj_ = RoomAreaRatiosType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.RoomAreaRatios = obj_
            obj_.original_tagname_ = 'RoomAreaRatios'
        elif nodeName_ == 'RoomAdjacencies':
            obj_ = RoomAdjacenciesType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.RoomAdjacencies = obj_
            obj_.original_tagname_ = 'RoomAdjacencies'
        elif nodeName_ == 'Objectives':
            obj_ = ObjectivesType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Objectives = obj_
            obj_.original_tagname_ = 'Objectives'
# end class FloorPlanRequirements


class PointType(GeneratedsSuper):
    """<p>
    A point in a point list. See the documentation for PointList.
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, x=None, y=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.x = _cast(float, x)
        self.x_nsprefix_ = None
        self.y = _cast(float, y)
        self.y_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, PointType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if PointType.subclass:
            return PointType.subclass(*args_, **kwargs_)
        else:
            return PointType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_x(self):
        return self.x
    def set_x(self, x):
        self.x = x
    def get_y(self):
        return self.y
    def set_y(self, y):
        self.y = y
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='PointType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('PointType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'PointType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='PointType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='PointType', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='PointType'):
        if self.x is not None and 'x' not in already_processed:
            already_processed.add('x')
            outfile.write(' x="%s"' % self.gds_format_double(self.x, input_name='x'))
        if self.y is not None and 'y' not in already_processed:
            already_processed.add('y')
            outfile.write(' y="%s"' % self.gds_format_double(self.y, input_name='y'))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='PointType', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('x', node)
        if value is not None and 'x' not in already_processed:
            already_processed.add('x')
            value = self.gds_parse_double(value, node, 'x')
            self.x = value
        value = find_attr_value_('y', node)
        if value is not None and 'y' not in already_processed:
            already_processed.add('y')
            value = self.gds_parse_double(value, node, 'y')
            self.y = value
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class PointType


class BuildingGeometryType(GeneratedsSuper):
    """<p>
    A floor within the building.
    </p><p>
    Each floor has a geometry delineated by rectilinear polygon. The polygon
    can be offset by setting the
    xOffset and yOffset attributes, which default to 0. The offset values will
    be added to each point, so
    to shift a floor "south" by six meters, xOffset should be -6.
    </p><p>
    Generates are free to reject multi-floor requirements.
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, xOffset=0, Floor=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.xOffset = _cast(float, xOffset)
        self.xOffset_nsprefix_ = None
        if Floor is None:
            self.Floor = []
        else:
            self.Floor = Floor
        self.Floor_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, BuildingGeometryType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if BuildingGeometryType.subclass:
            return BuildingGeometryType.subclass(*args_, **kwargs_)
        else:
            return BuildingGeometryType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_Floor(self):
        return self.Floor
    def set_Floor(self, Floor):
        self.Floor = Floor
    def add_Floor(self, value):
        self.Floor.append(value)
    def insert_Floor_at(self, index, value):
        self.Floor.insert(index, value)
    def replace_Floor_at(self, index, value):
        self.Floor[index] = value
    def get_xOffset(self):
        return self.xOffset
    def set_xOffset(self, xOffset):
        self.xOffset = xOffset
    def hasContent_(self):
        if (
            self.Floor
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='BuildingGeometryType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('BuildingGeometryType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'BuildingGeometryType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='BuildingGeometryType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='BuildingGeometryType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='BuildingGeometryType'):
        if self.xOffset != 0 and 'xOffset' not in already_processed:
            already_processed.add('xOffset')
            outfile.write(' xOffset="%s"' % self.gds_format_double(self.xOffset, input_name='xOffset'))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='BuildingGeometryType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for Floor_ in self.Floor:
            namespaceprefix_ = self.Floor_nsprefix_ + ':' if (UseCapturedNS_ and self.Floor_nsprefix_) else ''
            Floor_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Floor', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('xOffset', node)
        if value is not None and 'xOffset' not in already_processed:
            already_processed.add('xOffset')
            value = self.gds_parse_double(value, node, 'xOffset')
            self.xOffset = value
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'Floor':
            obj_ = FloorType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Floor.append(obj_)
            obj_.original_tagname_ = 'Floor'
# end class BuildingGeometryType


class FloorType(PointList):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = PointList
    def __init__(self, Point=None, name=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        super(globals().get("FloorType"), self).__init__(Point,  **kwargs_)
        self.name = _cast(None, name)
        self.name_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, FloorType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if FloorType.subclass:
            return FloorType.subclass(*args_, **kwargs_)
        else:
            return FloorType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_name(self):
        return self.name
    def set_name(self, name):
        self.name = name
    def hasContent_(self):
        if (
            super(FloorType, self).hasContent_()
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='FloorType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('FloorType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'FloorType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='FloorType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='FloorType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='FloorType'):
        super(FloorType, self).exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='FloorType')
        if self.name is not None and 'name' not in already_processed:
            already_processed.add('name')
            outfile.write(' name=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.name), input_name='name')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='FloorType', fromsubclass_=False, pretty_print=True):
        super(FloorType, self).exportChildren(outfile, level, namespaceprefix_, namespacedef_, name_, True, pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('name', node)
        if value is not None and 'name' not in already_processed:
            already_processed.add('name')
            self.name = value
        super(FloorType, self).buildAttributes(node, attrs, already_processed)
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        super(FloorType, self).buildChildren(child_, node, nodeName_, True)
        pass
# end class FloorType


class RoomTypesType(GeneratedsSuper):
    """A list of room types a floor plan may consist of."""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, RoomType=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        if RoomType is None:
            self.RoomType = []
        else:
            self.RoomType = RoomType
        self.RoomType_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, RoomTypesType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if RoomTypesType.subclass:
            return RoomTypesType.subclass(*args_, **kwargs_)
        else:
            return RoomTypesType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_RoomType(self):
        return self.RoomType
    def set_RoomType(self, RoomType):
        self.RoomType = RoomType
    def add_RoomType(self, value):
        self.RoomType.append(value)
    def insert_RoomType_at(self, index, value):
        self.RoomType.insert(index, value)
    def replace_RoomType_at(self, index, value):
        self.RoomType[index] = value
    def hasContent_(self):
        if (
            self.RoomType
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomTypesType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('RoomTypesType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'RoomTypesType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='RoomTypesType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='RoomTypesType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='RoomTypesType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomTypesType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for RoomType_ in self.RoomType:
            namespaceprefix_ = self.RoomType_nsprefix_ + ':' if (UseCapturedNS_ and self.RoomType_nsprefix_) else ''
            RoomType_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='RoomType', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'RoomType':
            obj_ = RoomTypeType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.RoomType.append(obj_)
            obj_.original_tagname_ = 'RoomType'
# end class RoomTypesType


class RoomTypeType(GeneratedsSuper):
    """<p>
    A room type.
    </p><p>
    A room type describes the constraints of a room with that type. Room types
    are named; these names
    are used in further room type-room type relations and when representing a
    floor plan.
    </p><p>
    Rooms can also be marked "main", which indicates that the room is
    </p><p>
    The possible constraints are:
    <ol>
    <li>Minimum and maximum amount of rooms that should have this type</li>
    <li>Minimum and maximum lengths</li>
    <li>A minimum and maximum area</li>
    <li>A minimum and maximum aspect ratio</li>
    <li>Lowest and highest floor rooms with this type may appear on (0-indexed,
    so 0 is the ground floor)</li>
    <li>A minimum and maximum length of external wall</li>
    <li>Whether the room can be used to reach other rooms</li>
    <li>Whether the room must be accessible via other thoroughfares</li>
    </ol>
    </p>"""
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, name=None, thoroughfare=False, publiclyAccessible=False, main=False, Count=None, Width=None, Length=None, Area=None, AspectRatio=None, Floor=None, ExternalWall=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.name = _cast(None, name)
        self.name_nsprefix_ = None
        self.thoroughfare = _cast(bool, thoroughfare)
        self.thoroughfare_nsprefix_ = None
        self.publiclyAccessible = _cast(bool, publiclyAccessible)
        self.publiclyAccessible_nsprefix_ = None
        self.main = _cast(bool, main)
        self.main_nsprefix_ = None
        self.Count = Count
        self.Count_nsprefix_ = None
        self.Width = Width
        self.Width_nsprefix_ = None
        self.Length = Length
        self.Length_nsprefix_ = None
        self.Area = Area
        self.Area_nsprefix_ = None
        self.AspectRatio = AspectRatio
        self.AspectRatio_nsprefix_ = None
        self.Floor = Floor
        self.Floor_nsprefix_ = None
        self.ExternalWall = ExternalWall
        self.ExternalWall_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, RoomTypeType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if RoomTypeType.subclass:
            return RoomTypeType.subclass(*args_, **kwargs_)
        else:
            return RoomTypeType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_Count(self):
        return self.Count
    def set_Count(self, Count):
        self.Count = Count
    def get_Width(self):
        return self.Width
    def set_Width(self, Width):
        self.Width = Width
    def get_Length(self):
        return self.Length
    def set_Length(self, Length):
        self.Length = Length
    def get_Area(self):
        return self.Area
    def set_Area(self, Area):
        self.Area = Area
    def get_AspectRatio(self):
        return self.AspectRatio
    def set_AspectRatio(self, AspectRatio):
        self.AspectRatio = AspectRatio
    def get_Floor(self):
        return self.Floor
    def set_Floor(self, Floor):
        self.Floor = Floor
    def get_ExternalWall(self):
        return self.ExternalWall
    def set_ExternalWall(self, ExternalWall):
        self.ExternalWall = ExternalWall
    def get_name(self):
        return self.name
    def set_name(self, name):
        self.name = name
    def get_thoroughfare(self):
        return self.thoroughfare
    def set_thoroughfare(self, thoroughfare):
        self.thoroughfare = thoroughfare
    def get_publiclyAccessible(self):
        return self.publiclyAccessible
    def set_publiclyAccessible(self, publiclyAccessible):
        self.publiclyAccessible = publiclyAccessible
    def get_main(self):
        return self.main
    def set_main(self, main):
        self.main = main
    def validate_RoomName(self, value):
        # Validate type fpi:RoomName, a restriction on xs:string.
        if value is not None and Validate_simpletypes_ and self.gds_collector_ is not None:
            if not isinstance(value, str):
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s is not of the correct base simple type (str)' % {"value": value, "lineno": lineno, })
                return False
            if len(value) < 1:
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s does not match xsd minLength restriction on RoomName' % {"value" : encode_str_2_3(value), "lineno": lineno} )
                result = False
    def hasContent_(self):
        if (
            self.Count is not None or
            self.Width is not None or
            self.Length is not None or
            self.Area is not None or
            self.AspectRatio is not None or
            self.Floor is not None or
            self.ExternalWall is not None
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomTypeType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('RoomTypeType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'RoomTypeType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='RoomTypeType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='RoomTypeType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='RoomTypeType'):
        if self.name is not None and 'name' not in already_processed:
            already_processed.add('name')
            outfile.write(' name=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.name), input_name='name')), ))
        if self.thoroughfare and 'thoroughfare' not in already_processed:
            already_processed.add('thoroughfare')
            outfile.write(' thoroughfare="%s"' % self.gds_format_boolean(self.thoroughfare, input_name='thoroughfare'))
        if self.publiclyAccessible and 'publiclyAccessible' not in already_processed:
            already_processed.add('publiclyAccessible')
            outfile.write(' publiclyAccessible="%s"' % self.gds_format_boolean(self.publiclyAccessible, input_name='publiclyAccessible'))
        if self.main and 'main' not in already_processed:
            already_processed.add('main')
            outfile.write(' main="%s"' % self.gds_format_boolean(self.main, input_name='main'))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomTypeType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.Count is not None:
            namespaceprefix_ = self.Count_nsprefix_ + ':' if (UseCapturedNS_ and self.Count_nsprefix_) else ''
            self.Count.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Count', pretty_print=pretty_print)
        if self.Width is not None:
            namespaceprefix_ = self.Width_nsprefix_ + ':' if (UseCapturedNS_ and self.Width_nsprefix_) else ''
            self.Width.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Width', pretty_print=pretty_print)
        if self.Length is not None:
            namespaceprefix_ = self.Length_nsprefix_ + ':' if (UseCapturedNS_ and self.Length_nsprefix_) else ''
            self.Length.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Length', pretty_print=pretty_print)
        if self.Area is not None:
            namespaceprefix_ = self.Area_nsprefix_ + ':' if (UseCapturedNS_ and self.Area_nsprefix_) else ''
            self.Area.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Area', pretty_print=pretty_print)
        if self.AspectRatio is not None:
            namespaceprefix_ = self.AspectRatio_nsprefix_ + ':' if (UseCapturedNS_ and self.AspectRatio_nsprefix_) else ''
            self.AspectRatio.export(outfile, level, namespaceprefix_, namespacedef_='', name_='AspectRatio', pretty_print=pretty_print)
        if self.Floor is not None:
            namespaceprefix_ = self.Floor_nsprefix_ + ':' if (UseCapturedNS_ and self.Floor_nsprefix_) else ''
            self.Floor.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Floor', pretty_print=pretty_print)
        if self.ExternalWall is not None:
            namespaceprefix_ = self.ExternalWall_nsprefix_ + ':' if (UseCapturedNS_ and self.ExternalWall_nsprefix_) else ''
            self.ExternalWall.export(outfile, level, namespaceprefix_, namespacedef_='', name_='ExternalWall', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('name', node)
        if value is not None and 'name' not in already_processed:
            already_processed.add('name')
            self.name = value
            self.validate_RoomName(self.name)    # validate type RoomName
        value = find_attr_value_('thoroughfare', node)
        if value is not None and 'thoroughfare' not in already_processed:
            already_processed.add('thoroughfare')
            if value in ('true', '1'):
                self.thoroughfare = True
            elif value in ('false', '0'):
                self.thoroughfare = False
            else:
                raise_parse_error(node, 'Bad boolean attribute')
        value = find_attr_value_('publiclyAccessible', node)
        if value is not None and 'publiclyAccessible' not in already_processed:
            already_processed.add('publiclyAccessible')
            if value in ('true', '1'):
                self.publiclyAccessible = True
            elif value in ('false', '0'):
                self.publiclyAccessible = False
            else:
                raise_parse_error(node, 'Bad boolean attribute')
        value = find_attr_value_('main', node)
        if value is not None and 'main' not in already_processed:
            already_processed.add('main')
            if value in ('true', '1'):
                self.main = True
            elif value in ('false', '0'):
                self.main = False
            else:
                raise_parse_error(node, 'Bad boolean attribute')
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'Count':
            obj_ = CountRange.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Count = obj_
            obj_.original_tagname_ = 'Count'
        elif nodeName_ == 'Width':
            class_obj_ = self.get_class_obj_(child_, Range)
            obj_ = class_obj_.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Width = obj_
            obj_.original_tagname_ = 'Width'
        elif nodeName_ == 'Length':
            class_obj_ = self.get_class_obj_(child_, Range)
            obj_ = class_obj_.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Length = obj_
            obj_.original_tagname_ = 'Length'
        elif nodeName_ == 'Area':
            class_obj_ = self.get_class_obj_(child_, Range)
            obj_ = class_obj_.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Area = obj_
            obj_.original_tagname_ = 'Area'
        elif nodeName_ == 'AspectRatio':
            class_obj_ = self.get_class_obj_(child_, Range)
            obj_ = class_obj_.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.AspectRatio = obj_
            obj_.original_tagname_ = 'AspectRatio'
        elif nodeName_ == 'Floor':
            obj_ = CountRange.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Floor = obj_
            obj_.original_tagname_ = 'Floor'
        elif nodeName_ == 'ExternalWall':
            obj_ = ExternalWallType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.ExternalWall = obj_
            obj_.original_tagname_ = 'ExternalWall'
# end class RoomTypeType


class ExternalWallType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, minLength=0, face='any', gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.minLength = _cast(float, minLength)
        self.minLength_nsprefix_ = None
        self.face = _cast(None, face)
        self.face_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, ExternalWallType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if ExternalWallType.subclass:
            return ExternalWallType.subclass(*args_, **kwargs_)
        else:
            return ExternalWallType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_minLength(self):
        return self.minLength
    def set_minLength(self, minLength):
        self.minLength = minLength
    def get_face(self):
        return self.face
    def set_face(self, face):
        self.face = face
    def validate_CardinalDirectionOrAny(self, value):
        # Validate type fpi:CardinalDirectionOrAny, a restriction on xs:string.
        if value is not None and Validate_simpletypes_ and self.gds_collector_ is not None:
            if not isinstance(value, str):
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s is not of the correct base simple type (str)' % {"value": value, "lineno": lineno, })
                return False
            value = value
            enumerations = ['north', 'east', 'south', 'west', 'any']
            if value not in enumerations:
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s does not match xsd enumeration restriction on CardinalDirectionOrAny' % {"value" : encode_str_2_3(value), "lineno": lineno} )
                result = False
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='ExternalWallType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('ExternalWallType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'ExternalWallType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='ExternalWallType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='ExternalWallType', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='ExternalWallType'):
        if self.minLength != 0 and 'minLength' not in already_processed:
            already_processed.add('minLength')
            outfile.write(' minLength="%s"' % self.gds_format_double(self.minLength, input_name='minLength'))
        if self.face != "any" and 'face' not in already_processed:
            already_processed.add('face')
            outfile.write(' face=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.face), input_name='face')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='ExternalWallType', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('minLength', node)
        if value is not None and 'minLength' not in already_processed:
            already_processed.add('minLength')
            value = self.gds_parse_double(value, node, 'minLength')
            self.minLength = value
        value = find_attr_value_('face', node)
        if value is not None and 'face' not in already_processed:
            already_processed.add('face')
            self.face = value
            self.validate_CardinalDirectionOrAny(self.face)    # validate type CardinalDirectionOrAny
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class ExternalWallType


class RoomAreaRatiosType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, RoomAreaRatio=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        if RoomAreaRatio is None:
            self.RoomAreaRatio = []
        else:
            self.RoomAreaRatio = RoomAreaRatio
        self.RoomAreaRatio_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, RoomAreaRatiosType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if RoomAreaRatiosType.subclass:
            return RoomAreaRatiosType.subclass(*args_, **kwargs_)
        else:
            return RoomAreaRatiosType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_RoomAreaRatio(self):
        return self.RoomAreaRatio
    def set_RoomAreaRatio(self, RoomAreaRatio):
        self.RoomAreaRatio = RoomAreaRatio
    def add_RoomAreaRatio(self, value):
        self.RoomAreaRatio.append(value)
    def insert_RoomAreaRatio_at(self, index, value):
        self.RoomAreaRatio.insert(index, value)
    def replace_RoomAreaRatio_at(self, index, value):
        self.RoomAreaRatio[index] = value
    def hasContent_(self):
        if (
            self.RoomAreaRatio
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAreaRatiosType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('RoomAreaRatiosType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'RoomAreaRatiosType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='RoomAreaRatiosType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='RoomAreaRatiosType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='RoomAreaRatiosType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAreaRatiosType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for RoomAreaRatio_ in self.RoomAreaRatio:
            namespaceprefix_ = self.RoomAreaRatio_nsprefix_ + ':' if (UseCapturedNS_ and self.RoomAreaRatio_nsprefix_) else ''
            RoomAreaRatio_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='RoomAreaRatio', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'RoomAreaRatio':
            obj_ = RoomAreaRatioRange.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.RoomAreaRatio.append(obj_)
            obj_.original_tagname_ = 'RoomAreaRatio'
# end class RoomAreaRatiosType


class RoomAdjacenciesType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, RoomAdjacency=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        if RoomAdjacency is None:
            self.RoomAdjacency = []
        else:
            self.RoomAdjacency = RoomAdjacency
        self.RoomAdjacency_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, RoomAdjacenciesType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if RoomAdjacenciesType.subclass:
            return RoomAdjacenciesType.subclass(*args_, **kwargs_)
        else:
            return RoomAdjacenciesType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_RoomAdjacency(self):
        return self.RoomAdjacency
    def set_RoomAdjacency(self, RoomAdjacency):
        self.RoomAdjacency = RoomAdjacency
    def add_RoomAdjacency(self, value):
        self.RoomAdjacency.append(value)
    def insert_RoomAdjacency_at(self, index, value):
        self.RoomAdjacency.insert(index, value)
    def replace_RoomAdjacency_at(self, index, value):
        self.RoomAdjacency[index] = value
    def hasContent_(self):
        if (
            self.RoomAdjacency
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAdjacenciesType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('RoomAdjacenciesType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'RoomAdjacenciesType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='RoomAdjacenciesType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='RoomAdjacenciesType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='RoomAdjacenciesType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAdjacenciesType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for RoomAdjacency_ in self.RoomAdjacency:
            namespaceprefix_ = self.RoomAdjacency_nsprefix_ + ':' if (UseCapturedNS_ and self.RoomAdjacency_nsprefix_) else ''
            RoomAdjacency_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='RoomAdjacency', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'RoomAdjacency':
            obj_ = RoomAdjacencyType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.RoomAdjacency.append(obj_)
            obj_.original_tagname_ = 'RoomAdjacency'
# end class RoomAdjacenciesType


class RoomAdjacencyType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, roomA=None, roomB=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.roomA = _cast(None, roomA)
        self.roomA_nsprefix_ = None
        self.roomB = _cast(None, roomB)
        self.roomB_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, RoomAdjacencyType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if RoomAdjacencyType.subclass:
            return RoomAdjacencyType.subclass(*args_, **kwargs_)
        else:
            return RoomAdjacencyType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_roomA(self):
        return self.roomA
    def set_roomA(self, roomA):
        self.roomA = roomA
    def get_roomB(self):
        return self.roomB
    def set_roomB(self, roomB):
        self.roomB = roomB
    def validate_RoomName(self, value):
        # Validate type fpi:RoomName, a restriction on xs:string.
        if value is not None and Validate_simpletypes_ and self.gds_collector_ is not None:
            if not isinstance(value, str):
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s is not of the correct base simple type (str)' % {"value": value, "lineno": lineno, })
                return False
            if len(value) < 1:
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s does not match xsd minLength restriction on RoomName' % {"value" : encode_str_2_3(value), "lineno": lineno} )
                result = False
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAdjacencyType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('RoomAdjacencyType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'RoomAdjacencyType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='RoomAdjacencyType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='RoomAdjacencyType', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='RoomAdjacencyType'):
        if self.roomA is not None and 'roomA' not in already_processed:
            already_processed.add('roomA')
            outfile.write(' roomA=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.roomA), input_name='roomA')), ))
        if self.roomB is not None and 'roomB' not in already_processed:
            already_processed.add('roomB')
            outfile.write(' roomB=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.roomB), input_name='roomB')), ))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='RoomAdjacencyType', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('roomA', node)
        if value is not None and 'roomA' not in already_processed:
            already_processed.add('roomA')
            self.roomA = value
            self.validate_RoomName(self.roomA)    # validate type RoomName
        value = find_attr_value_('roomB', node)
        if value is not None and 'roomB' not in already_processed:
            already_processed.add('roomB')
            self.roomB = value
            self.validate_RoomName(self.roomB)    # validate type RoomName
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class RoomAdjacencyType


class ObjectivesType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, Objective=None, gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        if Objective is None:
            self.Objective = []
        else:
            self.Objective = Objective
        self.Objective_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, ObjectivesType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if ObjectivesType.subclass:
            return ObjectivesType.subclass(*args_, **kwargs_)
        else:
            return ObjectivesType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_Objective(self):
        return self.Objective
    def set_Objective(self, Objective):
        self.Objective = Objective
    def add_Objective(self, value):
        self.Objective.append(value)
    def insert_Objective_at(self, index, value):
        self.Objective.insert(index, value)
    def replace_Objective_at(self, index, value):
        self.Objective[index] = value
    def hasContent_(self):
        if (
            self.Objective
        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='ObjectivesType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('ObjectivesType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'ObjectivesType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='ObjectivesType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='ObjectivesType', pretty_print=pretty_print)
            showIndent(outfile, level, pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='ObjectivesType'):
        pass
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='ObjectivesType', fromsubclass_=False, pretty_print=True):
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        for Objective_ in self.Objective:
            namespaceprefix_ = self.Objective_nsprefix_ + ':' if (UseCapturedNS_ and self.Objective_nsprefix_) else ''
            Objective_.export(outfile, level, namespaceprefix_, namespacedef_='', name_='Objective', pretty_print=pretty_print)
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        pass
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        if nodeName_ == 'Objective':
            obj_ = ObjectiveType.factory(parent_object_=self)
            obj_.build(child_, gds_collector_=gds_collector_)
            self.Objective.append(obj_)
            obj_.original_tagname_ = 'Objective'
# end class ObjectivesType


class ObjectiveType(GeneratedsSuper):
    __hash__ = GeneratedsSuper.__hash__
    subclass = None
    superclass = None
    def __init__(self, name=None, weight='1', gds_collector_=None, **kwargs_):
        self.gds_collector_ = gds_collector_
        self.gds_elementtree_node_ = None
        self.original_tagname_ = None
        self.parent_object_ = kwargs_.get('parent_object_')
        self.ns_prefix_ = None
        self.name = _cast(None, name)
        self.name_nsprefix_ = None
        self.weight = _cast(float, weight)
        self.weight_nsprefix_ = None
    def factory(*args_, **kwargs_):
        if CurrentSubclassModule_ is not None:
            subclass = getSubclassFromModule_(
                CurrentSubclassModule_, ObjectiveType)
            if subclass is not None:
                return subclass(*args_, **kwargs_)
        if ObjectiveType.subclass:
            return ObjectiveType.subclass(*args_, **kwargs_)
        else:
            return ObjectiveType(*args_, **kwargs_)
    factory = staticmethod(factory)
    def get_ns_prefix_(self):
        return self.ns_prefix_
    def set_ns_prefix_(self, ns_prefix):
        self.ns_prefix_ = ns_prefix
    def get_name(self):
        return self.name
    def set_name(self, name):
        self.name = name
    def get_weight(self):
        return self.weight
    def set_weight(self, weight):
        self.weight = weight
    def validate_ObjectiveName(self, value):
        # Validate type fpi:ObjectiveName, a restriction on xs:string.
        if value is not None and Validate_simpletypes_ and self.gds_collector_ is not None:
            if not isinstance(value, str):
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s is not of the correct base simple type (str)' % {"value": value, "lineno": lineno, })
                return False
            value = value
            enumerations = ['unusedArea', 'size', 'adjacency', 'corners', 'roomArea', 'connectivity', 'boundingBoxArea', 'boundingBoxDepth', 'windowArea']
            if value not in enumerations:
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s does not match xsd enumeration restriction on ObjectiveName' % {"value" : encode_str_2_3(value), "lineno": lineno} )
                result = False
    def validate_PositiveDouble(self, value):
        # Validate type fpi:PositiveDouble, a restriction on xs:double.
        if value is not None and Validate_simpletypes_ and self.gds_collector_ is not None:
            if not isinstance(value, float):
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s is not of the correct base simple type (float)' % {"value": value, "lineno": lineno, })
                return False
            if value < 0:
                lineno = self.gds_get_node_lineno_()
                self.gds_collector_.add_message('Value "%(value)s"%(lineno)s does not match xsd minInclusive restriction on PositiveDouble' % {"value": value, "lineno": lineno} )
                result = False
    def hasContent_(self):
        if (

        ):
            return True
        else:
            return False
    def export(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='ObjectiveType', pretty_print=True):
        imported_ns_def_ = GenerateDSNamespaceDefs_.get('ObjectiveType')
        if imported_ns_def_ is not None:
            namespacedef_ = imported_ns_def_
        if pretty_print:
            eol_ = '\n'
        else:
            eol_ = ''
        if self.original_tagname_ is not None and name_ == 'ObjectiveType':
            name_ = self.original_tagname_
        if UseCapturedNS_ and self.ns_prefix_:
            namespaceprefix_ = self.ns_prefix_ + ':'
        showIndent(outfile, level, pretty_print)
        outfile.write('<%s%s%s' % (namespaceprefix_, name_, namespacedef_ and ' ' + namespacedef_ or '', ))
        already_processed = set()
        self.exportAttributes(outfile, level, already_processed, namespaceprefix_, name_='ObjectiveType')
        if self.hasContent_():
            outfile.write('>%s' % (eol_, ))
            self.exportChildren(outfile, level + 1, namespaceprefix_, namespacedef_, name_='ObjectiveType', pretty_print=pretty_print)
            outfile.write('</%s%s>%s' % (namespaceprefix_, name_, eol_))
        else:
            outfile.write('/>%s' % (eol_, ))
    def exportAttributes(self, outfile, level, already_processed, namespaceprefix_='', name_='ObjectiveType'):
        if self.name is not None and 'name' not in already_processed:
            already_processed.add('name')
            outfile.write(' name=%s' % (self.gds_encode(self.gds_format_string(quote_attrib(self.name), input_name='name')), ))
        if self.weight != 1 and 'weight' not in already_processed:
            already_processed.add('weight')
            outfile.write(' weight="%s"' % self.gds_format_double(self.weight, input_name='weight'))
    def exportChildren(self, outfile, level, namespaceprefix_='', namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"', name_='ObjectiveType', fromsubclass_=False, pretty_print=True):
        pass
    def build(self, node, gds_collector_=None):
        self.gds_collector_ = gds_collector_
        if SaveElementTreeNode:
            self.gds_elementtree_node_ = node
        already_processed = set()
        self.ns_prefix_ = node.prefix
        self.buildAttributes(node, node.attrib, already_processed)
        for child in node:
            nodeName_ = Tag_pattern_.match(child.tag).groups()[-1]
            self.buildChildren(child, node, nodeName_, gds_collector_=gds_collector_)
        return self
    def buildAttributes(self, node, attrs, already_processed):
        value = find_attr_value_('name', node)
        if value is not None and 'name' not in already_processed:
            already_processed.add('name')
            self.name = value
            self.validate_ObjectiveName(self.name)    # validate type ObjectiveName
        value = find_attr_value_('weight', node)
        if value is not None and 'weight' not in already_processed:
            already_processed.add('weight')
            value = self.gds_parse_double(value, node, 'weight')
            self.weight = value
            self.validate_PositiveDouble(self.weight)    # validate type PositiveDouble
    def buildChildren(self, child_, node, nodeName_, fromsubclass_=False, gds_collector_=None):
        pass
# end class ObjectiveType


GDSClassesMapping = {
    'FloorPlanRequirements': FloorPlanRequirements,
}


USAGE_TEXT = """
Usage: python <Parser>.py [ -s ] <in_xml_file>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def get_root_tag(node):
    tag = Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = GDSClassesMapping.get(tag)
    if rootClass is None:
        rootClass = globals().get(tag)
    return tag, rootClass


def get_required_ns_prefix_defs(rootNode):
    '''Get all name space prefix definitions required in this XML doc.
    Return a dictionary of definitions and a char string of definitions.
    '''
    nsmap = {
        prefix: uri
        for node in rootNode.iter()
        for (prefix, uri) in node.nsmap.items()
        if prefix is not None
    }
    namespacedefs = ' '.join([
        'xmlns:{}="{}"'.format(prefix, uri)
        for prefix, uri in nsmap.items()
    ])
    return nsmap, namespacedefs


def parse(inFileName, silence=False, print_warnings=True):
    global CapturedNsmap_
    gds_collector = GdsCollector_()
    parser = None
    doc = parsexml_(inFileName, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    CapturedNsmap_, namespacedefs = get_required_ns_prefix_defs(rootNode)
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_=namespacedefs,
            pretty_print=True)
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj


def parseEtree(inFileName, silence=False, print_warnings=True,
               mapping=None, nsmap=None):
    parser = None
    doc = parsexml_(inFileName, parser)
    gds_collector = GdsCollector_()
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    # Enable Python to collect the space used by the DOM.
    if mapping is None:
        mapping = {}
    rootElement = rootObj.to_etree(
        None, name_=rootTag, mapping_=mapping, nsmap_=nsmap)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(str(content))
        sys.stdout.write('\n')
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False, print_warnings=True):
    '''Parse a string, create the object tree, and export it.

    Arguments:
    - inString -- A string.  This XML fragment should not start
      with an XML declaration containing an encoding.
    - silence -- A boolean.  If False, export the object.
    Returns -- The root object in the tree.
    '''
    parser = None
    rootNode= parsexmlstring_(inString, parser)
    gds_collector = GdsCollector_()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"')
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj


def parseLiteral(inFileName, silence=False, print_warnings=True):
    parser = None
    doc = parsexml_(inFileName, parser)
    gds_collector = GdsCollector_()
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode, gds_collector_=gds_collector)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from input import *\n\n')
        sys.stdout.write('import input as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    if print_warnings and len(gds_collector.get_messages()) > 0:
        separator = ('-' * 50) + '\n'
        sys.stderr.write(separator)
        sys.stderr.write('----- Warnings -- count: {} -----\n'.format(
            len(gds_collector.get_messages()), ))
        gds_collector.write_messages(sys.stderr)
        sys.stderr.write(separator)
    return rootObj


def main():
    args = sys.argv[1:]
    if len(args) == 1:
        parse(args[0])
    else:
        usage()


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()

RenameMappings_ = {
}

#
# Mapping of namespaces to types defined in them
# and the file in which each is defined.
# simpleTypes are marked "ST" and complexTypes "CT".
NamespaceToDefMappings_ = {'urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed': [('AllNNI',
                                                    'doc/schemas/input.xsd',
                                                    'ST'),
                                                   ('AllInteger',
                                                    'doc/schemas/input.xsd',
                                                    'ST'),
                                                   ('PositiveDouble',
                                                    'doc/schemas/input.xsd',
                                                    'ST'),
                                                   ('AllPositiveDouble',
                                                    'doc/schemas/input.xsd',
                                                    'ST'),
                                                   ('CardinalDirectionOrAny',
                                                    'doc/schemas/input.xsd',
                                                    'ST'),
                                                   ('ObjectiveName',
                                                    'doc/schemas/input.xsd',
                                                    'ST'),
                                                   ('RoomName',
                                                    'doc/schemas/input.xsd',
                                                    'ST'),
                                                   ('Range',
                                                    'doc/schemas/input.xsd',
                                                    'CT'),
                                                   ('CountRange',
                                                    'doc/schemas/input.xsd',
                                                    'CT'),
                                                   ('IntRange',
                                                    'doc/schemas/input.xsd',
                                                    'CT'),
                                                   ('RoomAreaRatioRange',
                                                    'doc/schemas/input.xsd',
                                                    'CT'),
                                                   ('PointList',
                                                    'doc/schemas/input.xsd',
                                                    'CT'),
                                                   ('FloorPlanRequirements',
                                                    'doc/schemas/input.xsd',
                                                    'CT')]}

__all__ = [
    "BuildingGeometryType",
    "CountRange",
    "ExternalWallType",
    "FloorPlanRequirements",
    "FloorType",
    "IntRange",
    "ObjectiveType",
    "ObjectivesType",
    "PointList",
    "PointType",
    "Range",
    "RoomAdjacenciesType",
    "RoomAdjacencyType",
    "RoomAreaRatioRange",
    "RoomAreaRatiosType",
    "RoomTypeType",
    "RoomTypesType"
]
