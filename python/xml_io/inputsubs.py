#!/usr/bin/env python

#
# Generated Tue Jun  1 11:36:16 2021 by generateDS.py version 2.38.6.
# Python 3.9.5 (default, May  4 2021, 03:36:27)  [Clang 12.0.0 (clang-1200.0.32.29)]
#
# Command line options:
#   ('-o', 'constraint/python/input.py')
#   ('-s', 'constraint/python/inputsubs.py')
#
# Command line arguments:
#   doc/schemas/input.xsd
#
# Command line:
#   /usr/local/bin/generateDS -o "constraint/python/input.py" -s "constraint/python/inputsubs.py" doc/schemas/input.xsd
#
# Current working directory (os.getcwd()):
#   automated-floor-plan-generation
#

import os
import sys
from lxml import etree as etree_

import python.xml_io as supermod

def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Globals
#

ExternalEncoding = ''
SaveElementTreeNode = True

#
# Data representation classes
#


class RangeSub(supermod.Range):
    def __init__(self, min='0', max='unbounded', target='0', extensiontype_=None, **kwargs_):
        super(RangeSub, self).__init__(min, max, target, extensiontype_,  **kwargs_)
supermod.Range.subclass = RangeSub
# end class RangeSub


class CountRangeSub(supermod.CountRange):
    def __init__(self, min=1, max='1', target=1, **kwargs_):
        super(CountRangeSub, self).__init__(min, max, target,  **kwargs_)
supermod.CountRange.subclass = CountRangeSub
# end class CountRangeSub


class IntRangeSub(supermod.IntRange):
    def __init__(self, min='unbounded', max='unbounded', target='1', **kwargs_):
        super(IntRangeSub, self).__init__(min, max, target,  **kwargs_)
supermod.IntRange.subclass = IntRangeSub
# end class IntRangeSub


class RoomAreaRatioRangeSub(supermod.RoomAreaRatioRange):
    def __init__(self, min='0', max='unbounded', target='0', roomA=None, roomB=None, **kwargs_):
        super(RoomAreaRatioRangeSub, self).__init__(min, max, target, roomA, roomB,  **kwargs_)
supermod.RoomAreaRatioRange.subclass = RoomAreaRatioRangeSub
# end class RoomAreaRatioRangeSub


class PointListSub(supermod.PointList):
    def __init__(self, Point=None, extensiontype_=None, **kwargs_):
        super(PointListSub, self).__init__(Point, extensiontype_,  **kwargs_)
supermod.PointList.subclass = PointListSub
# end class PointListSub


class FloorPlanRequirementsSub(supermod.FloorPlanRequirements):
    def __init__(self, BuildingGeometry=None, RoomTypes=None, RoomAreaRatios=None, RoomAdjacencies=None, Objectives=None, **kwargs_):
        super(FloorPlanRequirementsSub, self).__init__(BuildingGeometry, RoomTypes, RoomAreaRatios, RoomAdjacencies, Objectives,  **kwargs_)
supermod.FloorPlanRequirements.subclass = FloorPlanRequirementsSub
# end class FloorPlanRequirementsSub


class PointTypeSub(supermod.PointType):
    def __init__(self, x=None, y=None, **kwargs_):
        super(PointTypeSub, self).__init__(x, y,  **kwargs_)
supermod.PointType.subclass = PointTypeSub
# end class PointTypeSub


class BuildingGeometryTypeSub(supermod.BuildingGeometryType):
    def __init__(self, xOffset=0, Floor=None, **kwargs_):
        super(BuildingGeometryTypeSub, self).__init__(xOffset, Floor,  **kwargs_)
supermod.BuildingGeometryType.subclass = BuildingGeometryTypeSub
# end class BuildingGeometryTypeSub


class FloorTypeSub(supermod.FloorType):
    def __init__(self, Point=None, name=None, **kwargs_):
        super(FloorTypeSub, self).__init__(Point, name,  **kwargs_)
supermod.FloorType.subclass = FloorTypeSub
# end class FloorTypeSub


class RoomTypesTypeSub(supermod.RoomTypesType):
    def __init__(self, RoomType=None, **kwargs_):
        super(RoomTypesTypeSub, self).__init__(RoomType,  **kwargs_)
supermod.RoomTypesType.subclass = RoomTypesTypeSub
# end class RoomTypesTypeSub


class RoomTypeTypeSub(supermod.RoomTypeType):
    def __init__(self, name=None, thoroughfare=False, publiclyAccessible=False, main=False, Count=None, Width=None, Length=None, Area=None, AspectRatio=None, Floor=None, ExternalWall=None, **kwargs_):
        super(RoomTypeTypeSub, self).__init__(name, thoroughfare, publiclyAccessible, main, Count, Width, Length, Area, AspectRatio, Floor, ExternalWall,  **kwargs_)
supermod.RoomTypeType.subclass = RoomTypeTypeSub
# end class RoomTypeTypeSub


class ExternalWallTypeSub(supermod.ExternalWallType):
    def __init__(self, minLength=0, face='any', **kwargs_):
        super(ExternalWallTypeSub, self).__init__(minLength, face,  **kwargs_)
supermod.ExternalWallType.subclass = ExternalWallTypeSub
# end class ExternalWallTypeSub


class RoomAreaRatiosTypeSub(supermod.RoomAreaRatiosType):
    def __init__(self, RoomAreaRatio=None, **kwargs_):
        super(RoomAreaRatiosTypeSub, self).__init__(RoomAreaRatio,  **kwargs_)
supermod.RoomAreaRatiosType.subclass = RoomAreaRatiosTypeSub
# end class RoomAreaRatiosTypeSub


class RoomAdjacenciesTypeSub(supermod.RoomAdjacenciesType):
    def __init__(self, RoomAdjacency=None, **kwargs_):
        super(RoomAdjacenciesTypeSub, self).__init__(RoomAdjacency,  **kwargs_)
supermod.RoomAdjacenciesType.subclass = RoomAdjacenciesTypeSub
# end class RoomAdjacenciesTypeSub


class RoomAdjacencyTypeSub(supermod.RoomAdjacencyType):
    def __init__(self, roomA=None, roomB=None, **kwargs_):
        super(RoomAdjacencyTypeSub, self).__init__(roomA, roomB,  **kwargs_)
supermod.RoomAdjacencyType.subclass = RoomAdjacencyTypeSub
# end class RoomAdjacencyTypeSub


class ObjectivesTypeSub(supermod.ObjectivesType):
    def __init__(self, Objective=None, **kwargs_):
        super(ObjectivesTypeSub, self).__init__(Objective,  **kwargs_)
supermod.ObjectivesType.subclass = ObjectivesTypeSub
# end class ObjectivesTypeSub


class ObjectiveTypeSub(supermod.ObjectiveType):
    def __init__(self, name=None, weight='1', **kwargs_):
        super(ObjectiveTypeSub, self).__init__(name, weight,  **kwargs_)
supermod.ObjectiveType.subclass = ObjectiveTypeSub
# end class ObjectiveTypeSub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = supermod.Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = supermod.Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    if sys.version_info.major == 2:
        from StringIO import StringIO
    else:
        from xml_io import BytesIO as StringIO
    parser = None
    rootNode= parsexmlstring_(inString, parser)
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = supermod.Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed"')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Range'
        rootClass = supermod.Range
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from ??? import *\n\n')
        sys.stdout.write('import ??? as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
