include "globals.mzn";

function var int: coordinate_to_int(var int: x, var int: y) = (y-1) * ncells_x + x;
function var int: count_cells(var rooms: r, array[1..ncells_x, 1..ncells_y] of var rooms: cells) = count([cells[x,y] | x in 1..ncells_x, y in 1..ncells_y], r);
predicate check_adjacent(var rooms: rA, var rooms: rB, array[1..ncells_x, 1..ncells_y] of var rooms: cells) = 
count([
  count([cells[x, y_l] == rA /\ cells[x + 1, y_l] == rB | y_l in 1..ncells_y], true) >= cells_per_meter \/
  count([cells[x, y_l] == rB /\ cells[x + 1, y_l] == rA | y_l in 1..ncells_y], true) >= cells_per_meter
| x in 1..ncells_x-1], true) >= 1 \/
count([
  count([cells[x_l, y] == rA /\ cells[x_l, y + 1] == rB | x_l in 1..ncells_x], true) >= cells_per_meter \/
  count([cells[x_l, y] == rB /\ cells[x_l, y + 1] == rA | x_l in 1..ncells_x], true) >= cells_per_meter
| y in 1..ncells_y-1], true) >= 1;

% Input
int: length;
int: width;
float: max_ratio = int2float(max(length, width));
int: cells_per_meter;
int: cells_squared = cells_per_meter * cells_per_meter;
enum rooms;
array[rooms] of float: prev_areas;
array[rooms] of float: min_areas;
array[rooms] of float: max_areas;
array[rooms] of float: min_lengths;
array[rooms] of float: max_lengths;
array[rooms] of float: prev_lengths;
array[rooms] of float: min_widths;
array[rooms] of float: max_widths;
array[rooms] of float: prev_widths;
array[rooms] of float: min_aspects;
array[rooms] of float: max_aspects;
array[rooms] of float: prev_aspects;
array[rooms, rooms] of bool: adjacencies;
int: gauss_rooms = ((card(rooms) - 1) * card(rooms)) div 2;
array[1..gauss_rooms] of 1..card(rooms): p_from;
array[1..gauss_rooms] of 1..card(rooms): p_to;
array[1..card(rooms)] of bool: public_rooms;
array[1..card(rooms)] of bool: thoroughfare_rooms;
array[rooms, rooms] of int: p_mapping;

% The input is in metres, amount of cells is defined as input
int: ncells_x = length * cells_per_meter;
int: ncells_y = width * cells_per_meter;

% Array with assigned room for each cell in the grid, might want to make it optional
array[1..ncells_x, 1..ncells_y] of var rooms: cells;


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rooms are only one room %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use connected to check whether a room is actually one room
int: ngraph_cells = ncells_x * ncells_y;
% Each node has at most 2 "outgoing" edges, right and down (undirected graph)
int: ncells = ncells_x * ncells_y;
int: nedges = ncells * 2;
array[1..nedges] of 1..ncells: from;
array[1..nedges] of 1..ncells: to;

% Each room needs to exist
constraint forall(r in rooms)(count_leq(cells, r, 1));

% Specify edges and nodes of subgraphs for each room
array[rooms, 1..ncells] of var bool: ns_r;
array[rooms, 1..nedges] of var bool: es_r;
constraint forall(r in rooms, x in 1..ncells_x, y in 1..ncells_y)(
  ns_r[r, coordinate_to_int(x, y)] = (cells[x, y] == r) /\
  es_r[r, coordinate_to_int(x, y) * 2 - 1] = (if (x + 1 <= ncells_x) then (cells[x, y] == r /\ cells[x+1,y] == r) else false endif) /\
  es_r[r, coordinate_to_int(x, y) * 2] = (if (y + 1 <= ncells_y) then (cells[x, y] == r /\ cells[x, y+1] == r) else false endif)
);

% Each room must be fully connected
constraint forall(r in rooms)(
  connected(
    from,
    to,
    [ns_r[r, i] | i in 1..ncells],
    [es_r[r, i] | i in 1..nedges]
  )
);


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bounding Box dimensions %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

array[rooms] of var opt 1..length: r_lengths = [max (y in 1..ncells_y where count_leq([cells[x,y] | x in 1..ncells_x], r, 1)) (y + 1) - min (y in 1..ncells_y where count_leq([cells[x,y] | x in 1..ncells_x], r, 1)) (y) | r in rooms];
array[rooms] of var opt 1..width: r_widths = [max (x in 1..ncells_x where count_leq([cells[x,y] | y in 1..ncells_y], r, 1)) (x + 1) - min (x in 1..ncells_x where count_leq([cells[x,y] | y in 1..ncells_y], r, 1)) (x) | r in rooms];

% Minimal length
constraint forall(r in rooms where min_lengths[r] >= 0)(
  r_lengths[r] >= floor(min_lengths[r]) * cells_per_meter
);

% Minimal width
constraint forall(r in rooms where min_widths[r] >= 0)(
  r_widths[r] >= floor(min_widths[r]) * cells_per_meter
);

% Maximal length
constraint forall(r in rooms where max_lengths[r] >= 0)(
  r_lengths[r] <= ceil(max_lengths[r]) * cells_per_meter
);

% Maximal width
constraint forall(r in rooms where max_widths[r] >= 0)(
  r_widths[r] <= ceil(max_widths[r]) * cells_per_meter
);

% Scores of width and length
var float: width_score = sum(r in rooms where prev_widths[r] >= 0)(if prev_widths[r] * cells_per_meter > deopt(r_widths[r]) then prev_widths[r] * cells_per_meter - deopt(r_widths[r]) else deopt(r_widths[r]) - prev_widths[r] * cells_per_meter endif);
var float: length_score = sum(r in rooms where prev_lengths[r] >= 0)(if prev_lengths[r] * cells_per_meter > deopt(r_lengths[r]) then prev_lengths[r] * cells_per_meter - deopt(r_lengths[r]) else deopt(r_lengths[r]) - prev_lengths[r] * cells_per_meter endif);

% Aspect ratios - disabled due to incompatiblity with solver. Float div not supported
% Calculate the aspect ratio for each room
% array[rooms] of var opt 0.0..max_ratio: r_ratios = [
%   r_lengths[r] / r_widths[r] | r in rooms
% ];

% Minimal aspect ratio
% constraint forall(r in rooms where min_aspects[r] >= 0)(
%   r_ratios[r] >= min_aspects[r]
% );

% % Maximal aspect ratio
% constraint forall(r in rooms where max_aspects[r] >= 0)(
%   r_ratios[r] <= max_aspects[r]
% );

% var float: aspect_score = sum(r in rooms where prev_aspects[r] >= 0)(abs(prev_aspects[r] - deopt(r_ratios[r])));



%%%%%%%%%
% Areas %
%%%%%%%%%

% Minimal room size
constraint forall(r in rooms where min_areas[r] >= 0)(
  count_leq([cells[x,y] | x in 1..ncells_x, y in 1..ncells_y], r, floor(min_areas[r]) * cells_squared)
);

% Maximum room size
constraint forall(r in rooms where max_areas[r] >= 0)(
  count_geq([cells[x,y] | x in 1..ncells_x, y in 1..ncells_y], r, ceil(max_areas[r]) * cells_squared)
);

var float: area_score = sum(r in rooms where prev_areas[r] >= 0)(abs(prev_areas[r] * cells_squared - count_cells(r, cells)));


%%%%%%%%%%%%%%%
% Adjacencies %
%%%%%%%%%%%%%%%
constraint forall(rA, rB in rooms where adjacencies[rA, rB])(
  check_adjacent(rA, rB, cells)
);

%%%%%%%%%%%%%%%%%%
% Public private %
%%%%%%%%%%%%%%%%%%
array[1..gauss_rooms] of var bool: p_es;
constraint forall(rA in rooms, rB in rooms where p_mapping[rA, rB] >= 0)(
  p_es[p_mapping[rA, rB]] == ((thoroughfare_rooms[rA] \/ thoroughfare_rooms[rB]) /\ public_rooms[rA] /\ public_rooms[rB] /\ check_adjacent(rA, rB, cells))
);

constraint connected(
  p_from,
  p_to,
  public_rooms,
  p_es
);

%%%%%%%%%%%%%%%%%%%%%%%%
% Objective and Output %
%%%%%%%%%%%%%%%%%%%%%%%%

var float: target = width_score * width_score + length_score * length_score + area_score * area_score;
solve minimize(target);

output [ "\(r) " | r in rooms];
output [ "\n" ];
output [ "\(x), \(y); \(cells[x,y])\n" | x in 1..ncells_x, y in 1..ncells_y];
output [ "target: \(target)\n" ];
output [ "width \(r): \(r_widths[r])\n" | r in rooms ];
output [ "length \(r): \(r_lengths[r])\n" | r in rooms ];
output [ "area \(r): \(count_cells(r, cells))\n" | r in rooms ];
