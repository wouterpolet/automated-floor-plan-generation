from xml_io import input
from graph_generation import generate_grid_edge_lists
import dzn_io


def float_to_dzn_value(value):
    if value == 'unbounded':
        return -1
    # This typically means it's the default value
    if float(value) == 0:
        return -1
    return float(value)


def run(input_path, out_path, cells_per_meter):
    xml_in = input.parse(input_path)

    floor = xml_in.BuildingGeometry.Floor[0].Point
    if len(floor) != 4:
        exit('Only rectangles are supported')
    length = abs(round(floor[0].x - floor[2].x))
    width = abs(round(floor[0].y - floor[2].y))

    rooms = xml_in.RoomTypes.RoomType
    room_names = [r.name for r in rooms]
    min_areas = [float_to_dzn_value(r.Area.min) for r in rooms]
    max_areas = [float_to_dzn_value(r.Area.max) for r in rooms]
    prev_areas = [float_to_dzn_value(r.Area.target) for r in rooms]
    min_lengths = [float_to_dzn_value(r.Length.min) for r in rooms]
    max_lengths = [float_to_dzn_value(r.Length.max) for r in rooms]
    prev_lengths = [float_to_dzn_value(r.Length.target) for r in rooms]
    min_widths = [float_to_dzn_value(r.Width.min) for r in rooms]
    max_widths = [float_to_dzn_value(r.Width.max) for r in rooms]
    prev_widths = [float_to_dzn_value(r.Width.target) for r in rooms]
    min_aspects = [float_to_dzn_value(r.AspectRatio.min) for r in rooms]
    max_aspects = [float_to_dzn_value(r.AspectRatio.max) for r in rooms]
    prev_aspects = [float_to_dzn_value(r.AspectRatio.target) for r in rooms]

    adj = [['false'] * len(rooms) for i in range(len(rooms))]
    if xml_in.RoomAdjacencies:
        room_adjacencies = [(room_names.index(adj.roomA), room_names.index(adj.roomB)) for adj in xml_in.RoomAdjacencies.RoomAdjacency]
        for rA, rB in room_adjacencies:
            adj[rA][rB] = 'true'

    # Generate edge lists
    edge_from, edge_to = generate_grid_edge_lists(length * cells_per_meter, width * cells_per_meter)

    # Generate lists for public private graph
    public_rooms = ['true' if r.publiclyAccessible else 'false' for r in rooms]
    thoroughfare_rooms = ['true' if r.thoroughfare or r.main else 'false' for r in rooms]
    p_from = []
    p_to = []
    p_mapping = [[str(-1)] * len(rooms) for i in range(len(rooms))]
    for i, r_a in enumerate(room_names):
        for r_b in room_names[i+1:]:
            p_from.append(room_names.index(r_a) + 1)
            p_to.append(room_names.index(r_b) + 1)
    for i, (r_a, r_b) in enumerate(zip(p_from, p_to)):
        p_mapping[r_a - 1][r_b - 1] = str(i+1)

    with open(out_path, 'w') as f:
        dzn_io.write_value(f, 'length', length)
        dzn_io.write_value(f, 'width', width)
        dzn_io.write_value(f, 'cells_per_meter', cells_per_meter)
        dzn_io.write_enum(f, 'rooms', room_names)
        dzn_io.write_array1d(f, 'prev_areas', prev_areas)
        dzn_io.write_array1d(f, 'min_areas', min_areas)
        dzn_io.write_array1d(f, 'max_areas', max_areas)
        dzn_io.write_array1d(f, 'from', edge_from)
        dzn_io.write_array1d(f, 'to', edge_to)
        dzn_io.write_array1d(f, 'min_lengths', min_lengths)
        dzn_io.write_array1d(f, 'max_lengths', max_lengths)
        dzn_io.write_array1d(f, 'prev_lengths', prev_lengths)
        dzn_io.write_array1d(f, 'min_widths', min_widths)
        dzn_io.write_array1d(f, 'max_widths', max_widths)
        dzn_io.write_array1d(f, 'prev_widths', prev_widths)
        dzn_io.write_array1d(f, 'min_aspects', min_aspects)
        dzn_io.write_array1d(f, 'max_aspects', max_aspects)
        dzn_io.write_array1d(f, 'prev_aspects', prev_aspects)
        dzn_io.write_array2d(f, 'adjacencies', adj)
        dzn_io.write_array1d(f, 'public_rooms', public_rooms)
        dzn_io.write_array1d(f, 'thoroughfare_rooms', thoroughfare_rooms)
        dzn_io.write_array1d(f, 'p_from', p_from)
        dzn_io.write_array1d(f, 'p_to', p_to)
        dzn_io.write_array2d(f, 'p_mapping', p_mapping)
        f.close()
