from enum import Enum
from xml_io import output


class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __mul__(self, other):
        return Point(self.x * other.x, self.y + other.y)

    def __abs__(self):
        return Point(abs(self.x), abs(self.y))

    def __str__(self):
        return f'({self.x}, {self.y})'

    def __eq__(self, other):
        return other is not None and self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))


class Direction(Enum):
    NORTH = Point(0, -1)
    EAST = Point(1, 0)
    SOUTH = Point(0, 1)
    WEST = Point(-1, 0)


turn_left = {
    Direction.NORTH.value: Direction.WEST.value,
    Direction.EAST.value: Direction.NORTH.value,
    Direction.SOUTH.value: Direction.EAST.value,
    Direction.WEST.value: Direction.SOUTH.value
}

turn_right = {
    Direction.NORTH.value: Direction.EAST.value,
    Direction.EAST.value: Direction.SOUTH.value,
    Direction.SOUTH.value: Direction.WEST.value,
    Direction.WEST.value: Direction.NORTH.value
}


def _find_next_direction(grid, pos, dir, room):
    # If at edge of grid
    if pos + dir not in grid:
        return turn_right[dir]
    if dir == Direction.NORTH.value:
        left = grid[pos + Point(-1, -1)]
        right = grid[pos + Point(0, -1)]
    elif dir == Direction.EAST.value:
        left = grid[pos + Point(0, -1)]
        right = grid[pos]
    elif dir == Direction.SOUTH.value:
        right = grid[pos + Point(-1, 0)]
        left = grid[pos]
    elif dir == Direction.WEST.value:
        left = grid[pos + Point(-1, 0)]
        right = grid[pos + Point(-1, -1)]
    # If left is other room and right is room, straight
    if left != room and right == room:
        return dir
    # Both room is left
    if left == room and right == room:
        return turn_left[dir]
    if left != room and right != room:
        return turn_right[dir]
    print("oops")


def grid_to_polygons(grid, rooms):
    length = -1
    width = -1
    for p in grid.keys():
        if p.x >= length:
            length = p.x + 1
        if p.y >= width:
            width = p.y + 1
    length -= 1
    width -= 1
    polygons = {}
    for r in rooms:
        # Find first point of polygon
        current = Point(0, 0)
        while grid[current] != r:
            current += Point(1, 0)
            # Move to next row
            if current.x >= length:
                current += Point(-current.x, 1)
        points = [current]
        direction = Direction.EAST.value
        # While we did not make a round
        while current + direction != points[0]:
            # Move in direction
            current += direction
            # Change direction
            new_direction = _find_next_direction(grid, current, direction, r)
            if new_direction != direction:
                direction = new_direction
                points.append(current)
        polygons[r] = points
    return polygons


def read_output(file_content):
    solutions = file_content.split('----------\n')
    if len(solutions[-1].splitlines()) > 2:
        content = solutions[-1].splitlines()
    else:
        content = solutions[-2].splitlines()
    rooms = [r.rstrip() for r in content[0].split(' ') if len(r.rstrip()) > 0]
    grid = {}
    max_x = 0
    max_y = 0
    for line in content[1:]:
        if len(line.split(';')) < 2:
            break
        pos, room = line.split(';')
        x, y = pos.split(',')
        x = int(x) - 1
        y = int(y) - 1
        if x > max_x:
            max_x = x
        if y > max_y:
            max_y = y
        grid[Point(x, y)] = room.strip()
    for x in range(max_x + 1):
        grid[Point(x, -1)] = None
        grid[Point(x, max_y + 1)] = None
    for y in range(max_y + 1):
        grid[Point(-1, y)] = None
        grid[Point(max_x + 1, y)] = None
    grid[Point(-1, -1)] = None
    grid[Point(max_x + 1, max_y + 1)] = None
    return rooms, grid


def create_output(rooms, polygons):
    floorplan = output.FloorPlan()
    floor = output.FloorType()
    floorplan.add_Floor(floor)
    xml_rooms = []
    floor.set_Room(xml_rooms)
    for r in rooms:
        room = output.RoomType()
        room.set_type(r)
        room.set_ns_prefix_('fpo')

        point_list = []
        for p in polygons[r]:
            point = output.PointType()
            point.set_ns_prefix_('fpi')
            point.set_x(p.x)
            point.set_y(p.y)
            point_list.append(point)
        geometry = output.PointList()
        geometry.set_ns_prefix_('fpo')
        room.add_Geometry(geometry)
        geometry.set_Point(point_list)
        xml_rooms.append(room)
    return floorplan


def run(result_input, xml_output):
    with open(result_input, 'r') as read_file:
        rooms, grid = read_output(read_file.read())
        polygons = grid_to_polygons(grid, rooms)
        xml_out = create_output(rooms, polygons)
        with open(xml_output, 'w') as out_file:
            out_file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
            xml_out.export(out_file, level=0,
                        namespacedef_='xmlns:fpo="urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4" xmlns:fpi="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:uuid:93f50c73-8e3b-4039-a9f6-acccd08806ed input.xsd urn:uuid:98d4b9ef-21e0-4e74-9bbd-51de8369ead4 output.xsd"')
