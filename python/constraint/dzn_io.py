def write_array1d(f, key, xs):
    xs = [str(x) for x in xs]
    f.write(f'{key} = [{",".join(xs)}];\n')


def write_array2d(f, key, xs):
    f.write(f'{key} = [|{" | ".join(", ".join(x) for x in xs)}|];\n')


def write_value(f, key, x):
    f.write(f'{key} = {x};\n')


def write_enum(f, key, xs):
    xs = [str(x) for x in xs]
    f.write(f'{key} = \u007b{",".join(xs)}\u007d;\n')
