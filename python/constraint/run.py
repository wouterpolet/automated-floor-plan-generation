import os.path
import subprocess
import sys
import convert_xml_to_dzn
import convert_results_to_xml

minizinc_input = 'minizinc_input.dzn'
minizinc_output = 'minizinc_output.txt'

if len(sys.argv) < 4:
    print("Run with at least three arguments: python run.py <input_file> <output_file> <cells_per_meter> <minizinc_output>")
    print("Minizinc output is optional")
    exit(-1)

input_path = sys.argv[1]
output_path = sys.argv[2]
cells_per_meter = int(sys.argv[3])
if len(sys.argv) > 4:
    minizinc_output = sys.argv[4]

convert_xml_to_dzn.run(input_path, minizinc_input, cells_per_meter)
subprocess.run(f'minizinc --solver Gurobi {os.path.join(os.path.dirname(__file__), "./minizinc/model_room_alloc.mzn")} {minizinc_input} -o {minizinc_output} --output-time --intermediate -p 12 --time-limit 1200000', shell=True)
convert_results_to_xml.run(minizinc_output, output_path)
