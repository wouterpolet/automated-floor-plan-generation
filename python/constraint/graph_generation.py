def _coordinate_to_int(x, y, length):
    return (y - 1) * length + x


def generate_grid_edge_lists(length, width):
    edge_from = [1] * length * width * 2
    edge_to = [1] * length * width * 2

    for x in range(1, length * 1 + 1):
        for y in range(1, width * 1 + 1):
            if x + 1 <= length * 1:
                edge_from[(_coordinate_to_int(x, y, length) - 1) * 2] = _coordinate_to_int(x, y, length)
                edge_to[(_coordinate_to_int(x, y, length) - 1) * 2] = _coordinate_to_int(x + 1, y, length)
            if y + 1 <= width * 1:
                edge_from[_coordinate_to_int(x, y, length) * 2 - 1] = _coordinate_to_int(x, y, length)
                edge_to[_coordinate_to_int(x, y, length) * 2 - 1] = _coordinate_to_int(x, y + 1, length)

    return edge_from, edge_to
